Aplikasi ini dibuat menggunakan Laravel 5.5
https://laravel.com/

Template yang digunakan adalah template berbayar yang aku beli disini
https://wrapbootstrap.com/theme/unify-responsive-website-template-WB0412697

Template admin yang aku pakai itu template gratis aku download disini
https://www.themeineed.com/downloads/diffdash-free-admin-template/

Untuk menjalankan aplikasi, harus download dulu composer
https://getcomposer.org/

Lalu harus download package-package nya dulu
composer install
composer update

oh iya, copy dulu file .env.example lalu di buat jadi .env
cp .env.example .env

lalu samain konfigurasi nya kayak gini

APP_NAME='Yuliantos Merchant'
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://ecommerce.dev

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=namadatabsenya
DB_USERNAME=namausernamenya
DB_PASSWORD=passwordnya

BROADCAST_DRIVER=log
CACHE_DRIVER=array
SESSION_DRIVER=file
QUEUE_DRIVER=sync

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=

RAJAONGKIR_ENDPOINTAPI=https://api.rajaongkir.com/starter
RAJAONGKIR_APIKEY=aabfced4c39533c1a433c2273718dfbb

PAYPAL_CLIENT_ID=AV8kyHrWZdZaugC3js2AnR44drBZBWTmAPOViF1ZcFBHCpbbIdUhnMfnvzDam7_vSPX7BgL_PUE5m6rn
PAYPAL_SECRET=ENqsA0PUc89WPidpQzDrqFN5zF7amZCvQrJ1YXyarz0LkdsE0rZIeWA5Fvtl4GeYy-eBORzw1U_ubrtF

GOOGLE_ID=471701353103-3r5rsdjulnid3e3g6u3ivgqaossi6uc5.apps.googleusercontent.com
GOOGLE_SECRET=REmrTHQ95NVuwX7Wot-FmWMF
GOOGLE_URL=http://ecommerce.dev/login/google/callback

FACEBOOK_ID=341697136307755
FACEBOOK_SECRET=57902db35cba6226cf3b6d827cff8807
FACEBOOK_URL=http://ecommerce.dev/login/facebook/callback

oh iya, supaya bisa pakai fitur google auth sama facebook auth, harus buat virtual host dulu
nama url nya
http://ecommerce.dev

terimakasih, semoga menang :D




