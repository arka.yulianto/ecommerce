-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 11, 2018 at 02:53 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2017_11_21_093251_create_tb_m_products_table', 3),
(10, '2017_11_21_094652_create_tb_m_product_galleries_table', 3),
(13, '2017_11_21_100538_create_tb_m_stock_keepings_table', 4),
(16, '2017_11_22_094941_create_tb_m_product_attributes_table', 5),
(17, '2017_11_21_093656_create_tb_m_product_categories_table', 6),
(18, '2017_11_21_092960_create_tb_m_tags_table', 7),
(19, '2017_11_21_093841_create_tb_m_product_tags_table', 7),
(31, '2017_11_24_012940_add_cart_session_id_to_users_table', 14),
(34, '2017_11_24_023850_create_tb_m_coupons_table', 15),
(35, '2017_11_24_063904_create_tb_m_comments_table', 16),
(38, '2017_11_16_102407_create_tb_m_medias_table', 18),
(39, '2017_11_14_000000_entrust_setup_tables', 19),
(42, '2017_11_21_092841_create_tb_m_categories_table', 20),
(46, '2017_12_31_143029_create_tb_m_currency_table', 21),
(51, '2018_01_01_025634_create_tb_m_widget_table', 24),
(53, '2017_11_24_075716_create_tb_m_menus_table', 25),
(56, '2017_11_23_153144_create_tb_m_user_datas_table', 26),
(57, '2014_10_12_000000_create_users_table', 27),
(61, '2018_01_27_135542_create_tb_m_subsciber_table', 28),
(64, '2017_11_21_095934_create_tb_m_orders_table', 30),
(65, '2017_11_23_150500_create_tb_m_order_details_table', 30),
(66, '2017_11_23_151124_create_tb_m_order_detail_attributes_table', 30),
(68, '2018_01_30_090155_create_tb_m_shipping_table', 31);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_categories`
--

CREATE TABLE `tb_m_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `media_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_categories`
--

INSERT INTO `tb_m_categories` (`id`, `parent_id`, `name`, `slug`, `description`, `media_id`, `created_at`, `updated_at`) VALUES
(1, 0, 'Male', 'male', '<p>Curabitur sapien tortor, pulvinar quis nisi vel, placerat elementum lorem. Fusce ut tortor aliquet, viverra libero vel, pulvinar turpis. Nulla euismod neque sit amet malesuada lobortis. Nulla id tempor tortor. Curabitur laoreet iaculis lacus, a sagittis ante bibendum ac. Aenean dignissim elementum congue. Nulla quis leo nec risus suscipit vehicula eu auctor neque. In a eros imperdiet, volutpat lectus gravida, commodo enim. Morbi volutpat dolor et justo pharetra euismod. Fusce maximus eleifend neque vel porttitor. Maecenas convallis convallis molestie. Nullam semper finibus erat ac mattis. Cras sed tellus quis mi consectetur lacinia.</p>', 1, '2018-01-10 18:52:16', '2018-01-10 18:52:16'),
(2, 1, 'Male Shoes', 'male-shoes', '<p>Phasellus laoreet feugiat facilisis. Nunc dui eros, iaculis sed metus vitae, iaculis congue sem. Etiam condimentum augue ut nibh efficitur rhoncus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer odio risus, dapibus consequat convallis eu, cursus non lectus. Nam nec convallis turpis. Donec nec dui ac dolor pulvinar malesuada. Fusce tempus at risus id rutrum. Pellentesque fringilla nec libero vitae viverra. Sed elementum ipsum ut nunc congue, in lobortis tellus consequat. Maecenas dui eros, iaculis convallis tincidunt at, pharetra in quam. Nunc metus ex, interdum lobortis risus nec, tincidunt euismod lacus. Nam vitae est iaculis purus malesuada semper at id tellus.</p>', 2, '2018-01-10 18:53:17', '2018-01-10 18:53:17'),
(3, 2, 'Male Formal Shoes', 'male-formal-shoes', '<p>Praesent imperdiet neque nec diam semper posuere eu eget quam. In sed auctor leo. Fusce bibendum fermentum commodo. Cras ornare lectus augue, nec venenatis neque bibendum tempor. Vestibulum orci erat, egestas nec tincidunt at, feugiat a quam. Aenean tincidunt risus dolor, in rutrum mauris suscipit tincidunt. Sed leo turpis, fermentum euismod efficitur eget, suscipit faucibus leo.</p>', 3, '2018-01-10 18:56:43', '2018-01-10 18:56:43'),
(4, 2, 'Male Sport Shoes', 'male-sport-shoes', '<p>Donec pretium nec lacus vitae pellentesque. Duis consectetur volutpat velit, in ultrices dui pretium quis. Donec consectetur arcu orci, eget commodo tellus porta id. Nulla facilisi. Aenean facilisis tristique tortor nec posuere. Cras at faucibus est, ac facilisis leo. Nullam cursus est sed ipsum aliquam, eu vehicula ipsum dapibus.</p>', 4, '2018-01-10 19:00:47', '2018-01-10 19:07:33'),
(5, 2, 'Male Flip Flop', 'male-flip-flop', '<p>Nunc sem orci, ornare non diam id, dignissim rhoncus nulla. Mauris vel eros varius arcu mattis hendrerit. Ut tincidunt, justo a aliquet porttitor, est felis posuere massa, in cursus dolor felis nec mauris. Etiam efficitur, mauris sed ornare vulputate, elit massa vestibulum purus, eu mattis justo sapien a lectus. Donec leo augue, feugiat ac viverra eu, feugiat quis leo. Vestibulum eu posuere ipsum. Duis molestie pulvinar semper. Nunc dapibus nisl non libero vehicula cursus. Praesent porta velit tellus, non scelerisque leo vestibulum et.</p>', 5, '2018-01-10 19:05:48', '2018-01-10 19:05:48'),
(6, 2, 'Male Espardilles', 'male-espardilles', '<p>Fusce iaculis, sapien vel pharetra viverra, sem nulla ultricies tellus, vitae tincidunt tellus eros ac justo. In egestas condimentum nunc, imperdiet lobortis felis pulvinar ac. Vivamus efficitur pulvinar pretium. Praesent eget lacinia dui. Aliquam erat volutpat. Donec interdum ut tellus ut gravida. Aliquam interdum pharetra faucibus. Nunc ut accumsan tortor. Suspendisse volutpat nisl interdum arcu tempor sollicitudin. Cras at quam vitae quam blandit finibus.</p>', 51, '2018-01-10 19:41:22', '2018-01-10 19:41:22'),
(7, 1, 'Male Shirt', 'male-shirt', '<p>Curabitur et velit cursus nunc sodales varius et eget magna. Aliquam dui lacus, fringilla non arcu in, auctor porta nulla. Curabitur semper, felis id placerat molestie, sem nulla placerat ante, ultricies porttitor risus nisl tempus velit. Nulla dapibus consequat libero, in sollicitudin sapien dictum sit amet. Aliquam sit amet urna et ligula dictum hendrerit. Pellentesque ex diam, ultricies quis tellus sed, ornare sodales augue. Nunc eu nunc velit. Vestibulum consequat ut neque at suscipit. Morbi nec augue ex. Donec dapibus nisl sed nisl scelerisque mollis. Aenean iaculis, est at molestie blandit, nunc tellus fermentum libero, vel pellentesque metus lacus quis turpis. Sed viverra porta mi vestibulum tempus. Phasellus id feugiat enim.</p>', 57, '2018-01-10 19:47:57', '2018-01-10 19:47:57'),
(8, 7, 'T-Shirt', 't-shirt', '<p>Sed suscipit, ex vel consectetur sollicitudin, ipsum augue venenatis urna, a pulvinar est purus vel nunc. Curabitur vestibulum erat quis justo tincidunt auctor. In hac habitasse platea dictumst. Curabitur dolor leo, fringilla eu erat eget, pharetra laoreet massa. Aliquam erat volutpat. Maecenas in interdum nunc. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum sed turpis libero. Suspendisse lectus enim, ullamcorper a tempus rhoncus, elementum at enim. Vestibulum ligula nulla, pharetra sit amet aliquet vitae, pulvinar maximus nulla. Integer elit erat, volutpat in convallis id, pulvinar in ipsum. Maecenas a ex id nisl faucibus mollis quis a lacus. Vestibulum a diam tellus. Aliquam egestas ac neque a semper. Vivamus erat libero, blandit ut mattis ut, mollis ut justo. Donec non orci turpis.</p>', 58, '2018-01-10 19:49:09', '2018-01-10 19:49:09'),
(9, 1, 'Male Pants', 'male-pants', '<p>Cras gravida erat sit amet venenatis blandit. Vestibulum consectetur convallis felis eget rhoncus. Nulla facilisi. Donec blandit nisl mi, nec suscipit sapien facilisis sed. Sed semper ornare erat volutpat volutpat. Cras sagittis risus id tellus varius, et consectetur ipsum dictum. Suspendisse euismod, eros quis cursus vestibulum, elit sem varius libero, vel finibus nisl justo ut nulla. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut scelerisque nisi eget sapien faucibus molestie. Phasellus venenatis purus mi, id pellentesque ligula luctus ut. Morbi condimentum efficitur arcu. Nam in molestie justo, a vulputate dolor.</p>', 107, '2018-01-11 01:15:54', '2018-01-11 01:15:54'),
(10, 9, 'Shortpants', 'shortpants', '<p>Nunc facilisis posuere augue sit amet vestibulum. Donec id placerat lectus. In odio sapien, elementum vitae libero sed, commodo euismod quam. Ut vel viverra elit, nec vulputate velit. Donec placerat eros ipsum, nec ullamcorper dui ullamcorper non. Vivamus at tempus eros, fermentum vulputate augue. Curabitur ac tellus sit amet mi pharetra mollis. Mauris pretium elit a arcu viverra, sed interdum nunc hendrerit. Vivamus id risus scelerisque, mollis turpis id, lobortis magna. Curabitur sollicitudin gravida eros, tincidunt vestibulum erat pellentesque in. Cras vel tellus sit amet ante ornare pharetra at eget massa. Aliquam eu nibh justo. Integer cursus velit id magna fringilla tristique.</p>', 108, '2018-01-11 01:17:48', '2018-01-11 01:17:48'),
(11, 9, 'Longpants', 'longpants', '<p>Cras leo tellus, iaculis vel ante vitae, molestie iaculis nisl. In est massa, congue sed lorem vitae, aliquet sollicitudin ligula. Quisque sed faucibus nulla. Vivamus eget posuere nisi. Etiam a luctus eros, nec scelerisque metus. Proin mattis interdum orci, ut varius lorem commodo nec. Ut cursus lacinia pretium.</p>', 109, '2018-01-11 01:18:34', '2018-01-11 01:18:34');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_comments`
--

CREATE TABLE `tb_m_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_comments`
--

INSERT INTO `tb_m_comments` (`id`, `user_id`, `product_id`, `rate`, `content`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 4, 'Whoa amazing stuff I ever had', '2017-11-24 07:05:00', '2017-11-24 07:05:00'),
(2, 1, 3, 3, 'Whoa amazing stuff I ever had', '2017-11-24 07:05:00', '2017-11-24 07:05:00'),
(3, 1, 2, 3, 'Whoa amazing stuff I ever had', '2017-11-24 07:05:00', '2017-11-24 07:05:00'),
(4, 1, 21, 2, 'Too tight for a big size body like me', '2018-01-11 20:04:38', '2018-01-11 20:04:38'),
(5, 2, 2, 2, 'Too expensive', '2018-01-12 07:45:17', '2018-01-12 07:45:17');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_coupons`
--

CREATE TABLE `tb_m_coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `type` int(11) DEFAULT '0',
  `valid_thru` date NOT NULL,
  `products` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exclude_products` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exclude_categories` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `limit_user` int(11) DEFAULT '0',
  `limit_coupon` int(11) DEFAULT '0',
  `limit_item` int(11) DEFAULT '0',
  `minimum_spend` decimal(15,2) DEFAULT NULL,
  `maximum_spend` decimal(15,2) DEFAULT NULL,
  `individual_use_only` int(11) DEFAULT '0',
  `exclude_sale_items` int(11) DEFAULT '0',
  `allow_free_shipping` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_coupons`
--

INSERT INTO `tb_m_coupons` (`id`, `code`, `amount`, `type`, `valid_thru`, `products`, `categories`, `exclude_products`, `exclude_categories`, `limit_user`, `limit_coupon`, `limit_item`, `minimum_spend`, `maximum_spend`, `individual_use_only`, `exclude_sale_items`, `allow_free_shipping`, `created_at`, `updated_at`) VALUES
(2, 'SALE60', '60.00', 2, '2017-12-31', '1', '2', '4', '6', 3, 3, 3, '120.00', '800.00', 1, 1, 1, '2017-11-23 21:07:49', '2017-11-23 21:07:49'),
(3, 'HAPPYHOLIDAY90', '90.00', 1, '2017-12-24', '4', '15,16', '1', '1,2', 4, 5, 6, '1200.00', '3200.00', 1, 1, 1, '2017-11-23 21:17:02', '2017-11-23 21:17:02'),
(4, 'FREEALL10', '10.00', 1, '2018-01-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-23 22:54:25', '2017-11-23 22:54:25');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_currency`
--

CREATE TABLE `tb_m_currency` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `convertion` double NOT NULL,
  `thousand_separator` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `decimal_separator` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_currency`
--

INSERT INTO `tb_m_currency` (`id`, `name`, `alias`, `symbol`, `convertion`, `thousand_separator`, `decimal_separator`, `created_at`, `updated_at`) VALUES
(1, 'Euro', 'eur', '&euro;', 0.83, '.', ',', '2017-12-31 09:04:00', '2017-12-31 09:04:00'),
(2, 'US Dollar', 'usd', '$', 1, ',', '.', '2017-12-31 09:04:00', '2017-12-31 09:04:00'),
(3, 'Rupiah', 'idr', 'Rp. ', 13484, '.', ',', '2017-12-31 09:04:00', '2017-12-31 09:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_medias`
--

CREATE TABLE `tb_m_medias` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) NOT NULL,
  `caption` text COLLATE utf8mb4_unicode_ci,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_medias`
--

INSERT INTO `tb_m_medias` (`id`, `name`, `size`, `caption`, `alt`, `description`, `created_at`, `updated_at`) VALUES
(1, '1515635528_1079761857.jpg', 223648, NULL, 'pexels-photo-375880', NULL, '2018-01-10 18:52:10', '2018-01-10 18:52:10'),
(2, '1515635580_2087472917.jpg', 127898, NULL, 'pexels-photo-636010', NULL, '2018-01-10 18:53:02', '2018-01-10 18:53:02'),
(3, '1515635795_498421034.jpeg', 186558, NULL, 'pexels-photo-293406', NULL, '2018-01-10 18:56:37', '2018-01-10 18:56:37'),
(4, '1515636031_1083170531.jpeg', 217804, NULL, 'pexels-photo-786003', NULL, '2018-01-10 19:00:33', '2018-01-10 19:00:33'),
(5, '1515636340_224999044.jpeg', 118704, NULL, 'sandals-flip-flops-footwear-beach-40737', NULL, '2018-01-10 19:05:42', '2018-01-10 19:05:42'),
(6, '1515636569_1146994052.jpg', 282040, NULL, '1', NULL, '2018-01-10 19:09:31', '2018-01-10 19:09:31'),
(7, '1515636571_2127399093.jpg', 248526, NULL, '2', NULL, '2018-01-10 19:09:33', '2018-01-10 19:09:33'),
(8, '1515636573_1708394775.jpg', 223408, NULL, '3', NULL, '2018-01-10 19:09:34', '2018-01-10 19:09:34'),
(9, '1515636574_1855203006.jpg', 364932, NULL, '4', NULL, '2018-01-10 19:09:36', '2018-01-10 19:09:36'),
(10, '1515636576_1671658633.jpg', 224153, NULL, '5', NULL, '2018-01-10 19:09:38', '2018-01-10 19:09:38'),
(11, '1515636846_1748194231.jpg', 207291, NULL, '1 (1)', NULL, '2018-01-10 19:14:08', '2018-01-10 19:14:08'),
(12, '1515636848_90129482.jpg', 188804, NULL, '2', NULL, '2018-01-10 19:14:10', '2018-01-10 19:14:10'),
(13, '1515636850_2086501308.jpg', 158432, NULL, '3', NULL, '2018-01-10 19:14:11', '2018-01-10 19:14:11'),
(14, '1515636851_970443861.jpg', 295128, NULL, '4', NULL, '2018-01-10 19:14:13', '2018-01-10 19:14:13'),
(15, '1515637063_1441128585.jpg', 200449, NULL, '1', NULL, '2018-01-10 19:17:45', '2018-01-10 19:17:45'),
(16, '1515637065_784467636.jpg', 162309, NULL, '2', NULL, '2018-01-10 19:17:47', '2018-01-10 19:17:47'),
(17, '1515637067_284765139.jpg', 159474, NULL, '3', NULL, '2018-01-10 19:17:48', '2018-01-10 19:17:48'),
(18, '1515637068_1458855814.jpg', 249815, NULL, '4', NULL, '2018-01-10 19:17:50', '2018-01-10 19:17:50'),
(19, '1515637070_962690003.jpg', 291514, NULL, '5', NULL, '2018-01-10 19:17:52', '2018-01-10 19:17:52'),
(20, '1515637617_1130508540.jpg', 62669, NULL, '1 (1)', NULL, '2018-01-10 19:26:59', '2018-01-10 19:26:59'),
(21, '1515637619_1471634232.jpg', 65586, NULL, '2', NULL, '2018-01-10 19:27:01', '2018-01-10 19:27:01'),
(22, '1515637621_1414945819.jpg', 60519, NULL, '3', NULL, '2018-01-10 19:27:02', '2018-01-10 19:27:02'),
(23, '1515637622_1562558322.jpg', 81636, NULL, '4', NULL, '2018-01-10 19:27:04', '2018-01-10 19:27:04'),
(24, '1515637685_870977239.jpg', 200107, NULL, '1', NULL, '2018-01-10 19:28:07', '2018-01-10 19:28:07'),
(25, '1515637687_1694263182.jpg', 149688, NULL, '2', NULL, '2018-01-10 19:28:09', '2018-01-10 19:28:09'),
(26, '1515637689_1430951691.jpg', 148952, NULL, '3', NULL, '2018-01-10 19:28:11', '2018-01-10 19:28:11'),
(27, '1515637691_1844536829.jpg', 304789, NULL, '4', NULL, '2018-01-10 19:28:12', '2018-01-10 19:28:12'),
(28, '1515637692_1114587139.jpg', 230818, NULL, '5', NULL, '2018-01-10 19:28:14', '2018-01-10 19:28:14'),
(29, '1515637856_1165111656.jpg', 99890, NULL, '1 (1)', NULL, '2018-01-10 19:30:58', '2018-01-10 19:30:58'),
(30, '1515637858_997048144.jpg', 250715, NULL, '2', NULL, '2018-01-10 19:31:00', '2018-01-10 19:31:00'),
(31, '1515637860_64919466.jpg', 74771, NULL, '3', NULL, '2018-01-10 19:31:02', '2018-01-10 19:31:02'),
(32, '1515637862_106071321.jpg', 67533, NULL, '4', NULL, '2018-01-10 19:31:04', '2018-01-10 19:31:04'),
(33, '1515637864_749748416.jpg', 157220, NULL, '5', NULL, '2018-01-10 19:31:06', '2018-01-10 19:31:06'),
(34, '1515638036_2081137061.jpg', 212187, NULL, '1', NULL, '2018-01-10 19:33:58', '2018-01-10 19:33:58'),
(35, '1515638038_2115917792.jpg', 131568, NULL, '2', NULL, '2018-01-10 19:34:00', '2018-01-10 19:34:00'),
(36, '1515638040_232083693.jpg', 112104, NULL, '3', NULL, '2018-01-10 19:34:02', '2018-01-10 19:34:02'),
(37, '1515638042_280291292.jpg', 221854, NULL, '4', NULL, '2018-01-10 19:34:03', '2018-01-10 19:34:03'),
(38, '1515638043_1149125777.jpg', 237296, NULL, '5', NULL, '2018-01-10 19:34:05', '2018-01-10 19:34:05'),
(39, '1515638045_1344707066.jpg', 237696, NULL, '6', NULL, '2018-01-10 19:34:07', '2018-01-10 19:34:07'),
(40, '1515638171_1872690937.jpg', 103486, NULL, '1', NULL, '2018-01-10 19:36:13', '2018-01-10 19:36:13'),
(41, '1515638173_406820849.jpg', 65471, NULL, '2', NULL, '2018-01-10 19:36:14', '2018-01-10 19:36:14'),
(42, '1515638174_1661288196.jpg', 46589, NULL, '3', NULL, '2018-01-10 19:36:16', '2018-01-10 19:36:16'),
(43, '1515638176_637257147.jpg', 110717, NULL, '4', NULL, '2018-01-10 19:36:18', '2018-01-10 19:36:18'),
(44, '1515638178_1376082506.jpg', 107217, NULL, '5', NULL, '2018-01-10 19:36:20', '2018-01-10 19:36:20'),
(45, '1515638180_1854895307.jpg', 152911, NULL, '6 (1)', NULL, '2018-01-10 19:36:21', '2018-01-10 19:36:21'),
(46, '1515638254_2062707292.jpg', 198588, NULL, '1 (1)', NULL, '2018-01-10 19:37:36', '2018-01-10 19:37:36'),
(47, '1515638256_891179145.jpg', 162889, NULL, '2', NULL, '2018-01-10 19:37:37', '2018-01-10 19:37:37'),
(48, '1515638257_1875861390.jpg', 150478, NULL, '3', NULL, '2018-01-10 19:37:39', '2018-01-10 19:37:39'),
(49, '1515638259_92619657.jpg', 270606, NULL, '4', NULL, '2018-01-10 19:37:41', '2018-01-10 19:37:41'),
(50, '1515638261_384913731.jpg', 228575, NULL, '5', NULL, '2018-01-10 19:37:43', '2018-01-10 19:37:43'),
(51, '1515638476_1167236184.jpeg', 192804, NULL, 'pexels-photo-267320', NULL, '2018-01-10 19:41:18', '2018-01-10 19:41:18'),
(52, '1515638636_970395263.jpg', 123493, NULL, '1', NULL, '2018-01-10 19:43:58', '2018-01-10 19:43:58'),
(53, '1515638638_1538322804.jpg', 111150, NULL, '2', NULL, '2018-01-10 19:44:00', '2018-01-10 19:44:00'),
(54, '1515638640_823308351.jpg', 101325, NULL, '3', NULL, '2018-01-10 19:44:02', '2018-01-10 19:44:02'),
(55, '1515638642_1364556806.jpg', 162480, NULL, '4', NULL, '2018-01-10 19:44:04', '2018-01-10 19:44:04'),
(56, '1515638644_941110440.jpg', 165494, NULL, '5 (1)', NULL, '2018-01-10 19:44:05', '2018-01-10 19:44:05'),
(57, '1515638868_1214464003.jpeg', 224223, NULL, 'pexels-photo-428311', NULL, '2018-01-10 19:47:50', '2018-01-10 19:47:50'),
(58, '1515638941_1305005123.jpeg', 100135, NULL, 'pexels-photo-428340', NULL, '2018-01-10 19:49:04', '2018-01-10 19:49:04'),
(59, '1515638970_920880868.jpg', 234545, NULL, '1 (1)', NULL, '2018-01-10 19:49:32', '2018-01-10 19:49:32'),
(60, '1515638972_1252155449.jpg', 201811, NULL, '2', NULL, '2018-01-10 19:49:34', '2018-01-10 19:49:34'),
(61, '1515638974_1250489674.jpg', 260545, NULL, '3', NULL, '2018-01-10 19:49:36', '2018-01-10 19:49:36'),
(62, '1515638976_1743622013.jpg', 130985, NULL, '4', NULL, '2018-01-10 19:49:38', '2018-01-10 19:49:38'),
(63, '1515638978_1939273658.jpg', 491525, NULL, '5', NULL, '2018-01-10 19:49:40', '2018-01-10 19:49:40'),
(64, '1515657320_1542369348.jpg', 531449, NULL, '1', NULL, '2018-01-11 00:55:22', '2018-01-11 00:55:22'),
(65, '1515657322_1529083864.jpg', 578522, NULL, '2', NULL, '2018-01-11 00:55:24', '2018-01-11 00:55:24'),
(66, '1515657324_1413368264.jpg', 815501, NULL, '3', NULL, '2018-01-11 00:55:25', '2018-01-11 00:55:25'),
(67, '1515657325_256044474.jpg', 284854, NULL, '4', NULL, '2018-01-11 00:55:27', '2018-01-11 00:55:27'),
(68, '1515657580_435350874.jpg', 541841, NULL, '1 (1)', NULL, '2018-01-11 00:59:41', '2018-01-11 00:59:41'),
(69, '1515657581_1872053827.jpg', 513294, NULL, '2', NULL, '2018-01-11 00:59:43', '2018-01-11 00:59:43'),
(70, '1515657583_1445227960.jpg', 706139, NULL, '3', NULL, '2018-01-11 00:59:44', '2018-01-11 00:59:44'),
(71, '1515657584_1502766366.jpg', 317440, NULL, '4', NULL, '2018-01-11 00:59:45', '2018-01-11 00:59:45'),
(72, '1515657658_1021238156.jpg', 493923, NULL, '1 (1)', NULL, '2018-01-11 01:00:59', '2018-01-11 01:00:59'),
(73, '1515657659_1180048443.jpg', 540318, NULL, '2', NULL, '2018-01-11 01:01:01', '2018-01-11 01:01:01'),
(74, '1515657661_1361263216.jpg', 792817, NULL, '3', NULL, '2018-01-11 01:01:02', '2018-01-11 01:01:02'),
(75, '1515657662_1766920899.jpg', 329836, NULL, '4', NULL, '2018-01-11 01:01:03', '2018-01-11 01:01:03'),
(76, '1515657731_1416574604.jpg', 371533, NULL, '1', NULL, '2018-01-11 01:02:13', '2018-01-11 01:02:13'),
(77, '1515657733_1521543267.jpg', 381420, NULL, '2', NULL, '2018-01-11 01:02:14', '2018-01-11 01:02:14'),
(78, '1515657734_1212742439.jpg', 464499, NULL, '3', NULL, '2018-01-11 01:02:15', '2018-01-11 01:02:15'),
(79, '1515657735_201571524.jpg', 245311, NULL, '4', NULL, '2018-01-11 01:02:17', '2018-01-11 01:02:17'),
(80, '1515657737_368028299.jpg', 221117, NULL, '5', NULL, '2018-01-11 01:02:18', '2018-01-11 01:02:18'),
(81, '1515657817_1930297548.jpg', 141545, NULL, '1 (1)', NULL, '2018-01-11 01:03:39', '2018-01-11 01:03:39'),
(82, '1515657819_1216488794.jpg', 204026, NULL, '2', NULL, '2018-01-11 01:03:40', '2018-01-11 01:03:40'),
(83, '1515657820_463406122.jpg', 225853, NULL, '3', NULL, '2018-01-11 01:03:41', '2018-01-11 01:03:41'),
(84, '1515657821_1178457976.jpg', 122169, NULL, '4', NULL, '2018-01-11 01:03:42', '2018-01-11 01:03:42'),
(85, '1515657902_552380965.jpg', 434945, NULL, '1', NULL, '2018-01-11 01:05:03', '2018-01-11 01:05:03'),
(86, '1515657903_1960995636.jpg', 439490, NULL, '2', NULL, '2018-01-11 01:05:04', '2018-01-11 01:05:04'),
(87, '1515657904_950649219.jpg', 479626, NULL, '3', NULL, '2018-01-11 01:05:06', '2018-01-11 01:05:06'),
(88, '1515657906_1696575900.jpg', 330809, NULL, '4', NULL, '2018-01-11 01:05:07', '2018-01-11 01:05:07'),
(89, '1515657966_226234391.jpg', 515595, NULL, '1 (1)', NULL, '2018-01-11 01:06:08', '2018-01-11 01:06:08'),
(90, '1515657968_72172253.jpg', 527649, NULL, '2', NULL, '2018-01-11 01:06:09', '2018-01-11 01:06:09'),
(91, '1515657969_419490823.jpg', 630239, NULL, '3', NULL, '2018-01-11 01:06:10', '2018-01-11 01:06:10'),
(92, '1515657970_1114098861.jpg', 319066, NULL, '4', NULL, '2018-01-11 01:06:11', '2018-01-11 01:06:11'),
(93, '1515658042_215928891.jpg', 111491, NULL, '1', NULL, '2018-01-11 01:07:23', '2018-01-11 01:07:23'),
(94, '1515658043_792893861.jpg', 89611, NULL, '2', NULL, '2018-01-11 01:07:24', '2018-01-11 01:07:24'),
(95, '1515658044_423905918.jpg', 110914, NULL, '3', NULL, '2018-01-11 01:07:25', '2018-01-11 01:07:25'),
(96, '1515658045_1339192136.jpg', 95805, NULL, '4', NULL, '2018-01-11 01:07:26', '2018-01-11 01:07:26'),
(97, '1515658120_1577948474.jpg', 429539, NULL, '1 (1)', NULL, '2018-01-11 01:08:41', '2018-01-11 01:08:41'),
(98, '1515658121_649801526.jpg', 385692, NULL, '2', NULL, '2018-01-11 01:08:42', '2018-01-11 01:08:42'),
(99, '1515658122_662058810.jpg', 641252, NULL, '3', NULL, '2018-01-11 01:08:43', '2018-01-11 01:08:43'),
(100, '1515658123_1139299255.jpg', 296912, NULL, '4', NULL, '2018-01-11 01:08:45', '2018-01-11 01:08:45'),
(101, '1515658402_452479500.jpg', 99570, NULL, '1', NULL, '2018-01-11 01:13:23', '2018-01-11 01:13:23'),
(102, '1515658403_1139223228.jpg', 90233, NULL, '2', NULL, '2018-01-11 01:13:25', '2018-01-11 01:13:25'),
(103, '1515658405_647506805.jpg', 109562, NULL, '3', NULL, '2018-01-11 01:13:26', '2018-01-11 01:13:26'),
(104, '1515658406_1238015546.jpg', 89753, NULL, '4', NULL, '2018-01-11 01:13:27', '2018-01-11 01:13:27'),
(105, '1515658407_370697487.jpg', 61199, NULL, '5', NULL, '2018-01-11 01:13:28', '2018-01-11 01:13:28'),
(107, '1515658537_2015481996.jpeg', 193292, NULL, 'pexels-photo-296881', NULL, '2018-01-11 01:15:38', '2018-01-11 01:15:38'),
(108, '1515658648_1194316697.jpg', 163397, NULL, 'man-shorts-people-trunk', NULL, '2018-01-11 01:17:30', '2018-01-11 01:17:30'),
(109, '1515658699_1108582432.jpeg', 208700, NULL, 'pexels-photo-569172', NULL, '2018-01-11 01:18:21', '2018-01-11 01:18:21'),
(110, '1515658856_114046366.jpg', 406702, NULL, '1', NULL, '2018-01-11 01:20:57', '2018-01-11 01:20:57'),
(111, '1515658857_11295321.jpg', 420204, NULL, '2', NULL, '2018-01-11 01:20:59', '2018-01-11 01:20:59'),
(112, '1515658859_290731866.jpg', 421551, NULL, '3', NULL, '2018-01-11 01:21:00', '2018-01-11 01:21:00'),
(113, '1515658860_409188657.jpg', 393843, NULL, '4', NULL, '2018-01-11 01:21:01', '2018-01-11 01:21:01'),
(114, '1515658930_190439321.jpg', 387628, NULL, '1', NULL, '2018-01-11 01:22:11', '2018-01-11 01:22:11'),
(115, '1515658931_886159722.jpg', 417380, NULL, '2', NULL, '2018-01-11 01:22:12', '2018-01-11 01:22:12'),
(116, '1515658932_1713620811.jpg', 669699, NULL, '3', NULL, '2018-01-11 01:22:14', '2018-01-11 01:22:14'),
(117, '1515658934_594229264.jpg', 305875, NULL, '4', NULL, '2018-01-11 01:22:15', '2018-01-11 01:22:15'),
(118, '1515659012_532279685.jpg', 462971, NULL, '1 (1)', NULL, '2018-01-11 01:23:33', '2018-01-11 01:23:33'),
(119, '1515659013_359594150.jpg', 486080, NULL, '2', NULL, '2018-01-11 01:23:34', '2018-01-11 01:23:34'),
(120, '1515659014_952785338.jpg', 664627, NULL, '3', NULL, '2018-01-11 01:23:35', '2018-01-11 01:23:35'),
(121, '1515659015_1017730592.jpg', 346843, NULL, '4', NULL, '2018-01-11 01:23:37', '2018-01-11 01:23:37'),
(122, '1515738691_1590485775.jpg', 103030, NULL, '1', NULL, '2018-01-11 23:31:32', '2018-01-11 23:31:32'),
(123, '1515738692_73003062.jpg', 94001, NULL, '2', NULL, '2018-01-11 23:31:33', '2018-01-11 23:31:33'),
(124, '1515738693_43705317.jpg', 95756, NULL, '3', NULL, '2018-01-11 23:31:34', '2018-01-11 23:31:34'),
(125, '1515738694_1943481071.jpg', 82392, NULL, '4', NULL, '2018-01-11 23:31:35', '2018-01-11 23:31:35'),
(126, '1515738695_1687611936.jpg', 163713, NULL, '5', NULL, '2018-01-11 23:31:36', '2018-01-11 23:31:36'),
(127, '1515738696_778096936.jpg', 85506, NULL, '6', NULL, '2018-01-11 23:31:37', '2018-01-11 23:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_menus`
--

CREATE TABLE `tb_m_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_number` int(11) DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `widget_id` int(11) DEFAULT NULL,
  `tag_open` text COLLATE utf8mb4_unicode_ci,
  `tag_close` text COLLATE utf8mb4_unicode_ci,
  `is_mega` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_menus`
--

INSERT INTO `tb_m_menus` (`id`, `parent_id`, `name`, `route_name`, `url`, `method_name`, `order_number`, `icon`, `widget_id`, `tag_open`, `tag_close`, `is_mega`, `created_at`, `updated_at`) VALUES
(1, 0, 'Home', NULL, 'http://ecommerce.dev', NULL, 2, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:00:38', '2018-02-07 21:04:25'),
(2, 0, 'Categories', NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, 1, '2018-02-07 21:00:55', '2018-02-07 21:25:32'),
(3, 2, 'Shoes', NULL, NULL, NULL, 7, NULL, 8, NULL, NULL, 0, '2018-02-07 21:04:22', '2018-02-07 21:25:32'),
(5, 3, 'Shirt', NULL, NULL, NULL, 8, NULL, 10, NULL, NULL, 0, '2018-02-07 21:08:18', '2018-02-07 21:25:32'),
(6, 2, 'Pants', NULL, NULL, NULL, 11, NULL, 11, NULL, NULL, 0, '2018-02-07 21:09:07', '2018-02-07 21:25:32'),
(8, 6, 'Shoes Again', NULL, NULL, NULL, 12, NULL, 8, NULL, NULL, 0, '2018-02-07 21:11:29', '2018-02-07 21:25:32'),
(9, 2, 'Male', NULL, 'http://ecommerce.dev/category/1', NULL, 15, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:11:52', '2018-02-07 21:25:32'),
(10, 9, 'Male Shoes', NULL, 'http://ecommerce.dev/category/2', NULL, 16, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:12:01', '2018-02-07 21:25:32'),
(11, 9, 'Male Shirt', NULL, 'http://ecommerce.dev/category/7', NULL, 18, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:12:01', '2018-02-07 21:25:32'),
(12, 9, 'Male Pants', NULL, 'http://ecommerce.dev/category/9', NULL, 20, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:12:01', '2018-02-07 21:25:32'),
(13, 2, 'Pants Again', NULL, NULL, NULL, 23, NULL, 11, NULL, NULL, 0, '2018-02-07 21:12:58', '2018-02-07 21:25:32'),
(14, 13, 'Shirt Again', NULL, NULL, NULL, 24, NULL, 10, NULL, NULL, 0, '2018-02-07 21:14:03', '2018-02-07 21:25:32'),
(15, 2, 'Product', NULL, NULL, NULL, 27, NULL, 2, '<div class=\"col-sm-6 col-lg-4 g-mb-30 g-mb-0--md\">', '</div>', 0, '2018-02-07 21:17:22', '2018-02-07 21:25:32'),
(16, 0, 'Shop', NULL, 'http://ecommerce.dev/shop', NULL, 4, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:25:13', '2018-02-07 21:25:32'),
(17, 0, 'About', NULL, 'http://ecommerce.dev/pages/about-us', NULL, 15, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:26:07', '2018-02-07 21:26:07'),
(18, 0, 'Terms and Condition', NULL, 'http://ecommerce.dev/pages/terms-and-condition', NULL, 16, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:26:40', '2018-02-07 21:27:01'),
(19, 0, 'Contact Us', NULL, 'http://ecommerce.dev/pages/contact-us', NULL, 17, NULL, NULL, NULL, NULL, 0, '2018-02-07 21:27:34', '2018-02-07 21:27:34');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_orders`
--

CREATE TABLE `tb_m_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` decimal(8,2) NOT NULL,
  `coupon_id` decimal(8,2) DEFAULT NULL,
  `tax` decimal(8,2) DEFAULT NULL,
  `total` decimal(8,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_receive` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_orders`
--

INSERT INTO `tb_m_orders` (`id`, `order_number`, `sub_total`, `coupon_id`, `tax`, `total`, `user_id`, `payment_method`, `bank_name`, `account_name`, `bank_receive`, `status`, `created_at`, `updated_at`) VALUES
(1, '2-1517653891', '69.10', NULL, '0.00', '69.84', 2, 'PayPal', NULL, NULL, NULL, 2, '2018-02-03 03:32:11', '2018-02-07 19:02:41'),
(2, '13-1518281778', '70.10', NULL, '0.00', '70.84', 13, 'PayPal', NULL, NULL, NULL, 2, '2018-02-10 09:57:23', '2018-02-10 10:06:37'),
(3, '2-1518352338', '60.00', NULL, '0.00', '60.67', 2, 'PayPal', NULL, NULL, NULL, 2, '2018-02-11 05:34:40', '2018-02-11 05:40:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_order_details`
--

CREATE TABLE `tb_m_order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `total` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_order_details`
--

INSERT INTO `tb_m_order_details` (`id`, `order_id`, `product_id`, `price`, `qty`, `coupon_id`, `total`) VALUES
(1, 1, 25, '24.10', 1, NULL, '24.10'),
(2, 1, 20, '45.00', 1, NULL, '45.00'),
(3, 2, 25, '24.10', 1, NULL, '24.10'),
(4, 2, 21, '46.00', 1, NULL, '46.00'),
(5, 3, 13, '60.00', 1, NULL, '60.00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_order_detail_attributes`
--

CREATE TABLE `tb_m_order_detail_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_detail_id` int(11) NOT NULL,
  `attribute_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_order_detail_attributes`
--

INSERT INTO `tb_m_order_detail_attributes` (`id`, `order_detail_id`, `attribute_name`, `attribute_value`) VALUES
(1, 1, 'Size', '32'),
(2, 1, 'Color', 'Brown'),
(3, 2, 'Size', 'L'),
(4, 3, 'Size', '30'),
(5, 3, 'Color', 'Creme'),
(6, 4, 'Size', '31'),
(7, 5, 'Size', 'L');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_password_resets`
--

CREATE TABLE `tb_m_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_permissions`
--

CREATE TABLE `tb_m_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_permissions`
--

INSERT INTO `tb_m_permissions` (`id`, `parent_id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 0, 'manage-dashboard', 'Dashboard', 'manage dashboard', '2017-11-30 03:35:00', '2017-11-30 03:35:00'),
(2, 0, 'manage-product', 'Product', 'manage product', '2017-11-30 03:35:00', '2017-11-30 03:35:00'),
(4, 2, 'add-product', 'Add Product', 'add product', '2017-11-30 03:35:00', '2017-11-30 03:35:00'),
(5, 2, 'edit-product', 'Edit Product', 'edit Product', '2017-11-30 03:35:00', '2017-11-30 03:35:00'),
(6, 2, 'delete-product', 'Delete Product', 'delete Product', '2017-11-30 03:35:00', '2017-11-30 03:35:00'),
(7, 0, 'manage-category', 'Category', 'manage dashboard', '2017-11-30 03:35:00', '2017-11-30 03:35:00'),
(8, 0, 'manage-ass', 'Ass', 'lorem ipsum', '2017-11-29 22:23:47', '2017-11-29 22:23:47'),
(9, 8, 'add-ass', 'Add asshole', 'ass is shit', '2017-11-29 23:56:15', '2017-11-29 23:56:15');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_permission_role`
--

CREATE TABLE `tb_m_permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_permission_role`
--

INSERT INTO `tb_m_permission_role` (`permission_id`, `role_id`) VALUES
(1, 4),
(2, 4),
(4, 4),
(5, 4),
(6, 4),
(7, 4),
(8, 4),
(9, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_products`
--

CREATE TABLE `tb_m_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `media_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL,
  `discount` decimal(8,2) DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stock` bigint(20) DEFAULT NULL,
  `weight` bigint(20) DEFAULT NULL,
  `height` bigint(20) DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_products`
--

INSERT INTO `tb_m_products` (`id`, `name`, `description`, `media_id`, `price`, `discount`, `sku`, `stock`, `weight`, `height`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Nike Airwalk', '<p>Nunc sagittis semper venenatis. Mauris sit amet congue augue. Nam et nisl a est volutpat condimentum et sit amet nibh. Morbi odio mi, dignissim ac libero a, malesuada commodo justo. Nam imperdiet vestibulum magna. Donec vehicula justo nulla, ut porta nibh congue at. Sed non risus consectetur, eleifend dui quis, mattis mi. Morbi vel ornare lorem, non dictum quam. Cras dictum nisi sed posuere viverra. Nam a scelerisque mi, eu tempus arcu. Integer hendrerit ut turpis vel lacinia. Quisque ultrices laoreet sem, et tempor leo ultricies a. Phasellus cursus purus in congue pellentesque. Duis sit amet dolor ac ipsum fringilla auctor hendrerit et mauris. Phasellus at viverra elit, et vehicula purus. In eget posuere urna.</p>', '6', '299.00', NULL, NULL, 10, 400, 43, 'nike-airwalk', '2018-01-10 19:11:47', '2018-01-10 19:11:47'),
(2, 'Adidas Bounce', '<p>Fusce scelerisque blandit lacus eget auctor. Sed at turpis quis arcu venenatis rutrum eget quis augue. Suspendisse placerat ipsum nec tincidunt rutrum. Aliquam justo lorem, efficitur nec nunc ac, luctus ullamcorper justo. Phasellus eget sapien nec neque lobortis euismod quis non lorem. Suspendisse et massa tempor magna auctor tempor nec ut nulla. Sed sit amet pharetra odio. Pellentesque fringilla dapibus risus et mattis. Nam interdum, mi non faucibus pulvinar, magna augue interdum risus, eget facilisis nibh velit quis sapien. Nam diam ipsum, accumsan et lacus in, dignissim consequat purus. Donec tempor finibus tellus, luctus feugiat neque.</p>', '12', '198.00', NULL, NULL, 20, 400, 40, 'adidas-bounce', '2018-01-10 19:16:45', '2018-01-10 19:16:45'),
(3, 'Yongki Komaladi Brown', '<p>Aliquam sodales dui a euismod volutpat. Donec sit amet ullamcorper massa, quis tincidunt augue. Nulla pharetra turpis in libero feugiat rhoncus. Integer maximus pellentesque erat, et ultricies leo semper id. Aliquam eu nunc et mauris porta pulvinar. Mauris justo nibh, maximus eget eleifend at, lobortis ut est. Pellentesque maximus magna eu nibh pellentesque suscipit in ut lorem. In hac habitasse platea dictumst. Nullam euismod vestibulum neque, nec vulputate est maximus ut. Sed ullamcorper rutrum diam eu vehicula. Etiam nunc dolor, sagittis sit amet felis sit amet, rhoncus aliquet urna. Donec ut dignissim neque, eu varius sapien. Nulla facilisis erat ac pretium porttitor. Suspendisse potenti. Integer facilisis libero vel sapien facilisis, nec bibendum lacus pretium.</p>', '16', '152.00', '132.00', NULL, 60, 600, 25, 'yongki-komaladi-brown', '2018-01-10 19:22:28', '2018-01-10 19:22:28'),
(4, 'Dr. Kevin Brown', '<p>Suspendisse sit amet lectus nisi. Nam maximus, nisi non porta lacinia, urna metus pulvinar lorem, et fermentum arcu massa eget velit. Proin in massa nec orci consequat posuere. Suspendisse potenti. Quisque vitae est mattis, malesuada tortor sed, suscipit nisl. Nullam convallis quis est non accumsan. Fusce feugiat tellus et mauris eleifend fermentum. Phasellus porta nulla risus, sed finibus risus porttitor non. Nulla posuere lacus justo, sit amet pulvinar quam dictum vel. Sed elementum nisi nec odio finibus euismod. Vestibulum porttitor pulvinar elit id auctor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum malesuada posuere dapibus. Pellentesque consequat ultricies pharetra.</p>', '20', '169.00', NULL, NULL, 17, 600, 32, 'dr-kevin-brown', '2018-01-10 19:27:45', '2018-01-10 19:27:45'),
(5, 'Everbest Dark Brown', '<p>Pellentesque porta tellus eu egestas porta. Morbi efficitur at arcu a molestie. Cras vulputate, ligula at aliquet egestas, ex augue scelerisque erat, eget tincidunt nisi dolor sit amet odio. Ut tristique magna nec semper placerat. Etiam placerat est at hendrerit sodales. Fusce scelerisque, ex eu vestibulum auctor, purus libero consequat dui, non sollicitudin nibh lorem sed turpis. Praesent pretium velit sed est sollicitudin, nec dignissim purus ultrices. Mauris sed orci pretium, feugiat mi a, scelerisque magna. Suspendisse sed mollis sem, at sollicitudin sem. Quisque imperdiet semper est. Praesent commodo eu mi sit amet euismod. Donec justo augue, ullamcorper quis eros rutrum, rhoncus convallis lectus. Donec dictum, justo non ultricies ullamcorper, felis metus vehicula enim, vitae rutrum lacus risus quis mauris. Quisque sit amet ligula dui. Cras laoreet dolor ac ante blandit vestibulum. Cras volutpat vitae magna vel posuere.</p>', '24', '199.00', '160.00', NULL, 40, 550, 32, 'everbest-dark-brown-1', '2018-01-10 19:29:49', '2018-01-10 19:30:09'),
(6, 'Billabong Blue', '<p>Mauris pretium mi augue, ac pharetra justo tincidunt sed. Integer facilisis ullamcorper ligula, id consequat mi commodo non. Sed venenatis ante sed est tempus volutpat. Nulla facilisi. Cras tincidunt felis sapien, in porta nisl blandit nec. Nunc accumsan eros turpis, vitae accumsan lectus vulputate non. Donec venenatis diam id diam malesuada hendrerit. Suspendisse accumsan, massa sed egestas condimentum, sapien turpis varius enim, in iaculis ipsum libero ac turpis. Curabitur hendrerit arcu orci, non auctor magna pharetra et. Nam cursus congue dolor, at hendrerit nibh condimentum at. Sed eu purus nec augue laoreet sagittis.</p>', '29', '23.00', NULL, NULL, 17, 300, 32, 'billabong-blue-1', '2018-01-10 19:32:18', '2018-01-10 19:33:06'),
(7, 'Adidas Black Flip Flop', '<p>Integer est enim, tincidunt eu posuere quis, bibendum vel sapien. Proin vehicula sodales ligula, vitae placerat leo pellentesque quis. Maecenas finibus metus sit amet lorem commodo efficitur. Donec condimentum est et dapibus semper. Suspendisse sit amet risus non quam feugiat hendrerit in eget elit. Sed risus nulla, ornare eu quam sit amet, tempor sodales eros. Suspendisse vestibulum nisl in orci ullamcorper dignissim. Sed condimentum augue eget velit aliquam vulputate. Vestibulum viverra ultricies interdum. Donec finibus nibh molestie neque imperdiet, gravida facilisis magna cursus. Curabitur sagittis iaculis malesuada. Pellentesque ut sollicitudin tortor. Quisque eget erat in lectus molestie sollicitudin. Quisque pharetra elit ut pharetra luctus.</p>', '34', '56.00', NULL, NULL, 14, 200, 32, 'adidas-black-flip-flop', '2018-01-10 19:35:30', '2018-01-10 19:35:30'),
(8, 'Crock Blue Flip Flop', '<p>Mauris eget tortor purus. Fusce id metus vitae nulla faucibus condimentum quis gravida ex. Proin quis neque elit. Nam at efficitur mauris, et sodales urna. Aliquam erat volutpat. Sed placerat velit sit amet diam euismod ultricies. Aliquam erat volutpat. Fusce imperdiet augue ut imperdiet congue. Mauris hendrerit tellus sapien, id pharetra felis elementum vitae. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam tellus mi, molestie sed volutpat in, lacinia sit amet nunc. Nullam tincidunt odio ut purus congue viverra vulputate quis risus. Proin id metus et elit convallis sagittis. Praesent bibendum, nisi non placerat tincidunt, diam lectus sodales lectus, quis semper odio erat quis leo. Nunc gravida, mauris at scelerisque cursus, sapien odio pretium erat, eu gravida massa sapien ut leo. Proin sit amet magna ipsum.</p>', '40', '42.00', NULL, NULL, 14, 300, 32, 'crock-blue-flip-flop', '2018-01-10 19:37:15', '2018-01-10 19:37:15'),
(9, 'TOMS Brown', '<p>Fusce iaculis, sapien vel pharetra viverra, sem nulla ultricies tellus, vitae tincidunt tellus eros ac justo. In egestas condimentum nunc, imperdiet lobortis felis pulvinar ac. Vivamus efficitur pulvinar pretium. Praesent eget lacinia dui. Aliquam erat volutpat. Donec interdum ut tellus ut gravida. Aliquam interdum pharetra faucibus. Nunc ut accumsan tortor. Suspendisse volutpat nisl interdum arcu tempor sollicitudin. Cras at quam vitae quam blandit finibus.</p>', '46', '76.00', NULL, NULL, 14, 200, 32, 'toms-brown', '2018-01-10 19:43:01', '2018-01-10 19:43:01'),
(10, 'Dr. Kevin Light Brown', '<p>Sed fermentum, lacus vel condimentum pulvinar, ipsum neque sagittis nibh, sed rhoncus velit nisl sed quam. In at posuere tellus, in gravida massa. Duis diam mauris, lobortis in dignissim non, viverra sed tortor. Fusce vulputate risus ex, sit amet molestie risus ultricies vitae. Donec tempus ex vel elit aliquet, in consectetur nisi vulputate. Nulla ullamcorper nulla et dictum scelerisque. Vestibulum vel sapien et libero efficitur euismod. Praesent lacinia sodales justo, sit amet feugiat enim egestas nec. Nullam non mollis mauris. Nam egestas augue mi, in vehicula quam vehicula in. Pellentesque a dictum magna. Mauris ultrices dolor quis odio efficitur, ut ultricies justo varius. Nunc sed velit neque. Donec elementum, mi eu dignissim varius, felis metus lobortis tellus, eu tincidunt nisi lacus sed dolor.</p>', '52', '79.00', NULL, NULL, 15, 250, 32, 'dr-kevin-light-brown', '2018-01-10 19:45:23', '2018-01-10 19:45:23'),
(11, 'Arizona Gray', '<p>Aliquam ultrices gravida elementum. Nulla vel libero vitae lacus vulputate blandit. In ultrices enim porttitor urna fringilla volutpat. Nunc nisl ipsum, faucibus id justo in, pretium consectetur enim. Morbi fermentum tortor non urna semper sollicitudin. Aliquam molestie vitae sapien consectetur tempor. Vivamus ultricies consectetur dui, accumsan accumsan dui tempor non. Morbi eget tellus vel risus iaculis faucibus. Quisque ultricies lacus quis ante ultricies placerat. Vestibulum eu mi nec lacus tincidunt interdum. Proin sed risus ullamcorper, porta metus et, placerat ipsum. Integer ut risus ullamcorper, laoreet elit non, rutrum metus. Nulla at enim nec mi dictum tempor. Phasellus vehicula eros a porttitor hendrerit. Etiam ut erat non arcu venenatis laoreet ac at libero. Vivamus quis sollicitudin neque.</p>', '59', '32.00', NULL, NULL, 40, 60, 45, 'arizona-gray', '2018-01-10 19:51:34', '2018-01-10 19:51:34'),
(12, 'Shirt Light Blue', '<p>Phasellus diam purus, feugiat in hendrerit ut, consectetur in urna. Nullam eget imperdiet lectus. Aliquam ac dolor elementum nunc lacinia ultricies. Phasellus placerat dignissim augue facilisis suscipit. Nulla cursus hendrerit mi a rhoncus. Fusce leo lacus, gravida eu eros dapibus, finibus convallis nunc. Nulla non viverra dui. Donec non convallis mauris, non semper odio. Etiam finibus mauris venenatis magna iaculis, at luctus velit ultricies. Aliquam ac metus ut justo pulvinar dictum. Fusce lacinia nibh at lectus consequat, vitae dignissim urna aliquam. Etiam sed tortor quis mauris rhoncus feugiat nec quis magna. Aenean varius ipsum dui, vel aliquam ipsum suscipit ut.</p>', '64', '67.00', NULL, NULL, 13, 120, 120, 'shirt-light-blue', '2018-01-11 00:59:19', '2018-01-11 00:59:19'),
(13, 'Shirt White', '<p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris at massa sit amet eros consequat placerat. Fusce sed tempus urna. Ut iaculis risus eu tristique auctor. Nam mauris orci, accumsan finibus tellus ac, tincidunt aliquam neque. Curabitur nibh diam, faucibus et venenatis laoreet, semper auctor lectus. Aliquam libero lorem, luctus sit amet orci non, interdum pellentesque quam. Nam condimentum dui mattis magna tincidunt, vitae condimentum ipsum tempor. Donec ligula est, aliquet sit amet enim vel, euismod semper ligula. Suspendisse non augue elit. Sed quam quam, sodales at dui eget, semper vulputate ipsum. Maecenas egestas mattis magna, et imperdiet lacus scelerisque vitae. Morbi quis urna eget dolor eleifend tincidunt a nec eros.</p>', '68', '60.00', NULL, NULL, 14, 120, 50, 'shirt-white-1', '2018-01-11 01:00:44', '2018-01-11 01:12:03'),
(14, 'Singlet White', '<p>Integer pretium elementum quam in egestas. Fusce eget leo maximus, venenatis metus eget, sagittis ante. Integer sodales auctor turpis, eget placerat lacus ultrices in. Aliquam erat volutpat. Suspendisse a condimentum massa, vitae iaculis ligula. Pellentesque non sagittis libero, ultricies aliquet est. Pellentesque non orci vitae eros tincidunt mollis laoreet vel purus. In dapibus luctus mollis. Duis varius, mauris at euismod facilisis, mi enim egestas est, at elementum dolor magna sed nibh. Vestibulum ultrices eleifend tortor, consequat eleifend erat facilisis sit amet. Maecenas id metus lobortis, malesuada purus sit amet, porta tortor. Sed condimentum mi vitae porta sollicitudin.</p>', '72', '56.00', NULL, NULL, 80, 120, 54, 'singlet-white', '2018-01-11 01:01:58', '2018-01-11 01:01:58'),
(15, 'Nike T-Shirt Black', '<p>Nunc est dui, pretium eget odio vel, placerat rutrum turpis. Donec molestie ipsum quis eros blandit pharetra. Cras fermentum interdum elit, bibendum viverra risus egestas quis. Curabitur faucibus augue dolor, mollis mollis velit faucibus interdum. Quisque leo arcu, consectetur commodo consequat nec, imperdiet quis tellus. Nam vitae aliquam dolor, quis dignissim magna. In malesuada sed nunc nec commodo. Nullam nisl massa, tristique et tellus vitae, consequat facilisis nunc.</p>', '76', '46.00', NULL, NULL, NULL, 130, 56, 'nike-t-shirt-black', '2018-01-11 01:03:22', '2018-01-11 01:03:22'),
(16, 'Billabong Black', '<p>Etiam quis faucibus sapien. Morbi lacus dui, maximus ac accumsan non, malesuada nec urna. Praesent ut ipsum orci. Fusce sed pulvinar lacus. Mauris nunc neque, lacinia sed tellus in, gravida posuere odio. Integer nisl lectus, lacinia in ligula eget, hendrerit rhoncus dui. Vivamus tellus odio, mollis et mi ut, bibendum scelerisque urna. Praesent erat dolor, mattis et justo vitae, sollicitudin lobortis dolor. Cras libero ipsum, ultrices vitae porttitor id, lacinia sit amet ligula. Proin quis risus et turpis venenatis ullamcorper. Suspendisse quis purus ultrices, posuere felis sed, ullamcorper nunc. In efficitur velit fringilla, sodales lorem sed, hendrerit massa. Fusce in erat placerat, tincidunt sapien sit amet, interdum ligula. Nullam maximus euismod consequat.</p>', '81', '56.00', NULL, NULL, NULL, 120, 56, 'billabong-black', '2018-01-11 01:04:48', '2018-01-11 01:04:48'),
(17, 'Adidas Singlet White', '<p>Curabitur rhoncus efficitur convallis. Sed in leo ut urna malesuada cursus. Phasellus metus ligula, tincidunt in cursus eu, sollicitudin eu purus. Aenean ut eros dui. Maecenas ornare mi id convallis feugiat. Phasellus vitae molestie felis. Aliquam eget fringilla eros.</p>', '85', '44.00', NULL, NULL, 70, 170, 67, 'adidas-singlet-white', '2018-01-11 01:05:52', '2018-01-11 01:05:52'),
(18, 'Adidas Green T-Shirt', '<p>Nam dapibus molestie diam vel laoreet. Nulla lobortis nunc et lacus dictum vulputate. Donec non ipsum orci. Nunc faucibus a orci quis vulputate. Cras consectetur nulla urna, quis blandit velit dictum vitae. Cras et felis at ipsum semper posuere id eget mi. Vivamus in turpis leo. Suspendisse varius tristique aliquet. Nullam tincidunt convallis mollis. Aenean consequat dui in neque accumsan vehicula.</p>', '89', '42.00', NULL, NULL, NULL, 130, 56, 'adidas-green-t-shirt', '2018-01-11 01:07:02', '2018-01-11 01:07:02'),
(19, 'White Shirt Billabong', '<p>Duis efficitur efficitur dolor, non hendrerit purus egestas ac. Duis eleifend, turpis at consectetur ultrices, nulla mauris tristique augue, eget rhoncus magna urna ac est. Sed placerat nec est vel vehicula. Aliquam aliquet lacus ac convallis tempus. Cras nec ipsum a massa hendrerit viverra ut et sem. Morbi ultricies consectetur metus id finibus. Curabitur massa neque, tempus eu purus at, lobortis facilisis felis. Duis mattis lectus vitae sapien iaculis pulvinar. Etiam pharetra mauris quis ligula efficitur, nec pellentesque ligula faucibus. Sed maximus sapien ut efficitur sollicitudin. Mauris pretium sed tellus non convallis. Sed molestie, nulla eget euismod pharetra, nisi turpis malesuada ligula, maximus fermentum arcu odio a turpis. Praesent efficitur ligula id ultricies blandit. Vivamus ac lacus ipsum.</p>', '93', '32.00', NULL, NULL, 14, 160, 67, 'white-shirt-billabong', '2018-01-11 01:08:24', '2018-01-11 01:08:24'),
(20, 'Billabong Shirt Light Blue', '<p>Quisque id consectetur elit. Mauris eget felis ut ligula gravida placerat sed vitae risus. Duis tellus felis, blandit sed mollis nec, aliquet at elit. Vivamus ultricies, augue id finibus aliquam, nibh leo sodales est, at bibendum est magna non est. Sed at tempor leo. Etiam sagittis sodales bibendum. Fusce sed arcu odio. Pellentesque non ullamcorper odio. Nulla augue nisl, placerat sed tristique sed, fermentum id nisl. Nulla placerat arcu vitae magna maximus, vitae egestas elit suscipit. Quisque et tellus at lorem lobortis varius quis in lorem.</p>', '97', '45.00', NULL, NULL, 34, 100, 78, 'billabong-shirt-light-blue', '2018-01-11 01:09:54', '2018-01-11 01:09:54'),
(21, 'Black Shortpant', '<p>Pellentesque arcu est, volutpat id ex ac, tincidunt accumsan odio. Integer consequat in velit non vehicula. Ut quis nulla ultrices, sollicitudin augue non, tincidunt velit. Morbi mollis urna eget neque aliquet lobortis. Morbi porta commodo mattis. Quisque dui ex, suscipit id mi ut, sagittis interdum lorem. Maecenas cursus lorem ipsum, ac pretium libero fermentum id. Cras tristique tellus non gravida accumsan. Nam ullamcorper sollicitudin dolor a egestas. Nullam viverra imperdiet odio quis posuere. Phasellus finibus orci ut nibh tristique consectetur. Phasellus vel orci quis lectus rhoncus porta sed a erat. Nulla ac egestas orci, rhoncus tincidunt nisi. Quisque dapibus vitae erat imperdiet maximus. In hac habitasse platea dictumst.</p>', '101', '46.00', NULL, NULL, 13, 200, 56, 'black-shortpant', '2018-01-11 01:20:40', '2018-01-11 01:20:40'),
(22, 'Brown Longpants', '<p>Nam a purus id ipsum molestie molestie non nec magna. Integer nec mollis eros. Cras aliquam nulla nec tincidunt tristique. Maecenas posuere sed tortor in tristique. Morbi quis eleifend odio. Nulla consectetur risus rhoncus orci ullamcorper aliquam. Quisque erat urna, laoreet et efficitur id, dictum quis leo. Pellentesque imperdiet erat vel gravida bibendum.</p>', '110', '56.00', NULL, NULL, 10, 400, 70, 'brown-longpants', '2018-01-11 01:21:56', '2018-01-11 01:21:56'),
(23, 'Blue Jeans Long', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pellentesque commodo felis nec lacinia. Suspendisse semper rhoncus malesuada. Duis vulputate tempus est eu consequat. Morbi a turpis venenatis, accumsan nibh at, volutpat erat. Fusce sed pharetra libero, eget dapibus libero. Cras elit justo, commodo et nisl a, varius feugiat velit. In hac habitasse platea dictumst. Quisque rhoncus venenatis est, in tempor mi tincidunt volutpat. Quisque pellentesque iaculis pharetra.</p>', '114', '65.00', NULL, NULL, NULL, 69, 80, 'blue-jeans-long', '2018-01-11 01:23:12', '2018-01-11 01:23:12'),
(24, 'Grey Jeans Long', '<p>Proin facilisis arcu dignissim, pellentesque turpis ut, consectetur lectus. Mauris ac tincidunt eros. Aenean vitae lorem in felis aliquam dapibus. Aliquam blandit viverra vehicula. Pellentesque congue lectus quis aliquet rhoncus. Donec consectetur, lectus ac tincidunt placerat, justo diam lobortis lectus, et posuere erat neque sed lorem. Pellentesque cursus mauris venenatis, elementum ipsum vitae, elementum purus. Aenean et ex ut leo tempor vestibulum id ac massa. Nunc eleifend sit amet nisl vel faucibus. Proin mattis purus sed nibh congue, quis mollis arcu mattis. Vestibulum iaculis mi at faucibus dictum. Vivamus iaculis eros nisi, vel tincidunt eros gravida ac. Vivamus lectus dui, posuere eu sodales sed, facilisis quis diam. Aenean tempus eros nisl, at lacinia libero pharetra et. Vivamus ut ultricies nunc. Morbi lorem ipsum, ultricies eu accumsan vitae, feugiat id leo.</p>', '118', '56.00', NULL, NULL, 100, 450, 120, 'grey-jeans-long', '2018-01-11 01:25:12', '2018-01-11 01:25:12'),
(25, 'Jogger Cream', '<p>ed neque nibh, tempor quis dictum eget, facilisis at nisi. Integer justo eros, ornare sed iaculis et, blandit ut risus. Proin sed rutrum nulla. Donec tempor et sem fringilla consequat. Nullam fermentum lacus vel odio viverra sodales. Duis tempor pulvinar mattis. Integer eros dolor, laoreet porta quam vel, fermentum tincidunt magna. Integer ac varius nunc, vitae sagittis lectus.</p>', '122', '24.10', NULL, NULL, 9, NULL, NULL, 'jogger-cream', '2018-01-11 23:33:16', '2018-01-11 23:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_product_attributes`
--

CREATE TABLE `tb_m_product_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `attribute_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_product_attributes`
--

INSERT INTO `tb_m_product_attributes` (`id`, `product_id`, `attribute_name`, `attribute_value`, `created_at`, `updated_at`) VALUES
(1, 1, 'Size', '38,39,40,41,42,43', '2018-01-10 19:11:47', '2018-01-10 19:11:47'),
(2, 2, 'Size', '38,40,42', '2018-01-10 19:16:45', '2018-01-10 19:16:45'),
(3, 3, 'Size', '40,41,42', '2018-01-10 19:22:28', '2018-01-10 19:22:28'),
(4, 4, 'Size', '41,42,43', '2018-01-10 19:27:45', '2018-01-10 19:27:45'),
(6, 5, 'Size', '40,41,42', '2018-01-10 19:30:09', '2018-01-10 19:30:09'),
(9, 6, 'Size', '40,42', '2018-01-10 19:33:06', '2018-01-10 19:33:06'),
(10, 9, 'Size', '38,40,42', '2018-01-10 19:43:01', '2018-01-10 19:43:01'),
(11, 10, 'Size', '39,41', '2018-01-10 19:45:23', '2018-01-10 19:45:23'),
(12, 11, 'Size', 'M,L', '2018-01-10 19:51:34', '2018-01-10 19:51:34'),
(13, 12, 'Size', 'L,XL', '2018-01-11 00:59:19', '2018-01-11 00:59:19'),
(15, 14, 'Size', 'L,XL', '2018-01-11 01:01:58', '2018-01-11 01:01:58'),
(16, 15, 'Size', 'M,L,XL', '2018-01-11 01:03:22', '2018-01-11 01:03:22'),
(17, 16, 'Size', 'M,L,XL', '2018-01-11 01:04:48', '2018-01-11 01:04:48'),
(18, 17, 'Size', 'L,XL', '2018-01-11 01:05:52', '2018-01-11 01:05:52'),
(19, 18, 'Size', 'M,L,XL', '2018-01-11 01:07:02', '2018-01-11 01:07:02'),
(20, 19, 'Size', 'M,L', '2018-01-11 01:08:24', '2018-01-11 01:08:24'),
(21, 20, 'Size', 'M,L,XL', '2018-01-11 01:09:54', '2018-01-11 01:09:54'),
(22, 13, 'Size', 'L,XL', '2018-01-11 01:12:03', '2018-01-11 01:12:03'),
(23, 21, 'Size', '31,32,33', '2018-01-11 01:20:40', '2018-01-11 01:20:40'),
(24, 22, 'Size', '32,33,34', '2018-01-11 01:21:56', '2018-01-11 01:21:56'),
(25, 23, 'Size', '31,32,33', '2018-01-11 01:23:12', '2018-01-11 01:23:12'),
(26, 24, 'Size', '31,32,33', '2018-01-11 01:25:12', '2018-01-11 01:25:12'),
(27, 25, 'Size', '30,31,32,33,34', '2018-01-11 23:33:16', '2018-01-11 23:33:16'),
(28, 25, 'Color', 'Creme,Brown', '2018-01-11 23:33:16', '2018-01-11 23:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_product_categories`
--

CREATE TABLE `tb_m_product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_product_categories`
--

INSERT INTO `tb_m_product_categories` (`id`, `category_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 4, 1, NULL, NULL),
(2, 4, 2, NULL, NULL),
(3, 3, 3, NULL, NULL),
(4, 3, 4, NULL, NULL),
(5, 3, 5, NULL, NULL),
(6, 5, 6, NULL, NULL),
(7, 5, 7, NULL, NULL),
(8, 5, 8, NULL, NULL),
(9, 6, 9, NULL, NULL),
(10, 6, 10, NULL, NULL),
(11, 8, 11, NULL, NULL),
(12, 7, 12, NULL, NULL),
(13, 7, 13, NULL, NULL),
(14, 7, 14, NULL, NULL),
(15, 7, 15, NULL, NULL),
(16, 8, 16, NULL, NULL),
(17, 7, 17, NULL, NULL),
(18, 8, 18, NULL, NULL),
(19, 8, 19, NULL, NULL),
(20, 7, 20, NULL, NULL),
(21, 10, 21, NULL, NULL),
(22, 11, 22, NULL, NULL),
(23, 11, 23, NULL, NULL),
(24, 11, 24, NULL, NULL),
(25, 11, 25, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_product_galleries`
--

CREATE TABLE `tb_m_product_galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_product_galleries`
--

INSERT INTO `tb_m_product_galleries` (`id`, `product_id`, `media_id`, `created_at`, `updated_at`) VALUES
(1, 1, 10, NULL, NULL),
(2, 1, 9, NULL, NULL),
(3, 1, 8, NULL, NULL),
(4, 1, 7, NULL, NULL),
(5, 1, 6, NULL, NULL),
(6, 2, 14, NULL, NULL),
(7, 2, 13, NULL, NULL),
(8, 2, 12, NULL, NULL),
(9, 3, 19, NULL, NULL),
(10, 3, 18, NULL, NULL),
(11, 3, 17, NULL, NULL),
(12, 3, 16, NULL, NULL),
(13, 4, 23, NULL, NULL),
(14, 4, 22, NULL, NULL),
(15, 4, 21, NULL, NULL),
(16, 4, 20, NULL, NULL),
(17, 5, 28, NULL, NULL),
(18, 5, 27, NULL, NULL),
(19, 5, 26, NULL, NULL),
(20, 5, 25, NULL, NULL),
(21, 5, 24, NULL, NULL),
(22, 6, 33, NULL, NULL),
(23, 6, 32, NULL, NULL),
(24, 6, 31, NULL, NULL),
(25, 6, 30, NULL, NULL),
(26, 6, 29, NULL, NULL),
(27, 7, 39, NULL, NULL),
(28, 7, 38, NULL, NULL),
(29, 7, 37, NULL, NULL),
(30, 7, 36, NULL, NULL),
(31, 7, 35, NULL, NULL),
(32, 7, 34, NULL, NULL),
(33, 9, 50, NULL, NULL),
(34, 9, 49, NULL, NULL),
(35, 9, 48, NULL, NULL),
(36, 9, 47, NULL, NULL),
(37, 9, 46, NULL, NULL),
(38, 10, 56, NULL, NULL),
(39, 10, 55, NULL, NULL),
(40, 10, 54, NULL, NULL),
(41, 10, 53, NULL, NULL),
(42, 10, 52, NULL, NULL),
(43, 11, 63, NULL, NULL),
(44, 11, 62, NULL, NULL),
(45, 11, 61, NULL, NULL),
(46, 11, 60, NULL, NULL),
(47, 11, 59, NULL, NULL),
(48, 12, 67, NULL, NULL),
(49, 12, 66, NULL, NULL),
(50, 12, 65, NULL, NULL),
(51, 12, 64, NULL, NULL),
(52, 13, 71, NULL, NULL),
(53, 13, 70, NULL, NULL),
(54, 13, 69, NULL, NULL),
(55, 13, 68, NULL, NULL),
(57, 16, 84, NULL, NULL),
(58, 16, 83, NULL, NULL),
(59, 16, 82, NULL, NULL),
(60, 16, 81, NULL, NULL),
(61, 19, 96, NULL, NULL),
(62, 19, 95, NULL, NULL),
(63, 19, 94, NULL, NULL),
(64, 19, 93, NULL, NULL),
(65, 20, 100, NULL, NULL),
(66, 20, 99, NULL, NULL),
(67, 20, 98, NULL, NULL),
(68, 20, 97, NULL, NULL),
(69, 21, 105, NULL, NULL),
(70, 21, 104, NULL, NULL),
(71, 21, 103, NULL, NULL),
(72, 21, 102, NULL, NULL),
(73, 21, 101, NULL, NULL),
(74, 22, 113, NULL, NULL),
(75, 22, 112, NULL, NULL),
(76, 22, 111, NULL, NULL),
(77, 22, 110, NULL, NULL),
(78, 23, 117, NULL, NULL),
(79, 23, 116, NULL, NULL),
(80, 23, 115, NULL, NULL),
(81, 23, 114, NULL, NULL),
(82, 24, 121, NULL, NULL),
(83, 24, 120, NULL, NULL),
(84, 24, 119, NULL, NULL),
(85, 24, 118, NULL, NULL),
(86, 25, 127, NULL, NULL),
(87, 25, 126, NULL, NULL),
(88, 25, 125, NULL, NULL),
(89, 25, 124, NULL, NULL),
(90, 25, 123, NULL, NULL),
(91, 25, 122, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_product_tags`
--

CREATE TABLE `tb_m_product_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_product_tags`
--

INSERT INTO `tb_m_product_tags` (`id`, `tag_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 2, 1, NULL, NULL),
(3, 3, 1, NULL, NULL),
(4, 2, 2, NULL, NULL),
(5, 3, 2, NULL, NULL),
(6, 4, 2, NULL, NULL),
(7, 5, 3, NULL, NULL),
(8, 6, 3, NULL, NULL),
(9, 6, 4, NULL, NULL),
(10, 7, 4, NULL, NULL),
(11, 6, 5, NULL, NULL),
(12, 8, 5, NULL, NULL),
(13, 9, 6, NULL, NULL),
(14, 4, 7, NULL, NULL),
(15, 10, 7, NULL, NULL),
(16, 10, 8, NULL, NULL),
(17, 11, 8, NULL, NULL),
(18, 12, 9, NULL, NULL),
(19, 13, 9, NULL, NULL),
(20, 7, 10, NULL, NULL),
(21, 12, 10, NULL, NULL),
(22, 14, 10, NULL, NULL),
(23, 15, 11, NULL, NULL),
(24, 16, 11, NULL, NULL),
(25, 17, 12, NULL, NULL),
(26, 18, 12, NULL, NULL),
(27, 17, 13, NULL, NULL),
(28, 18, 13, NULL, NULL),
(29, 3, 14, NULL, NULL),
(30, 19, 14, NULL, NULL),
(31, 1, 15, NULL, NULL),
(32, 3, 15, NULL, NULL),
(33, 16, 15, NULL, NULL),
(34, 9, 16, NULL, NULL),
(35, 4, 17, NULL, NULL),
(36, 19, 17, NULL, NULL),
(37, 4, 18, NULL, NULL),
(38, 16, 18, NULL, NULL),
(39, 9, 19, NULL, NULL),
(40, 16, 19, NULL, NULL),
(41, 9, 20, NULL, NULL),
(42, 18, 20, NULL, NULL),
(43, 20, 21, NULL, NULL),
(44, 21, 22, NULL, NULL),
(45, 21, 23, NULL, NULL),
(46, 21, 24, NULL, NULL),
(47, 17, 25, NULL, NULL),
(48, 22, 25, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_roles`
--

CREATE TABLE `tb_m_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_roles`
--

INSERT INTO `tb_m_roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administration', 'Can access all modules', '2018-01-26 23:32:29', '2018-01-26 23:32:29'),
(2, 'user', 'Registed User', 'Can access what desserve', '2018-01-26 23:32:29', '2018-01-26 23:32:29');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_role_user`
--

CREATE TABLE `tb_m_role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_role_user`
--

INSERT INTO `tb_m_role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(8, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_shipping`
--

CREATE TABLE `tb_m_shipping` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `courier_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_weight` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `service_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` text COLLATE utf8mb4_unicode_ci,
  `estimate_delivery` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receipt_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_shipping`
--

INSERT INTO `tb_m_shipping` (`id`, `order_id`, `first_name`, `last_name`, `email`, `phone_number`, `country_id`, `province_id`, `city_id`, `zip`, `address`, `courier_id`, `total_weight`, `cost`, `service_name`, `service_description`, `estimate_delivery`, `receipt_number`, `created_at`, `updated_at`) VALUES
(1, 1, 'Joel', 'Anthonio', 'user1@local.local', '089611081676', 'Indonesia', 9, 171, '41362', 'Jl. Sukaduka No 46', 'jne', 100, 0.74161969741916, 'OKE', 'Ongkos Kirim Ekonomis', '2-3', 'ASD-0918391-019381', '2018-02-03 04:36:48', '2018-02-07 19:02:41'),
(2, 2, 'Yulianto', 'Saparudin', 'yulianto.saparudin@gmail.com', '085779549820', 'Indonesia', 9, 171, '41361', 'Jl. Ahmad Yani No 98, Perumahan Puri Telukjambe Block C90 No 19', 'jne', 200, 0.74161969741916, 'OKE', 'Ongkos Kirim Ekonomis', '2-3', NULL, '2018-02-10 09:57:23', '2018-02-10 10:06:37'),
(3, 3, 'Hany', 'Amran', 'user1@local.local', '082213340942', 'Indonesia', 6, 152, '14000', 'Jl. Medan Merdeka No 8, Apartemen Skyland Building No 90, Jakarta Pusat', 'jne', 120, 0.66745772767725, 'OKE', 'Ongkos Kirim Ekonomis', '2-3', 'BDG-0918-13030', '2018-02-11 05:34:40', '2018-02-11 05:40:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_stock_keepings`
--

CREATE TABLE `tb_m_stock_keepings` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `stock` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_subsciber`
--

CREATE TABLE `tb_m_subsciber` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_tags`
--

CREATE TABLE `tb_m_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_tags`
--

INSERT INTO `tb_m_tags` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Nike', 'nike', '2018-01-10 19:11:47', '2018-01-10 19:11:47'),
(2, 'Run', 'run', '2018-01-10 19:11:47', '2018-01-10 19:11:47'),
(3, 'Sport', 'sport', '2018-01-10 19:11:47', '2018-01-10 19:11:47'),
(4, 'Adidas', 'adidas', '2018-01-10 19:16:45', '2018-01-10 19:16:45'),
(5, 'Yongki', 'yongki', '2018-01-10 19:22:28', '2018-01-10 19:22:28'),
(6, 'Oxford', 'oxford', '2018-01-10 19:22:28', '2018-01-10 19:22:28'),
(7, 'Dr.Kevin', 'drkevin', '2018-01-10 19:27:45', '2018-01-10 19:27:45'),
(8, 'Everbest', 'everbest', '2018-01-10 19:29:49', '2018-01-10 19:29:49'),
(9, 'Billabong', 'billabong', '2018-01-10 19:32:18', '2018-01-10 19:32:18'),
(10, 'Sandals', 'sandals', '2018-01-10 19:35:30', '2018-01-10 19:35:30'),
(11, 'Crock', 'crock', '2018-01-10 19:37:15', '2018-01-10 19:37:15'),
(12, 'Slip', 'slip', '2018-01-10 19:43:01', '2018-01-10 19:43:01'),
(13, 'TOM', 'tom', '2018-01-10 19:43:01', '2018-01-10 19:43:01'),
(14, 'Espardilles', 'espardilles', '2018-01-10 19:45:23', '2018-01-10 19:45:23'),
(15, 'Arizona', 'arizona', '2018-01-10 19:51:34', '2018-01-10 19:51:34'),
(16, 'Shirt', 'shirt', '2018-01-10 19:51:34', '2018-01-10 19:51:34'),
(17, 'Casual', 'casual', '2018-01-11 00:59:19', '2018-01-11 00:59:19'),
(18, 'Collar', 'collar', '2018-01-11 00:59:19', '2018-01-11 00:59:19'),
(19, 'Singlet', 'singlet', '2018-01-11 01:01:58', '2018-01-11 01:01:58'),
(20, 'short', 'short', '2018-01-11 01:20:40', '2018-01-11 01:20:40'),
(21, 'Long', 'long', '2018-01-11 01:21:56', '2018-01-11 01:21:56'),
(22, 'Jogger', 'jogger', '2018-01-11 23:33:16', '2018-01-11 23:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_users`
--

CREATE TABLE `tb_m_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart_session_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_users`
--

INSERT INTO `tb_m_users` (`id`, `name`, `email`, `password`, `cart_session_id`, `provider`, `provider_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin1', 'admin1@local.local', '$2y$10$/LEI9ezgFnSULezHkL/mme6SDFtbejNs0hfR1XIRV7buXsyFjE8tq', NULL, NULL, NULL, 'JfQdSBrdbPJapnZySluc3UjADfaC12IQG3G6ppmJN85oxhvKFRIE3NGN7gMG', '2018-01-26 23:32:29', '2018-01-26 23:32:29'),
(2, 'user1', 'user1@local.local', '$2y$10$EHC7VLK1yqe4ADnEoQVgoeZ7h4x6i5vTr9GfvpFgROgqpsBeXXoWS', NULL, NULL, NULL, 'mcKcz7QEW2MBNC6uN7tNXbemvcocDX57w03DBH3qID7LZBBG1mk7R3cAbrs5', '2018-01-26 23:32:29', '2018-01-26 23:32:29'),
(11, 'irfanoscar', 'irfanoscar@gmail.com', '$2y$10$Pd9Rf23rGFkaUHH8Eok1gO9SNU82920xKrGe0FVY4t1SUAkO3fk3.', NULL, NULL, NULL, NULL, '2018-01-27 07:28:13', '2018-01-27 07:28:13'),
(12, 'fauwaz', 'fauwazfauzan@gmail.com', '$2y$10$RN4ZU9BZnluYaMhMTPBHleBEZ6nwc3.jXPwAhPbn4/ryiYvyUbf6i', NULL, 'google', '111822183077007821023', 'f7YX0lWajUlYvjUiD62dOhZ3yubAtdGSjewlXoYMkwIawfoucjKboNAmwH1S', '2018-01-27 07:34:11', '2018-01-27 07:34:11'),
(13, 'yulianto', 'yulianto.saparudin@gmail.com', '$2y$10$sGkhOgze7klUpYVOyaPVNe18hxIx5A5TwCv/zYjo1iTX7lkLpJchu', NULL, 'facebook', '1937237219655012', 'ttXDySjEhpvLfSpvxOJbrfpVhqCvGkhM7KCtVMcJ1a61l5h4wDuKyJfCF5Ni', '2018-01-28 00:09:21', '2018-02-10 09:51:41');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_user_datas`
--

CREATE TABLE `tb_m_user_datas` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `sex` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_birth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_user_datas`
--

INSERT INTO `tb_m_user_datas` (`id`, `first_name`, `last_name`, `identity_number`, `phone_number`, `country_id`, `province_id`, `city_id`, `sex`, `zip`, `place_of_birth`, `date_of_birth`, `address`, `picture`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Yulianto', 'Saparudin', '399103918391', '089611081675', 'Indonesia', 9, 171, 'Male', '41362', 'Karawang', '1995-07-13', 'Jl. Sukaduka No 46', 'https://s.gravatar.com/avatar/dbe830afd9174dff8a1bedb0ffcea478?s=80', 1, '2018-01-26 23:32:29', '2018-01-26 23:32:29'),
(2, 'Hany', 'Amran', '321040294002', '085779549820', 'Indonesia', 6, 152, 'Female', '14000', 'Jakarta', '1992-04-02', 'Jl. Medan Merdeka No 8, Apartemen Skyland Building No 90, Jakarta Pusat', 'https://s.gravatar.com/avatar/09c2bd3799a0310b6402afd2f6d8ab57?s=80', 2, '2018-01-26 23:32:29', '2018-02-07 08:16:14'),
(9, 'Irfan', 'Oscar', NULL, NULL, NULL, NULL, NULL, 'Male', NULL, NULL, '1995-04-20', NULL, 'http://www.gravatar.com/avatar/0a3b6328408152aad6dca20e5ae2057c.jpg?s=200&d=mm&r=g', 11, '2018-01-27 07:28:13', '2018-01-27 07:28:13'),
(10, 'Fauwaz', 'Fauzan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg', 12, '2018-01-27 07:34:11', '2018-01-27 07:34:11'),
(11, 'Yulianto', 'Saparudin', NULL, '085779549820', 'Indonesia', 9, 171, 'Male', NULL, NULL, NULL, NULL, 'https://graph.facebook.com/v2.10/1937237219655012/picture?type=large', 13, '2018-01-28 00:09:21', '2018-02-10 06:50:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_m_widget`
--

CREATE TABLE `tb_m_widget` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_parent` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `limit` int(11) DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image_dimension` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_m_widget`
--

INSERT INTO `tb_m_widget` (`id`, `name`, `type`, `category_parent`, `category_id`, `limit`, `content`, `created_at`, `updated_at`, `image_dimension`) VALUES
(2, 'Product', 'product', NULL, 4, NULL, NULL, '2018-01-04 19:29:29', '2018-01-04 19:29:29', '700x700'),
(8, 'Shoes', 'categories', 2, NULL, 10, NULL, '2018-02-07 21:03:39', '2018-02-07 21:03:39', NULL),
(10, 'Shirt', 'categories', 7, NULL, 10, NULL, '2018-02-07 21:07:58', '2018-02-07 21:07:58', NULL),
(11, 'Pants', 'categories', 9, NULL, 10, NULL, '2018-02-07 21:08:48', '2018-02-07 21:08:48', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_categories`
--
ALTER TABLE `tb_m_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_m_categories_slug_unique` (`slug`);

--
-- Indexes for table `tb_m_comments`
--
ALTER TABLE `tb_m_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_coupons`
--
ALTER TABLE `tb_m_coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_currency`
--
ALTER TABLE `tb_m_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_medias`
--
ALTER TABLE `tb_m_medias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_menus`
--
ALTER TABLE `tb_m_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_orders`
--
ALTER TABLE `tb_m_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_order_details`
--
ALTER TABLE `tb_m_order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_order_detail_attributes`
--
ALTER TABLE `tb_m_order_detail_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_password_resets`
--
ALTER TABLE `tb_m_password_resets`
  ADD KEY `tb_m_password_resets_email_index` (`email`);

--
-- Indexes for table `tb_m_permissions`
--
ALTER TABLE `tb_m_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_m_permissions_name_unique` (`name`);

--
-- Indexes for table `tb_m_permission_role`
--
ALTER TABLE `tb_m_permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`);

--
-- Indexes for table `tb_m_products`
--
ALTER TABLE `tb_m_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_m_products_slug_unique` (`slug`);

--
-- Indexes for table `tb_m_product_attributes`
--
ALTER TABLE `tb_m_product_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_product_categories`
--
ALTER TABLE `tb_m_product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_product_galleries`
--
ALTER TABLE `tb_m_product_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_product_tags`
--
ALTER TABLE `tb_m_product_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_roles`
--
ALTER TABLE `tb_m_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_m_roles_name_unique` (`name`);

--
-- Indexes for table `tb_m_role_user`
--
ALTER TABLE `tb_m_role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `tb_m_shipping`
--
ALTER TABLE `tb_m_shipping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_stock_keepings`
--
ALTER TABLE `tb_m_stock_keepings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_subsciber`
--
ALTER TABLE `tb_m_subsciber`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_m_subsciber_email_unique` (`email`);

--
-- Indexes for table `tb_m_tags`
--
ALTER TABLE `tb_m_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_m_tags_slug_unique` (`slug`);

--
-- Indexes for table `tb_m_users`
--
ALTER TABLE `tb_m_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tb_m_users_email_unique` (`email`);

--
-- Indexes for table `tb_m_user_datas`
--
ALTER TABLE `tb_m_user_datas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_m_widget`
--
ALTER TABLE `tb_m_widget`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `tb_m_categories`
--
ALTER TABLE `tb_m_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_m_comments`
--
ALTER TABLE `tb_m_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_m_coupons`
--
ALTER TABLE `tb_m_coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_m_currency`
--
ALTER TABLE `tb_m_currency`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_m_medias`
--
ALTER TABLE `tb_m_medias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `tb_m_menus`
--
ALTER TABLE `tb_m_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tb_m_orders`
--
ALTER TABLE `tb_m_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_m_order_details`
--
ALTER TABLE `tb_m_order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_m_order_detail_attributes`
--
ALTER TABLE `tb_m_order_detail_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_m_permissions`
--
ALTER TABLE `tb_m_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_m_products`
--
ALTER TABLE `tb_m_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tb_m_product_attributes`
--
ALTER TABLE `tb_m_product_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tb_m_product_categories`
--
ALTER TABLE `tb_m_product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tb_m_product_galleries`
--
ALTER TABLE `tb_m_product_galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `tb_m_product_tags`
--
ALTER TABLE `tb_m_product_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `tb_m_roles`
--
ALTER TABLE `tb_m_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_m_shipping`
--
ALTER TABLE `tb_m_shipping`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_m_stock_keepings`
--
ALTER TABLE `tb_m_stock_keepings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_m_subsciber`
--
ALTER TABLE `tb_m_subsciber`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_m_tags`
--
ALTER TABLE `tb_m_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tb_m_users`
--
ALTER TABLE `tb_m_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tb_m_user_datas`
--
ALTER TABLE `tb_m_user_datas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_m_widget`
--
ALTER TABLE `tb_m_widget`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
