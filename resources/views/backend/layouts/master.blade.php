<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
	<title> @yield('title') | DiffDash - Free Admin Template</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/font-awesome/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/linearicons/style.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/metisMenu/metisMenu.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/chartist/css/chartist.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/toastr/toastr.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/summernote/summernote.css') }}">
	<!-- DataTables -->
    <link href="{{ url('assets/backend/vendor/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/backend/vendor/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/backend/vendor/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/backend/vendor/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/backend/vendor/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/backend/vendor/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/backend/vendor/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ url('assets/backend/vendor/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
	<!-- dropzone -->
    <link href="{{ url('assets/backend/vendor/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- TOast -->
    <link href="{{ url('assets/backend/vendor/toast-master/css/jquery.toast.css') }}" rel="stylesheet" type="text/css" />
	<!-- Select2 -->
    <link href="{{ url('assets/backend/vendor/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ url('assets/backend/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{ url('assets/backend/css/main.css') }}">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{ url('assets/backend/css/demo.css') }}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="{{ url('assets/backend/img/apple-icon.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ url('assets/backend/img/favicon.png') }}">
	<!-- scustom -->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/backend/css/custom.css') }}">
	<script>
        var SITE_URL = "{{ url('/') }}";
    </script>

</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			@include('backend.layouts.navbar')
		</nav>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<div id="left-sidebar" class="sidebar">
			@include('backend.layouts.sidebar')
		</div>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN CONTENT -->
		<div id="main-content">
			<div class="container-fluid">
				@yield('content')
			</div>
		</div>
		<!-- END MAIN CONTENT -->
		<div class="clearfix"></div>
		<footer>
			@include('backend.layouts.footer')
		</footer>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="{{ url('assets/backend/vendor/jquery/jquery.min.js') }}"></script>
	 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
	 <script src="{{ url('assets/backend/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/jquery-sortable/jquery.mjs.nestedSortable.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/metisMenu/metisMenu.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/jquery-sparkline/js/jquery.sparkline.min.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/bootstrap-progressbar/js/bootstrap-progressbar.min.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/chartist/js/chartist.min.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/chartist-plugin-axistitle/chartist-plugin-axistitle.min.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/chartist-plugin-legend-latest/chartist-plugin-legend.js') }}"></script>
	<script src="{{ url('assets/backend/vendor/toastr/toastr.js') }}"></script>
	<script src="{{ url('assets/backend/scripts/common.js') }}"></script>
	 <!-- DataTable -->
    <script src="{{ url('assets/backend/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/dataTables.bootstrap.js') }}"></script>

    <script src="{{ url('assets/backend/vendor/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/buttons.bootstrap.min.js') }}"></script>
    <!-- <script src="{{ url('assets/backend/vendor/datatables/jszip.min.js') }}"></script> -->
    <script src="{{ url('assets/backend/vendor/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/responsive.bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/dataTables.scroller.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/dataTables.colVis.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/dataTables.fixedColumns.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/datatables/dataTables.rowsGroup.js') }}"></script>
	<!-- Dropzone -->
    <script src="{{ url('assets/backend/vendor/dropzone/dropzone.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ url('assets/backend/vendor/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <!-- Jquery Validator -->
    <script src="{{ url('assets/backend/vendor/jquery-validation/js/jquery.validate.min.js') }}"></script>
    <!-- Datepicker -->
    <script src="{{ url('assets/backend/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- autoNumeric -->
    <script src="{{ url('assets/backend/vendor/autoNumeric/autoNumeric.js') }}"></script>
    <!-- tinymce -->
    <script src="{{ url('assets/backend/vendor/tinymce/tinymce.js') }}"></script>
    <!-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script> -->
    <!-- custom js -->
    <script src="{{ url('assets/backend/vendor/summernote/summernote.min.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('assets/backend/vendor/ckeditor/adapters/jquery.js') }}"></script>
     <!-- Toast -->
    <script src="{{ url('assets/backend/vendor/toast-master/js/jquery.toast.js') }}"></script>
    <!-- dragable sortable -->
    <script src="{{ url('assets/backend/scripts/general.js') }}" type="text/javascript"></script>

	@stack('js')

</body>

</html>
