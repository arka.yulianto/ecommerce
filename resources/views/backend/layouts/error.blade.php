<!doctype html>
<html lang="{{ app()->getLocale() }}" class="fullscreen-bg">

<head>
	<title>@yield('title') | {{ config('app.name', 'Ecommerce') }}</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('assets/backend/vendor/font-awesome/css/font-awesome.min.css') }}">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="{{ url('assets/backend/css/main.css') }}">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{ url('assets/backend/css/demo.css') }}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="{{ url('assets/backend/img/apple-icon.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ url('assets/backend/img/favicon.png') }}">
</head>

<body class="page-error">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			@yield('content')
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
