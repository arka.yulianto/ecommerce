<button type="button" class="btn btn-xs btn-link btn-toggle-fullwidth">
	<span class="sr-only">Toggle Fullwidth</span>
	<i class="fa fa-angle-left"></i>
</button>
<div class="sidebar-scroll">
	<div class="user-account">
		<img src="{{ auth()->user()->user_data->picture }}" class="img-responsive img-circle user-photo" alt="User Profile Picture">
		<div class="dropdown">
			<a href="#" class="dropdown-toggle user-name" data-toggle="dropdown">Hello, <strong>{{ auth()->user()->user_data->first_name }}</strong> <i class="fa fa-caret-down"></i></a>
			<ul class="dropdown-menu dropdown-menu-right account">
				<li><a href="#">My Profile</a></li>
				<li><a href="#">Messages</a></li>
				<li><a href="#">Settings</a></li>
				<li class="divider"></li>
				<li><a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    Logout
                </a></li>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
			</ul>
		</div>
	</div>
	<nav id="left-sidebar-nav" class="sidebar-nav">
		<!-- <ul id="main-menu" class="metismenu">
			<li class="active"><a href="{{ url('admin/dashboard') }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
			<li class="">
				<a href="javascript:void(0)" class="has-arrow" aria-expanded="false"><i class="lnr lnr-layers"></i> <span>Product</span></a>
				<ul aria-expanded="true">
					<li class=""><a href="{{ url('admin/product') }}">All Product</a></li>
					<li class=""><a href="{{ url('admin/category') }}">Category</a></li>
					<li class=""><a href="{{ url('admin/tag') }}">Tag</a></li>
				</ul>
			</li>
			<li class=""><a href="{{ url('admin/order') }}"><i class="lnr lnr-cart"></i><span>Order</span>  <span class="badge bg-success">2</span></a></li>
			<li class=""><a href="{{ url('admin/coupon') }}"><i class="fa fa-ticket"></i> <span>Coupon</span></a></li>
			<li class=""><a href="{{ url('admin/review') }}"><i class="lnr lnr-bubble"></i> <span>Review</span></a></li>
			<li class=""><a href="{{ url('admin/menu') }}"><i class="lnr lnr-menu"></i> <span>Menu</span></a></li>
			<li class="">
				<a href="javascript:void(0)" class="has-arrow" aria-expanded="false"><i class="fa fa-cog"></i> <span>Settings</span></a>
				<ul aria-expanded="true">
					<li class=""><a href="{{ url('admin/user') }}">User</a></li>
					<li class=""><a href="{{ url('admin/role') }}">Role</a></li>
					<li class=""><a href="{{ url('admin/permission') }}">Permission</a></li>
				</ul>
			</li>
		</ul> -->
		{!! $MyNavBar->asUl(['class' => 'metismenu', 'id' => 'main-menu']) !!}
	</nav>
</div>