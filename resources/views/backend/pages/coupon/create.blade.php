@extends('backend.layouts.master')
@section('title')
Add Coupon
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Add Coupon</h1>
	<div class="pull-right">
		<a href="{{ route('coupon.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>


<form action="{{ route('coupon.store') }}" method="post" id="form-add-edit-coupon">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<div class="panel-content">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Coupon Code <span class="text-danger">*</span></label>
						<input type="text" name="code" placeholder="Unique Code" class="form-control" required="required">
						<span class="help-block"></span>
					</div>

					<div class="form-group">
						<label class="control-label">Amount <span class="text-danger">*</span></label>
						<input type="text" name="amount" placeholder="0.00" class="form-control autonumeric" required="required">
						<span class="help-block"></span>
					</div>

					<div class="form-group">
						<label class="control-label">Type <span class="text-danger">*</span></label>
						<select name="type" class="form-control">
							<option value="1">Percentage</option>
							<option value="2">Fixed</option>
						</select>
						<span class="help-block"></span>
					</div>

					<div class="form-group">
						<label class="control-label">Valid Thru</label>
						<input type="text" name="valid_thru" placeholder="yyyy-mm-dd" class="form-control datepicker">
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">products</label>
						<select name="products[]" class="select2 form-control" multiple="multiple" data-placeholder="Specific Categories">
							@foreach ($products as $data_product)
							<option value="{{ $data_product->id }}">{{ $data_product->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">categories</label>
						<select name="categories[]" class="select2 form-control" multiple="multiple" data-placeholder="Specific Categories Exception">
							@foreach ($categories as $data_category)
							<option value="{{ $data_category->id }}">{{ $data_category->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">Exclude Products</label>
						<select name="exclude_products[]" class="select2 form-control" multiple="multiple" data-placeholder="Specific Products">
							@foreach ($products as $data_product)
							<option value="{{ $data_product->id }}">{{ $data_product->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">Exclude Categories</label>
						<select name="exclude_categories[]" class="select2 form-control" multiple="multiple" data-placeholder="Specific Products Exception">
							@foreach ($categories as $data_category)
							<option value="{{ $data_category->id }}">{{ $data_category->name }}</option>
							@endforeach
						</select>
					</div>

				</div>

				<div class="col-md-12">
					<hr>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label">Limit User</label>
						<input type="text" name="limit_user" class="form-control number" placeholder="0">
					</div>

					<div class="form-group">
						<label class="control-label">Limit Coupon</label>
						<input type="text" name="limit_coupon" class="form-control number" placeholder="0">
					</div>

					<div class="form-group">
						<label class="control-label">Limit Item</label>
						<input type="text" name="limit_item" class="form-control number" placeholder="0">
					</div>
				</div>

				<div class="col-md-4">
					<div class="form-group">
						<label class="control-label">Min Spend</label>
						<input type="text" name="minimum_spend" class="form-control autonumeric" placeholder="0.00">
					</div>

					<div class="form-group">
						<label class="control-label">Max Spend</label>
						<input type="text" name="maximum_spend" class="form-control autonumeric" placeholder="0.00">
					</div>

				</div>

				<div class="col-md-4">

					<label class="fancy-checkbox custom-bgcolor-green">
						<input type="checkbox" name="individual_use_only" value="1">
						<span>Individual Use Only</span>
					</label>
					<br>
					<label class="fancy-checkbox custom-bgcolor-green">
						<input type="checkbox" name="exclude_sale_items" value="1">
						<span>Exclude Sale Items</span>
					</label>
					<br>
					<label class="fancy-checkbox custom-bgcolor-green">
						<input type="checkbox" name="allow_free_shipping" value="1">
						<span>Allow Free Shipping</span>
					</label>

				</div>

				<div class="col-md-12">
					<hr>
					<div class="pull-right">
						<button class="btn btn-default" type="reset">Reset</button>
						<button class="btn btn-primary" type="submit">Save</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</form>

@endsection

@push('js')
<script src="{{ url('assets/backend/pages/js/coupon-add-edit.js') }}"></script>
@endpush