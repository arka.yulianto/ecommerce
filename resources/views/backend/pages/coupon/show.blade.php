@extends('backend.layouts.master')
@section('title')
{{ $coupon->code }}
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">{{ $coupon->code }}</h1>
	<div class="pull-right">
		<a href="{{ route('coupon.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel-content">
			<table class="table table-primary table-custom">
				<tr class="info">
					<td colspan="2">Coupon Information</td>
					<td colspan="2">Usage Restriction</td>
				</tr>
				<tr>
					<td><strong>Discount Type</strong></td>
					<td>{{ $coupon->type == 1 ? 'Percentage' : 'Fixed' }}</td>

					<td><strong>Minimum Spend</strong></td>
					<td>{{ number_format($coupon->minimum_spend, 2, '.', ',') }}</td>
				</tr>

				<tr>
					<td><strong>Coupon Amount</strong></td>
					<td>{{ number_format($coupon->amount, 2, '.', ',') }}</td>

					<td><strong>Maximum Spend</strong></td>
					<td>{{ number_format($coupon->maximum_spend, 2, '.', ',') }}</td>
				</tr>

				<tr>
					<td><strong>Allow Free Shipping</strong></td>
					<td>{{ $coupon->allow_free_shipping == 1 ? 'Yes' : 'No' }}</td>

					<td><strong>Individual use only</strong></td>
					<td>{{ $coupon->individual_use_only == 1 ? 'Yes' : 'No' }}</td>
				</tr>

				<tr>
					<td><strong>Coupon Expire Date</strong></td>
					<td>{{ $coupon->valid_thru }}</td>

					<td><strong>Exclude sale item</strong></td>
					<td>{{ $coupon->exclude_sale_item == 1 ? 'Yes' : 'No' }}</td>
				</tr>

				<tr>
					<td colspan="2" class="info">Usage Limits</td>

					<td style="border-right: none !important"><strong>Products</strong></td>
					<td>{{ App\Product::whereIn('id', explode(',', $coupon->products))->pluck('name')->implode(', ') }}</td>
				</tr>

				<tr>
					<td><strong>Usage Limit Per Coupon</strong></td>
					<td>{{ $coupon->limit_user }}</td>

					<td><strong>Products Exclude</strong></td>
					<td>{{ App\Product::whereIn('id', explode(',', $coupon->exclude_products))->pluck('name')->implode(', ') }}</td>
				</tr>

				<tr>
					<td><strong>Usage Limit Per User</strong></td>
					<td>{{ $coupon->limit_coupon }}</td>

					<td><strong>Categories</strong></td>
					<td>{{ App\Category::whereIn('id', explode(',', $coupon->categories))->pluck('name')->implode(', ') }}</td>
				</tr>

				<tr>
					<td><strong>Usage Limit Per Item</strong></td>
					<td>{{ $coupon->limit_item }}</td>

					<td><strong>Categories Exclude</strong></td>
					<td>{{ App\Category::whereIn('id', explode(',', $coupon->exclude_categories))->pluck('name')->implode(', ') }}</td>
				</tr>

			</table>
		</div>
	</div>
</div>

@endsection