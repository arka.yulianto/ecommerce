@extends('backend.layouts.master')
@section('title')
Edit New Product
@endsection
@section('content')

	<div class="section-heading">
		<h1 class="page-title">Edit Product</h1>
		<div class="pull-right">
			<a href="{{ route('product.index') }}" class="btn btn-primary">Back</a>
		</div>
	</div>

	<form action="{{ route('product.update', $product->id) }}" method="post" id="form-add-edit-product">
		{{ csrf_field() }}

		{{ method_field('PUT') }}

		<div class="row">
			<div class="col-md-8">
				<div class="panel-content">
					<div class="form-group">
						<input type="text" name="name" value="{{ $product->name }}" placeholder="Product Name *" class="form-control input-lg" required="required">
					</div>
					<div class="form-group">
						<textarea class="tinymce" placeholder="Content" name="description">{{ $product->description }}</textarea>
					</div>

					<div class="row" style="margin-top: 40px">
						<div class="col-md-4">

							<h3 class="heading"><i class="fa fa-angle-right"></i>Product Image</h3>

							@if(!empty($product->media_id))

								@php ($is_image_exists = file_exists(public_path('uploads/images/150x150/'.$product->media->name)) ? true : false )

							@else

								@php($is_image_exists = 0)

							@endif

							<img class="img img-responsive img-thumbnail" id="image" {{ $is_image_exists ? 'src='.url('uploads/images/150x150/'.$product->media->name) : 'style=display:none' }}>
								<input type="text" name="media_id" value="{{ $product->media_id }}" hidden="hidden">
								<button type="button" class="btn btn-link text-danger btn-sm" onclick="on_remove_image()" id="btn-remove-img" {{ !$is_image_exists ? 'style=display: none' : '' }}>Remove</button>

							<button type="button" class="btn btn-link btn-sm" onclick="open_modal()">Select Image</button>

						</div>

						<div class="col-md-8">
							<h3 class="heading"><i class="fa fa-angle-right"></i>Gallery</h3>
							<div class="row no-gutter" id="gallery-wrapper">
								@if ($product->galleries->count() > 0)
									@foreach ($product->galleries as $data_galleries)
									<div class="col-md-4" style="margin-bottom: 10px">
										<img src="{{ url('uploads/images/150x150/'.$data_galleries->name) }}" class="img img-responsive img-thumbnail">
										<input type="text" value="{{ $data_galleries->id }}" hidden="hidden" name="galleries[]">
									</div>
									@endforeach
								@endif
							</div>
							<button type="button" class="btn btn-link btn-sm" onclick="on_create_gallery()">Edit Gallery</button>
						</div>

						<div class="col-md-12">
							<hr>
							<h3 class="heading"><i class="fa fa-angle-right"></i>Other Attributes</h3>
							<table class="table table-bordered" id="tbl-attribute">
								<thead>
									<tr>
										<th>Attrubut Name</th>
										<th style="width: 400px">Attribute Value</th>
										<th>
											<button class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Add Row" id="btn-add-row" type="button"><i class="fa fa-plus"></i></button>
										</th>
									</tr>
								</thead>
								<tbody>
									@php($i = 0)
									@if ($product->attributes->count() > 0)
										@foreach ($product->attributes as $data_attributes)
											<tr id="{{ $i }}">
												<td>
													<input type="text" name="attribute_name[]" class="form-control" placeholder="eg: Color" value="{{ $data_attributes->attribute_name }}">
												</td>
												<td>
													<select name="attribute_value[{{ $i }}][]" class="select2 attribute_value" data-tags="true" multiple="multiple" data-token-separators="[',', ' ']">
														@foreach (explode(',', $data_attributes->attribute_value) as $value)
															<option value="{{ $value }}" selected="selected">{{ $value }}</option>
														@endforeach
													</select>
												</td>
												<td>
													<button class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Remove Row" type="button" onclick="remove_row({{ $i }})"><i class="fa fa-times"></i></button>
												</td>
											</tr>
										@php($i++)
										@endforeach
									@else
									<tr id="no-data">
										<td colspan="3" class="text-center">No data</td>
									</tr>
									@endif
								</tbody>
							</table>
						</div>

					</div>

				</div>

			</div>

			<div class="col-md-4">
				<div class="panel-content">
					<h3 class="heading"><i class="fa fa-angle-right"></i>Price <span class="text-danger">*</span></h3>
					<div class="form-group">
						<div class="input-group">
						  	<span class="input-group-addon" id="basic-addon1">
					            {{ $currency->symbol }}
						  	</span>
							<input type="text" name="price" value="{{ Helper::getCurrency($product->price) }}" placeholder="0.0" class="form-control autonumeric" required="required">
						</div>
					</div>
				
					<h3 class="heading"><i class="fa fa-angle-right"></i>Discount Price</h3>
					<div class="form-group">
						<div class="input-group">
						  	<span class="input-group-addon" id="basic-addon1">
					            {{ $currency->symbol }}
						  	</span>
							<input type="text" name="discount" value="{{ Helper::getCurrency($product->discount) }}" placeholder="0.0" class="form-control autonumeric">
						</div>
					</div>
				
					<h3 class="heading"><i class="fa fa-angle-right"></i><abbr title="Stock Keeping Unit">SKU</abbr></h3>
					<div class="form-group">
						<input type="text" name="sku" value="{{ $product->sku }}" placeholder="Unique Code" class="form-control">
					</div>
				
					<h3 class="heading"><i class="fa fa-angle-right"></i>Stock</h3>
					<div class="form-group">
						<input type="text" name="stock" value="{{ $product->stock }}" placeholder="0" class="form-control number">
					</div>
				
					<h3 class="heading"><i class="fa fa-angle-right"></i>Shipping</h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" name="weight" value="{{ $product->weight }}" placeholder="Weight (GR)" class="form-control number">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" name="height" value="{{ $product->height }}" placeholder="Dimension (CM)" class="form-control number">
							</div>
						</div>
					</div>


					<h3 class="heading"><i class="fa fa-angle-right"></i> Categories</h3>
					<div class="form-group">
						<select name="category_id[]" data-placeholder="Select Categories" class="form-control select2" multiple="multiple">
							<option value="0">&#43; &nbsp; Add New Category</option>
							@foreach ($categories as $data_categories)
							<option value="{{ $data_categories->id }}" {{ in_array($data_categories->id, $product->categories->pluck('id')->toArray()) ? 'selected=selected' : '' }}>{{ $data_categories->name }}</option>
							@endforeach
						</select>
					</div>

					<h3 class="heading"><i class="fa fa-angle-right"></i> Tags</h3>
					<div class="form-group">
						<select name="tags[]" data-placeholder="Type or Select Tags" class="form-control select2" multiple="multiple" data-tags="true" data-token-separators="[',', ' ']">
							@foreach ($tags as $data_tags)
							<option value="{{ $data_tags['name'] }}" {{ in_array($data_tags['name'], $product->tags->pluck('name')->toArray()) ? 'selected=selected' : '' }}>{{ $data_tags['name'] }}</option>
							@endforeach
						</select>
					</div>

				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">
				<hr>
				<div class="pull-right">
					<button type="reset" class="btn btn-default">Reset</button>
					<button type="submit" class="btn btn-primary" name="remove_levels">Save Changes</button>
				</div>
			</div>
		</div>
	</form>

	<!-- Modal for question -->
	<form id="form-add-category">
		<div class="modal fade in" tabindex="-1" role="dialog" id="modal-add-category">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
		                <h4 class="modal-title">Add new category</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="row">
		            		<div class="col-md-12">
		            			<div class="form-group">
		            				<label class="control-label">Category Name <span class="text-danger">*</span></label>
		            				<input type="text" name="name" class="form-control" placeholder="Name" required="required">
		            				<span class="help-block"></span>
		            			</div>

		            			<div class="form-group">
		            				<label class="control-label">Category Name <span class="text-danger">*</span></label>
		            				<select name="parent_id" class="select2" data-placeholder="Select parent">
		            					
		            				</select>
		            			</div>

		            			<div class="form-group">
		            				<label class="control-label">Description</label>
		            				<textarea type="text" name="description" class="form-control" placeholder="Description"></textarea>
		            				<span class="help-block"></span>
		            			</div>
		            			
		            		</div>
		            	</div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" id="btn-save-category" class="btn btn-primary btn-sm">Save</button>
		                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
		            </div>
		        </div>
		    </div>
		</div>
	</form>

	@include('backend.pages.media.list_data')

	@include('backend.pages.media.gallery')

@endsection

@push('js')

<script src="{{ url('assets/backend/pages/js/media.js') }}"></script>
<script src="{{ url('assets/backend/pages/js/product-create-edit.js') }}"></script>
@endpush