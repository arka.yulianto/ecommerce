@extends('backend.layouts.master')
@section('title')
Create New Product
@endsection
@section('content')

	<div class="section-heading">
		<h1 class="page-title">Create Product</h1>
		<div class="pull-right">
			<a href="{{ route('product.index') }}" class="btn btn-primary">Back</a>
		</div>
	</div>

	<form action="{{ route('product.store') }}" method="post" id="form-add-edit-product">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-md-8">
				<div class="panel-content">
					<div class="form-group">
						<input type="text" name="name" placeholder="Product Name *" class="form-control input-lg" required="required">
						<span class="help-block"></span>
					</div>
					<div class="form-group">
						<textarea class="tinymce" placeholder="Content" name="description"></textarea>
					</div>

					<div class="row" style="margin-top: 40px">
						<div class="col-md-4">
							<h3 class="heading"><i class="fa fa-angle-right"></i>Product Image</h3>
							<img class="img img-responsive img-thumbnail" id="image" style="display: none;">
							<input type="text" name="media_id" hidden="hidden">
							<button type="button" class="btn btn-link text-danger btn-sm" onclick="on_remove_image()" id="btn-remove-img" style="display: none">Remove</button>
							<button type="button" class="btn btn-link btn-sm" onclick="open_modal()">Select Image</button>
						</div>

						<div class="col-md-8">
							<h3 class="heading"><i class="fa fa-angle-right"></i>Gallery</h3>
							<div class="row no-gutter" id="gallery-wrapper">
								
							</div>
							<button type="button" class="btn btn-link btn-sm" onclick="on_create_gallery()">Create Gallery</button>
						</div>

						<div class="col-md-12">
							<hr>
							<h3 class="heading"><i class="fa fa-angle-right"></i>Other Attributes</h3>
							<table class="table table-bordered" id="tbl-attribute">
								<thead>
									<tr>
										<th>Attrubut Name</th>
										<th style="width: 400px">Attribute Value</th>
										<th>
											<button class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Add Row" id="btn-add-row" type="button"><i class="fa fa-plus"></i></button>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr id="no-data">
										<td colspan="3" class="text-center">No data</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>

				</div>

			</div>

			<div class="col-md-4">
				<div class="panel-content">
					<h3 class="heading"><i class="fa fa-angle-right"></i>Price <span class="text-danger">*</span></h3>
					<div class="form-group">
						<div class="input-group">
						  	<span class="input-group-addon" id="basic-addon1">
					            {{ $currency->symbol }}
						  	</span>
							<input type="text" name="price" placeholder="0.0" class="form-control autonumeric" required="required">
							<span class="help-block"></span>
						</div>
					</div>
				
					<h3 class="heading"><i class="fa fa-angle-right"></i>Discount Price</h3>
					<div class="form-group">
						<div class="input-group">
						  	<span class="input-group-addon" id="basic-addon1">
					            {{ $currency->symbol }}
						  	</span>
					 		<input type="text" name="discount" placeholder="0.0" class="form-control autonumeric">
						</div>
					</div>
				
					<h3 class="heading"><i class="fa fa-angle-right"></i><abbr title="Stock Keeping Unit">SKU</abbr></h3>
					<div class="form-group">
						<input type="text" name="sku" placeholder="Unique Code" class="form-control">
					</div>
				
					<h3 class="heading"><i class="fa fa-angle-right"></i>Stock</h3>
					<div class="form-group">
						<input type="text" name="stock" placeholder="0" class="form-control number">
					</div>
				
					<h3 class="heading"><i class="fa fa-angle-right"></i>Shipping</h3>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" name="weight" placeholder="Weight (GR)" class="form-control number">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" name="height" placeholder="Dimension (CM)" class="form-control number">
							</div>
						</div>
					</div>


					<h3 class="heading"><i class="fa fa-angle-right"></i> Categories</h3>
					<div class="form-group">
						<select name="category_id[]" data-placeholder="Select Categories" class="form-control select2" multiple="multiple">
							<option value="0">&#43; &nbsp; Add New Category</option>
						</select>
					</div>

					<h3 class="heading"><i class="fa fa-angle-right"></i> Tags</h3>
					<div class="form-group">
						<select name="tags[]" data-placeholder="Type or Select Tags" class="form-control select2" multiple="multiple" data-tags="true" data-token-separators="[',', ' ']">
							@foreach ($tags as $data_tags)
							<option value="{{ $data_tags['name'] }}">{{ $data_tags['name'] }}</option>
							@endforeach
						</select>
					</div>

				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">
				<hr>
				<div class="pull-right">
					<button type="reset" class="btn btn-default">Reset</button>
					<button type="submit" class="btn btn-primary" name="remove_levels">Save</button>
				</div>
			</div>
		</div>
	</form>

	<!-- Modal for question -->
	<form id="form-add-category">
		<div class="modal fade in" tabindex="-1" role="dialog" id="modal-add-category">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
		                <h4 class="modal-title">Add new category</h4>
		            </div>
		            <div class="modal-body">
		            	<div class="row">
		            		<div class="col-md-12">
		            			<div class="form-group">
		            				<label class="control-label">Category Name <span class="text-danger">*</span></label>
		            				<input type="text" name="name" class="form-control" placeholder="Name" required="required">
		            				<span class="help-block"></span>
		            			</div>

		            			<div class="form-group">
		            				<label class="control-label">Category Name <span class="text-danger">*</span></label>
		            				<select name="parent_id" class="select2" data-placeholder="Select parent">
		            					
		            				</select>
		            			</div>

		            			<div class="form-group">
		            				<label class="control-label">Description</label>
		            				<textarea type="text" name="description" class="form-control" placeholder="Description"></textarea>
		            				<span class="help-block"></span>
		            			</div>
		            		</div>
		            	</div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" id="btn-save-category" class="btn btn-primary btn-sm">Save</button>
		                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
		            </div>
		        </div>
		    </div>
		</div>
	</form>

	@include('backend.pages.media.list_data')

	@include('backend.pages.media.gallery')

@endsection

@push('js')

<script src="{{ url('assets/backend/pages/js/media.js') }}"></script>
<script src="{{ url('assets/backend/pages/js/product-create-edit.js') }}"></script>
@endpush