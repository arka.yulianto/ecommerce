@extends('backend.layouts.master')
@section('title')
{{ $product->name }}
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">{{ $product->name }}</h1>
	<div class="pull-right">
		<a href="{{ route('product.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel-content">
			<div class="row">
				<div class="col-md-4">
					<img src="{{ url('uploads/images/thumbnails/600x600_'.$product->media->name) }}" class="img img-responsive img-thumbnail">
					<div class="clearfix no-gutter" style="margin-top: 20px">
						@foreach ($product->galleries as $data_gallery)
							<div class="col-md-4">
								<img src="{{ url('uploads/images/thumbnails/600x600_'.$data_gallery->name) }}" class="img img-responsive img-thumbnail">
							</div>
						@endforeach
					</div>
				</div>

				<div class="col-md-8">
					@if(!empty($product->discount))
						<s><sup class="text-muted"> {{ $product->price }} </sup></s>
						<h3 class="text-primary" style="display: inline-block; margin-left: 10px">{{ $product->discount }}</h3>
					@else
						<h3 class="text-primary">{{ $product->price}}</h3>
					@endif

					<hr>

					{!! $product->description !!}

					<table class="table table-primary" style="margin-top: 30px">

						<tr>
							<th>SKU</th>
							<td>{{ $product->sku }}</td>
						</tr>

						<tr>
							<th>Stock</th>
							<td>{{ $product->stock }}</td>
						</tr>

						<tr>
							<th>Weight</th>
							<td>{{ $product->weight }} gr</td>
						</tr>

						<tr>
							<th>Dimension</th>
							<td>{{ $product->height }} cm</td>
						</tr>

						<tr>
							<th>Categories</th>
							<td>{{ $product->categories->pluck('name')->implode(', ') }}</td>
						</tr>

						<tr>
							<th>Tags</th>
							<td>{!!  '<span class="badge bg-info">'.$product->tags->pluck('name')->implode('</span>&nbsp;<span class="badge bg-info">') !!}</td>
						</tr>

						@foreach ($product->attributes as $data_attributes)

						<tr>
							<th>{{ $data_attributes->attribute_name }}</th>
							<td>
								{{ collect(explode(',', $data_attributes->attribute_value))->implode(', ') }}
							</td>
						</tr>

						@endforeach

					</table>

				</div>

			</div>
		</div>
	</div>
</div>



@endsection