@extends('backend.layouts.master')
@section('title')
Add Widget
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Add Widget</h1>
	<div class="pull-right">
		<a href="{{ route('widget.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>

<form method="post" action="{{ route('widget.store') }}">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<div class="panel-content">
				<div class="col-md-12">

					<div class="form-group">
						<label class="control-label">Widget Name</label>
						<input type="text" name="name" placeholder="Widget Name" class="form-control">
					</div>

					<div class="form-group">
						<label class="control-label">Widget Type</label>
						<select name="type" class="select2 form-control" data-placeholder="Select Type">
							<option value=""></option>
							<option value="categories">Categories</option>
							<option value="text">Text</option>
							<option value="product">Product</option>
						</select>
					</div>

					<div class="form-group" id="categories" style="display: none">
						<div class="row">
							<div class="col-md-6">
								<label class="control-label">Category Parent</label>
								<select name="category_parent" class="select2 form-control" data-placeholder="Select Parent">
									<option></option>
									@foreach($categories as $category)
										<option value="{{ $category->id }}">{{ $category->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-6">
								<label class="control-label">Show limit</label>
								<input type="text" name="limit" placeholder="0" class="form-control number">
							</div>
						</div>
					</div>

					<div class="form-group" id="text" style="display: none">
						<label class="control-label">Text Content</label>
						<textarea class="form-control" rows="10" name="content"></textarea>
					</div>

					<div class="form-group" id="product" style="display: none">
						<label class="control-label">Category</label>
						<select name="category_id" class="select2 form-control" data-placeholder="Select Parent">
							<option></option>
							@foreach($categories as $category)
								<option value="{{ $category->id }}">{{ $category->name }}</option>
							@endforeach
						</select>
					</div>

				</div>

				<div class="col-md-12 text-right">
					<hr>
					<button class="btn btn-default" type="reset">Reset</button>
					<button class="btn btn-primary" type="submit">Save</button>
				</div>

			</div>
		</div>
	</div>
</form>

@endsection

@push('js')
<script src="{{ url('assets/backend/pages/js/widget-add-edit.js') }}"></script>
@endpush