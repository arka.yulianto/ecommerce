@extends('backend.layouts.master')
@section('title')
Widget
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Widget</h1>
	<div class="pull-right">
		<a href="{{ route('widget.create') }}" class="btn btn-primary">Add new</a>
	</div>
</div>

<div class="row" style="margin-top: 100px">

	@foreach ($widgets as $widget)
	<div class="col-md-4">
		<div class="panel panel-default">
		  <div class="panel-heading">
		  	{{ $widget->name }}
		  	<div class="pull-right" style="display: inline-block;">
		  		<a class="btn btn-success btn-xs" href="{{ route('widget.edit', $widget->id) }}"><i class="fa fa-pencil"></i></a>
		  		<form style="display: inline-block;" class="form-inline" action="{{ route('widget.destroy', $widget->id) }}" method="post">
		  			{{ csrf_field() }}
		  			{{ method_field('DELETE') }}
		  			<button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-times"></i></a>
		  			</button>
		  		</form>
	  		</div>
		  </div>
		  	<div class="panel-body">
		  		{{ $widget->type }}
		  	</div>
		</div>
	</div>
	@endforeach

</div>

@endsection

@push('js')

@if(session()->has('message'))
<script type="text/javascript">
	show_notification("{{ session('title') }}","{{ session('type') }}","{{ session('message') }}");
</script>
@endif

@endpush
