@extends('backend.layouts.master')
@section('title')
Menu
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Menu</h1>
</div>

<div class="row">
	<div class="col-md-4">
		<div class="panel-content">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Category</h3>
					<span class="pull-right clickable"><i class="fa fa-angle-up"></i></span>
				</div>
				<div class="panel-body">
					@foreach ($categories as $data_category)
						<label class="fancy-checkbox custom-bgcolor-green">
							<input type="checkbox" name="category[]" value="{{ $data_category->id }}">
							<span>{{ $data_category->name }}</span>
						</label>
						<br>
					@endforeach

					<hr>
					<div class="pull-right">
						<button type="button" class="btn btn-primary btn-add" data-type="category">Add to menu</button>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">URL</h3>
					<span class="pull-right clickable"><i class="fa fa-angle-up"></i></span>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<input type="text" name="name" placeholder="Menu Name" class="form-control">
						<br>
						<input type="text" name="url" placeholder="http://..." class="form-control">
						<br>
						<label class="fancy-checkbox custom-bgcolor-green">
							<input type="checkbox" name="is_mega" value="1">
							<span>Is Mega Menu?</span>
						</label>
						<hr>
						<div class="pull-right">
							<button type="button" class="btn btn-primary btn-add" data-type="url">Add to menu</button>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Widget (only for mega menu)</h3>
					<span class="pull-right clickable"><i class="fa fa-angle-up"></i></span>
				</div>
				<div class="panel-body">
					<input type="text" name="widget_name" placeholder="Widget Name" class="form-control">
					<br>
					<select name="widget_id" class="select2" data-placeholder="Select Widget">
						<option></option>
						@foreach ($widgets as $widget)
						<option value="{{ $widget->id }}">{{ $widget->name }}</option>
						@endforeach
					</select>
					<br>
					<br>
					<textarea name="tag_open" class="form-control" placeholder="Tag Open"></textarea>
					<br>
					<textarea name="tag_close" class="form-control" placeholder="Tag Close"></textarea>
					<hr>
					<div class="pull-right">
						<button type="button" class="btn btn-primary btn-add" data-type="widget">Add to menu</button>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="col-md-8" style="overflow: hidden; min-height: 900px">
		<div class="panel-content" id="menu-content">
			
		</div>
	</div>

	
</div>

<!-- Modal for question -->
<form id="form-edit">
	<div class="modal fade in" tabindex="-1" role="dialog" id="modal-edit">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
	                <h4 class="modal-title">Edit Menu</h4>
	            </div>
	            <div class="modal-body">
	        		<div class="col-md-12">
	        			<div class="form-group">
	        				<label class="control-label">Menu Name <span class="text-danger">*</span></label>
	        				<input type="text" name="name" class="form-control" required="required">
	        			</div>
	        			<div class="form-group">
	        				<label class="control-label">Menu URL <span class="text-danger">*</span></label>
	        				<input type="text" name="url" class="form-control" required="required">
	        			</div>
	        			<div class="form-group">
	        				<label class="control-label">Parent <span class="text-danger">*</span></label>
	        				<select name="parent_id" class="form-control select2" required="required">
	        					
	        				</select>
	        			</div>
	        			<div class="form-group">
	        				<label class="control-label">Order# <span class="text-danger">*</span></label>
	        				<input type="text" name="order_number" class="form-control" required="required">
	        			</div>
	        		</div>
	        		<div class="clearfix"></div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" id="btn-save" class="btn btn-primary btn-sm">Save Changes</button>
	                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
	            </div>
	        </div>
	    </div>
	</div>
</form>

@endsection

@push('js')

<script src="{{ url('assets/backend/pages/js/menu.js') }}"></script>

@endpush