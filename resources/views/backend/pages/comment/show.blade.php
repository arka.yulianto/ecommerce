@extends('backend.layouts.master')
@section('title')
	{{ $comment->user->user_data->fullname }}'s review
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">{{ $comment->user->user_data->fullname }}'s review</h1>
	<div class="pull-right">
		<a href="{{ route('review.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel-content">
			<div class="col-md-2 text-center">
				<img src="{{ $comment->user->user_data->picture }}" class="img img-circle img-thumbnail" alt="{{ $comment->user->user_data->fullname }}">
				<div style="margin-top: 20px">
					{!! Helper::getRate($comment->rate) !!}
				</div>
			</div>
			<div class="col-md-10">
				<blockquote>
				  <p>{{ $comment->content }}</p>
				  <footer> {{ $comment->user->user_data->fullname }} @ <cite title="Source Title">{{ Carbon\Carbon::parse($comment->created_at)->format('F jS, Y') }}</cite></footer>
				</blockquote>
			</div>
		</div>
	</div>
</div>

@endsection