@extends('backend.layouts.master')
@section('title')
User
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">User</h1>
	<div class="pull-right">
		<a href="{{ route('user.create') }}" class="btn btn-primary">Add new</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel-content">
			@foreach ($users as $data_user)
				@if (count($data_user->user_data) > 0)
				<div class="col-md-3 text-center">
					<div class="profile">
						<img src="{{ $data_user->user_data->picture }}" alt="{{ $data_user->user_data->fullname }}" class="img img-circle img-thumbnail img-profile">
						<h4 class="user-title">{{ $data_user->name }}</h4>
						<p>{{ $data_user->roles()->pluck('display_name')->implode(', ') }}</p>
						<a href="{{ route('user.edit', $data_user->id) }}" class="btn btn-link text-success" data-toggle="tooltip" data-original-title="Edit"><i class="lnr lnr-pencil"></i></a>
						<button type="button" class="btn btn-link text-danger" data-toggle="tooltip" data-original-title="Delete" onclick="on_delete('{{ $data_user->id }}')"><i class="lnr lnr-cross"></i></button>
						<a href="{{ route('user.show', $data_user->id) }}" class="btn btn-link text-primary" data-toggle="tooltip" data-original-title="View"><i class="lnr lnr-eye"></i></a>
					</div>
				</div>
				@endif
			@endforeach
			<div class="col-md-12">
				{{ $users->links() }}
			</div>
		</div>
	</div>
</div>

@endsection

@push('js')
@if (session()->has('message'))
<script type="text/javascript">
	show_notification("{{ session('title') }}","{{ session('type') }}","{{ session('message') }}");
</script>
@endif
@endpush