@extends('backend.layouts.master')
@section('title')
{{ $user->user_data->fullname }}
@endsection
@section('content')

	<div class="section-heading">
		<h1 class="page-title">{{ $user->user_data->fullname }}</h1>
		<div class="pull-right">
			<a href="{{ route('user.index') }}" class="btn btn-primary">Back</a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel-content">
				<div class="row">
					<div class="col-md-2 text-center">
						<img src="{{ $user->user_data->picture }}" class="img img-responsive img-circle img-thumbnail" alt="{{ $user->user_data->fullname }}">
						<hr>
						<p>
							{{ '@'.$user->name }}
						</p>
						<p>{{ $user->roles()->pluck('display_name')->implode(', ') }}</p>
					</div>
					<div class="col-md-10">
						<table class="table">
							<thead>
								<tr>
									<td>Fullname</td>
									<td>{{ $user->user_data->fullname }}</td>
								</tr>
								<tr>
									<td>Email</td>
									<td>{{ $user->email }}</td>
								</tr>
								<tr>
									<td>Phone Number</td>
									<td>{{ $user->user_data->fullname }}</td>
								</tr>
								<tr>
									<td>Identity Number</td>
									<td>{{ $user->user_data->identity_number }}</td>
								</tr>
								<tr>
									<td>Gender</td>
									<td>{{ $user->user_data->sex }}</td>
								</tr>
								<tr>
									<td>pob, dob</td>
									<td>{{ $user->user_data->place_of_birth }}, {{ Carbon\Carbon::parse($user->user_data->date_of_birth)->format('F jS, Y') }}</td>
								</tr>
								<tr>
									<td>Address</td>
									<td>{{ $user->user_data->address }}</td>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection