@extends('backend.layouts.master')
@section('title')
Edit user
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Edit user</h1>
	<div class="pull-right">
		<a href="{{ route('user.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>

<form action="{{ route('user.update', $user->id) }}" method="post" id="form-add-edit">
{{ csrf_field() }}
{{ method_field('PUT') }}
	<div class="row">
		<div class="col-md-12">
			<div class="panel-content">
				<div class="row">

					<div class="col-md-9">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Nick Name <span class="text-danger">*</span></label>
									<input type="text" name="name" value="{{ $user->name }}" class="form-control" placeholder="eg: John" required="required">
									<span class="help-block"></span>
								</div>
								<div class="form-group">
									<label class="control-label">Email<span class="text-danger">*</span></label>
									<input type="email" name="email" class="form-control" placeholder="youremail@example.com" required="required" value="{{ $user->email }}">
									<span class="help-block"></span>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="control-label">Password</label>
									<input type="password" name="password" class="form-control" placeholder="***">
								</div>
								<div class="form-group">
									<label class="control-label">Role<span class="text-danger">*</span></label>
									<select name="role_id" class="select2 form-control" data-placeholder="select role" required="required">
										@foreach ($roles as $data_role)
										<option value="{{ $data_role->id }}">{{ $data_role->name }}</option>
										@endforeach
									</select>
									<span class="help-block"></span>
								</div>
							</div>

						</div>
					</div>
					<div class="col-md-3">
						<div class="form-gorup avatar-wrapper">
							<label class="control-label">Avatar</label>

							<div class="small-drag-drop" id="another-upload-wrapper">
								<p>Drag and drop file here <br> or</p>
								<button class="btn btn-primary btn-sm" type="button" id="browse-file">Browse File</button>
							</div>
						</div>

						<div class="preview">
							<div class="template">
								<div class="remove">
									<button class="btn btn-danger btn-xs btn-circle" type="button" data-dz-remove><i class="lnr lnr-cross"></i></button>
								</div>

								<img data-dz-thumbnail class="thumbail-preview img img-responsive img-thumbnail">

								<div class="progress-wrapper">
									<div class="progress progress-striped active" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
						          		<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
							        </div>
							    </div>

							</div>
						</div>

						<input type="text" name="avatar" hidden="hidden">

					</div>

					<div class="col-md-12">
						<hr>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Fullname</label>
									<input type="text" name="fullname" value="{{ $user->user_data->fullname }}" placeholder="eg: John Doe" class="form-control">
								</div>

								<div class="form-group">
									<label class="control-label">Identity number</label>
									<input type="text" name="identity_number" value="{{ $user->user_data->identity_number }}" placeholder="ID Card Number" class="form-control">
								</div>


								<div class="form-group">
									<label class="control-label">Phone number</label>
									<input type="text" name="phone_number" value="{{ $user->user_data->phone_number }}" placeholder="eg: 0812345689" class="form-control number">
								</div>

							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Gender</label>
									<select name="sex" class="select2">
										<option value="Male">Male</option>
										<option value="Female">Female</option>
									</select>
								</div>

								<div class="form-group">
									<label class="control-label">Place of birth</label>
									<input type="text" name="place_of_birth" value="{{ $user->user_data->place_of_birth }}" placeholder="eg: Jakarta" class="form-control">
								</div>


								<div class="form-group">
									<label class="control-label">Date of birth</label>
									<input type="text" name="date_of_birth" value="{{ $user->user_data->date_of_birth }}" placeholder="yyyy-mm-dd" class="form-control datepicker">
								</div>

							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label class="control-label">Address</label>
									<textarea class="form-control" name="address" rows="8" placeholder="St. No St. name, District - City, Postal Code">{{ $user->user_data->address }}</textarea>
								</div>
							</div>


						</div>
					</div>

					<div class="col-md-12">
						<hr>
						<div class="pull-right">
							<button class="btn btn-default" type="reset">Reset</button>
							<button class="btn btn-primary" type="button" id="btn-save">Save</button>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</form>

@endsection

@push('js')
<script src="{{ url('assets/backend/pages/js/user-create-edit.js') }}"></script>
@endpush