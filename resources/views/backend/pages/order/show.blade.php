@extends('backend.layouts.master')
@section('title')
#{{ $order->order_number }}
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">#{{ $order->order_number }}</h1>
	<div class="pull-right">
		@if ($order->status == 0)
		<button class="btn btn-primary" id="btn-update">Update Status</button>
		@endif
		<a href="{{ route('order.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="panel-content">	
			@if (session()->has('message'))
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Well Done!</strong> {{ session()->get('message') }}
				</div>
			@endif
			<div class="row">
				<div class="col-md-4">
					<strong>Order Number</strong>
					<h3 class="text-muted">#{{ $order->order_number }}</h3>
				</div>

				<div class="col-md-4">
					<strong>Amount</strong>	
					<h3 class="text-muted">{{ Helper::currency($order->total) }}</h3>
				</div>

				<div class="col-md-4">
					<strong>Payment Method</strong>
					<h3 class="text-muted">{{ $order->payment_method }}</h3>
				</div>

			</div>

			<div class="row">
				<div class="col-md-12">
					<hr>
					<table class="table table-primary table-custom">
						<thead>
							<tr class="alert-success">
								<td colspan="2">Order Details</td>
								<td colspan="2">Customer Information</td>
							</tr>
							<tr>
								<td><strong>Order Number</strong></td>
								<td>{{ $order->order_number }}</td>

								<td><strong>Name</strong></td>
								<td>{{ $order->user->user_data->fullname }}</td>
							</tr>
							<tr>
								<td><strong>Payment Method</strong></td>
								<td>{{ $order->payment_method }}</td>

								<td><strong>Phone Number</strong></td>
								<td>{{ $order->user->user_data->phone_number }}</td>
							</tr>
							<tr>
								<td><strong>Transaction Date</strong></td>
								<td>{{ Carbon\Carbon::parse($order->created_at)->format('F jS, Y h:i A') }}</td>

								<td><strong>Email</strong></td>
								<td>{{ $order->user->email }}</td>
							</tr>
							<tr>
								<td><strong>Transaction Status</strong></td>
								<td>{!! Helper::getStatusTransaction($order->status) !!}</td>

								<td><strong>Address</strong></td>
								<td>
									{{ $order->user->user_data->address }}, {{ $order->user->user_data->zip }}
									<br>
									{{ Helper::getProvince($order->user->user_data->province_id) }}
									<br>
									{{ Helper::getCity($order->user->user_data->city_id) }}
									<br>
									{{ $order->user->user_data->country_id }}
								</td>
							</tr>

						</thead>
					</table>
				</div>

				<div class="col-md-12">
					
					<table class="table table-primary table-custom">
						<thead>
							<tr class="alert-info">
								<td colspan="4">Shipping Info #{{ $order->shipping->receipt_number }}</td>
							</tr>
							<tr>
								<td><strong>Shipping To</strong></td>
								<td>{{ $order->shipping->first_name.' '.$order->shipping->last_name }}</td>
								<td><strong>Shipping Phone Number</strong></td>
								<td>
									{{ $order->shipping->phone_number }}
								</td>
							</tr>
							<tr>
								<td><strong>Shipping Address</strong></td>
								<td>
									{{ $order->shipping->address }}
			                        <br>
			                        {{ Helper::getCity($order->shipping->city_id) }}
			                        <br>
			                        {{ Helper::getProvince($order->shipping->province_id) }}
			                        <br>
			                        {{ $order->shipping->country_id }}
			                        <br>
								</td>
								<td><strong>Courier Details</strong></td>
								<td>
									{{ strtoupper($order->shipping->courier_id) }}
			                        <br>
		                         	{{ $order->shipping->service_name }} ({{ $order->shipping->service_description }})
			                        <br>
			                        {{ $order->shipping->estimate_delivery }} (Days)
								</td>
							</tr>
						</thead>
					</table>
						
				</div>

				<div class="col-md-12">
					<h3 class="heading">Transaction Details</h3>
					<table class="table table-primary table-bordered">
						<thead class="alert-info">
							<tr>
								<th>Product Name</th>
								<th class="text-right">Price</th>
								<th class="text-center">Qty</th>
								<th class="text-right">Coupon</th>
								<th>Attributes</th>
								<th class="text-right">Total</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($order->details as $data_details)
							<tr>
								<td>{{ $data_details->product->name }}</td>
								<td class="text-right">{{ Helper::currency($data_details->price) }}</td>
								<td class="text-center">{{ $data_details->qty }}</td>
								<td class="text-right">{{ $data_details->coupon_id }}</td>
								<td>

									@foreach ($data_details->attributes as $attribute)

										{{ $attribute->attribute_name }} : {{ $attribute->attribute_value }} <br>

									@endforeach

								</td>
								<td class="text-right">{{ Helper::currency($data_details->total) }}</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<th colspan="5" class="text-right">Sub Total</th>
								<th class="text-right">{{ Helper::currency($order->sub_total) }}</th>
							</tr>
							<tr>
								<th colspan="5" class="text-right">Tax</th>
								<th class="text-right">{{ Helper::currency($order->tax) }}</th>
							</tr>
							<tr>
								<th colspan="5" class="text-right">Shipping Fee</th>
								<th class="text-right">{{ Helper::currency($order->shipping->cost) }}</th>
							</tr>
							<tr>
								<th colspan="5" class="text-right">Total</th>
								<th class="text-right">{{ Helper::currency($order->total) }}</th>
							</tr>
						</tfoot>
					</table>
				</div>

			</div>
			
		</div>
	</div>
</div>

<form action="{{ route('order.update', $order->id) }}" method="post">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
	<div class="modal fade" tabindex="-1" role="dialog" id="modal-update">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Update Status</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
	        			<label class="control-label">Status</label>
	        			<select name="status" class="select2" data-placeholder="Change Status">
	        				<option></option>
	        				<option value="1">Challange</option>
	        				<option value="2">Success</option>
	        				<option value="3">Fraud</option>
	        			</select>
	        		</div>
	        		<div class="form-group">
	        			<label class="control-label">Receipt Number</label>
	        			<input type="text" name="receipt_number" class="form-control" placeholder="Courier Receipt Number">
	        		</div>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary">Save changes</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>

@endsection

@push('js')
<script src="{{ url('assets/backend/pages/js/order-show.js') }}"></script>
@endpush