@extends('backend.layouts.master')
@section('title')
Order
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Order</h1>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="panel-content">
			<table class="table table-primary table-bordered" id="table-order">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Date & Time</th>
						<th>Payment Method</th>
						<th>Status</th>
						<th>Total Order</th>
						<th>Option</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

@endsection

@push('js')
<script src="{{ url('assets/backend/pages/js/order.js') }}"></script>
@endpush