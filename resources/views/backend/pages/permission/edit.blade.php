@extends('backend.layouts.master')
@section('title')
Edit Permission
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Edit Permission</h1>
	<div class="pull-right">
		<a href="{{ route('permission.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>

<form action="{{ route('permission.update', $permission->id) }}" method="post" id="form-add-edit">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
	<div class="row">
		<div class="col-md-12">
			<div class="panel-content">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Permission Name<span class="text-danger">*</span></label>
						<input type="text" name="name" value="{{ $permission->name }}" class="form-control" placeholder="eg: manage-user" required="required">
						<span class="help-block"></span>
					</div>

					<div class="form-group">
						<label class="control-label">Display Name<span class="text-danger">*</span></label>
						<input type="text" name="display_name" value="{{ $permission->display_name }}" class="form-control" placeholder="eg: User" required="required">
						<span class="help-block"></span>
					</div
					>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Parent</label>
						<select name="parent_id" class="select2 form-control" data-placeholder="Select Parent">
							<option value="0">No Parent</option>
							@foreach ($permissions as $parent)
							<option value="{{ $parent->id }}" {{ $parent->id == $permission->parent_id ? 'selected=selected' : '' }}>{{ $parent->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">Description</label>
						<textarea class="form-control" placeholder="Description" name="description" rows="4">{{ $permission->description }}</textarea>
					</div
					>
				</div>

				<div class="col-md-12">
					<hr>
					<div class="pull-right">
						<button class="btn btn-default" type="reset">Reset</button>
						<button class="btn btn-primary" type="submit">Save Changes</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</form>

@endsection

@push('js')
<script src="{{ url('assets/backend/pages/js/permission-add-edit.js') }}"></script>
@endpush