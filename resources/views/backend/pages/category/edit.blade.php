@extends('backend.layouts.master')
@section('title')
Edit Category
@endsection
@section('content')
	
	<div class="section-heading">
		<h1 class="page-title">Edit Category</h1>
		<div class="pull-right">
			<a href="{{ route('category.index') }}" class="btn btn-primary">Back</a>
		</div>
	</div>

	<form action="{{ route('category.update', $category->id) }}" method="post" id="form-add-edit-category">
		{{ csrf_field() }}
		{{ method_field('PUT') }}
		<div class="row">
			<div class="col-md-8">
				<div class="panel-content">
					<div class="form-group">
						<input type="text" name="name" value="{{ $category->name }}" placeholder="Category Name *" class="form-control input-lg" required="required">
						<span class="help-block"></span>
					</div>
					<div class="form-group">
						<textarea class="tinymce" placeholder="Content" name="description">{{ $category->description }}</textarea>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="panel-content">
					<h3 class="heading"><i class="fa fa-angle-right"></i> Categories</h3>
					<div class="form-group">
						<select name="parent_id" data-placeholder="Select parent" class="form-control select2" style="width: auto !important">
							<option value="0">No Parent</option>
							@foreach($parents as $parent)
							<option value="{{ $parent->id }}" {{ $parent->id == $category->parent_id ? 'selected=selected' : '' }}>{{ $parent->name }}</option>
							@endforeach
						</select>
					</div>

					<h3 class="heading"><i class="fa fa-angle-right"></i>Feature Image</h3>
					<div class="form-group">
							
						@if(!empty($category->media_id))

								@php ($is_image_exists = file_exists(public_path('uploads/images/250x170/'.$category->media->name)) ? true : false )

						@else

							@php($is_image_exists = 0)

						@endif

						<img class="img img-responsive img-thumbnail" id="image" {{ $is_image_exists ? 'src='.url('uploads/images/250x170/'.$category->media->name) : 'style=display:none' }}>
							<input type="text" name="media_id" value="{{ $category->media_id }}" hidden="hidden">
							<button type="button" class="btn btn-link text-danger btn-sm" onclick="on_remove_image()" id="btn-remove-img" {{ !$is_image_exists ? 'style=display: none' : '' }}>Remove</button>

						<button type="button" class="btn btn-link btn-sm" onclick="open_modal()">Select Image</button>

					</div>
				</div>
				<div class="clearfix"></div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-12">
				<hr>
				<div class="pull-right">
					<button type="reset" class="btn btn-default">Reset</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>

	</form>


	@include('backend.pages.media.list_data')

	@include('backend.pages.media.gallery')

@endsection

@push('js')
<script src="{{ url('assets/backend/pages/js/media.js') }}"></script>
@endpush