@extends('backend.layouts.master')
@section('title')
Create new role
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Create new role</h1>
	<div class="pull-right">
		<a href="{{ route('role.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>

<form action="{{ route('role.store') }}" method="post" id="form-add-edit">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<div class="panel-content">
				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Role Name<span class="text-danger">*</span></label>
						<input type="text" name="name" class="form-control" placeholder="eg: admin" required="required">
						<span class="help-block"></span>
					</div>


					<div class="form-group">
						<label class="control-label">Display Name<span class="text-danger">*</span></label>
						<input type="text" name="display_name" class="form-control" placeholder="eg: Admin" required="required">
						<span class="help-block"></span>
					</div>

				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="control-label">Description</label>
						<textarea name="description" placeholder="Description" class="form-control" rows="5"></textarea>
					</div>

				</div>

				<div class="col-md-12" style="margin-top: 20px">
					<h3 class="panel-title">Permission</h3>
					<hr>
				</div>

				@foreach ($permissions as $permission)
				<div class="col-md-3">
					<ul class="no-list">
					    <li>
					      	<label class="fancy-checkbox custom-bgcolor-green">
								<input type="checkbox" name="permission_id[]" value="{{ $permission->id }}" class="parent parent-{{ $permission->id }}" data-value="{{ $permission->id }}"><span>{{ $permission->display_name }}</span>
							</label>
							<ul class="no-list">
								@foreach ($permission->children as $children)
								<li>
									<label class="fancy-checkbox custom-bgcolor-green">
										<input type="checkbox" name="permission_id[]" value="{{ $children->id }}" class=" children children-{{ $permission->id}}" data-value="{{ $permission->id }}"><span>{{ $children->display_name }}</span>
									</label>
								</li>
								@endforeach
							</ul>
					    </li>
				  	</ul>
				</div>
				@endforeach

				

				<div class="col-md-12">
					<hr>
					<div class="pull-right">
						<button class="btn btn-default" type="reset">Reset</button>
						<button class="btn btn-primary" type="submit">Save</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</form>

@endsection

@push('js')
<script src="{{ url('assets/backend/pages/js/role-create-edit.js') }}"></script>
@endpush