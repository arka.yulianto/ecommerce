@extends('backend.layouts.master')
@section('title')
{{ $role->display_name }}
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">{{ $role->display_name }}</h1>
	<div class="pull-right">
		<a href="{{ route('role.index') }}" class="btn btn-primary">Back</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel-content">
			<table class="table table-primary table-custom">
				<tr class="info">
					<td colspan="2">Role Information</td>
					<td style="width: 350px">Permission</td>
				</tr>
				<tr>
					<td>Role Name</td>
					<td>{{ $role->name }}</td>
					<td rowspan="3">
						{!! '<span class="badge bg-info" style="margin: 5px">'.$role->perms()->pluck('display_name')->implode('</span><span class="badge bg-info" style="margin: 5px">') !!}
					</td>
				</tr>
				<tr>
					<td>Display Name</td>
					<td>{{ $role->display_name }}</td>
				</tr>
				<tr>
					<td>Description</td>
					<td>{{ $role->description }}</td>
				</tr>
			</table>
		</div>
	</div>
</div>
@endsection