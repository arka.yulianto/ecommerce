@extends('backend.layouts.master')
@section('title')
Role
@endsection
@section('content')

<div class="section-heading">
	<h1 class="page-title">Role</h1>
	<div class="pull-right">
		<a href="{{ route('role.create') }}" class="btn btn-primary">Add new</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel-content">
			<table class="table table-bordered" id="tbl-role">
				<thead>
					<tr>
						<th>Role Name</th>
						<th>Display Name</th>
						<th>Description</th>
						<th>Option</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>


<!-- Modal for question -->
<div class="modal fade in" tabindex="-1" role="dialog" id="modal-delete-confirm">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                <h4 class="modal-title">Are you sure</h4>
            </div>
            <div class="modal-body">Selected data will be destroyed, are you sure?</div>
            <div class="modal-footer">
                <button type="button" id="btn-confirm" class="btn btn-primary btn-sm">Yes</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">No (Cancel)</button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')

@if (session()->has('message'))
<script type="text/javascript">
	show_notification("{{ session('title') }}","{{ session('type') }}","{{ session('message') }}");
</script>
@endif

<script src="{{ url('assets/backend/pages/js/role.js') }}"></script>
@endpush