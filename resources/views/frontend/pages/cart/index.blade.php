@extends('frontend.layouts.master_left_sidebar')
@section('title')
Cart
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Home</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Pages</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>Checkout</span>
        </li>
      </ul>
    </div>
</section>

@if (count($carts) > 0)

<div class="container g-pt-100 g-pb-70">
      <div class="g-mb-100">
        <!-- Step Titles -->
        <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
          <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm active">
            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-primary g-color-primary g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
              <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
              <i class="fa fa-check g-show-check"></i>
            </span>
            <h4 class="g-font-size-16 text-uppercase mb-0">Shopping Cart</h4>
          </li>

          <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm">
            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
              <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
              <i class="fa fa-check g-show-check"></i>
            </span>
            <h4 class="g-font-size-16 text-uppercase mb-0">Shipping</h4>
          </li>

          <li class="col-3 list-inline-item">
            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
              <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
              <i class="fa fa-check g-show-check"></i>
            </span>
            <h4 class="g-font-size-16 text-uppercase mb-0">Payment &amp; Review</h4>
          </li>
        </ul>
        <!-- End Step Titles -->
      </div>
     <form action="{{ route('cart.bulk_update') }}" method="post" id="form-cart">
      {{ csrf_field() }}
      <div id="stepFormSteps">
        <!-- Shopping Cart -->
        <div id="step1" class="active">
          <div class="row">
            <div class="col-md-8 g-mb-30">
              <!-- Products Block -->
              <div class="g-overflow-x-scroll g-overflow-x-visible--lg">
                <table class="text-center w-100">
                  <thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-black text-uppercase">
                    <tr>
                      <th class="g-font-weight-400 text-left g-pb-20">Product</th>
                      <!-- <th class="g-font-weight-400 g-width-130 g-pb-20">Attribute</th> -->
                      <th class="g-font-weight-400 g-width-130 g-pb-20">Price</th>
                      <th class="g-font-weight-400 g-width-50 g-pb-20">Qty</th>
                      <th class="g-font-weight-400 g-width-130 g-pb-20">Total</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <!-- Item-->
                    @foreach ($carts as $cart)
                    <tr class="g-brd-bottom g-brd-gray-light-v3">
                      <td class="text-left g-py-25">
                        <img class="d-inline-block g-width-100 mr-4" src="{{ url('uploads/images/150x150/'.$cart->picture) }}" alt="{{ $cart->name }}">
                        <div class="d-inline-block align-middle">
                          <h4 class="h6 g-color-black">{{ $cart->name }}</h4>
                          <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_6 mb-0">
                            @foreach ($cart->attributes as $attribute)
                            <li>{{ $attribute['name'] }} : {{ $attribute['value'] }} </li>
                            @endforeach
                          </ul>
                        </div>
                      </td>
                      <td class="g-color-gray-dark-v2 g-font-size-13">{{ Helper::currency($cart->price) }}</td>
                      <td>
                        <div class="js-quantity input-group u-quantity-v1 g-width-80 g-brd-primary--focus">

                          <input class="js-result form-control text-center g-font-size-13 rounded-0 g-pa-0" type="text" value="{{ $cart->qty }}" readonly="" name="qty[]">
                          <input type="text" name="row_id[]" value="{{ $cart->getHash() }}" hidden="hidden">

                          <div class="input-group-addon d-flex align-items-center g-width-30 g-brd-gray-light-v2 g-bg-white g-font-size-12 rounded-0 g-px-5 g-py-6">
                            <i class="js-plus g-color-gray g-color-primary--hover fa fa-angle-up"></i>
                            <i class="js-minus g-color-gray g-color-primary--hover fa fa-angle-down"></i>
                          </div>
                        </div>
                      </td>
                      <td class="text-right g-color-black">
                        <span class="g-color-gray-dark-v2 g-font-size-13 mr-4">{{ Helper::currency($cart->price * $cart->qty) }}</span>
                        </span>


                        <a class="g-color-gray-dark-v4 g-color-black--hover g-cursor-pointer mr-2" data-toggle="tooltip" data-original-title="Remove" href="{{ route('cart.remove', $cart->getHash()) }}">
                            <i class="mt-auto fa fa-trash"></i></a>


                        <span class="g-color-gray-dark-v4 g-color-black--hover g-cursor-pointer btn-change" data-toggle="tooltip" data-original-title="Change attributes" data-value="{{ $cart->id }}" data-row-id="{{ $cart->getHash() }}">
                          <i class="mt-auto fa fa-pencil"></i>
                        </span>
                      </td>
                    </tr>
                    @endforeach
                    <!-- End Item-->
                  </tbody>
                </table>
              </div>
              <!-- End Products Block -->
            </div>

            <div class="col-md-4 g-mb-30">
              <!-- Summary -->
              <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                <h4 class="h6 text-uppercase mb-3">Summary</h4>

                <!-- Accordion -->
                <div id="accordion-01" class="mb-4" role="tablist" aria-multiselectable="true">
                  <div id="accordion-01-heading-01" class="g-brd-y g-brd-gray-light-v2 py-3" role="tab">
                    <h5 class="g-font-weight-400 g-font-size-default mb-0">
                      <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#accordion-01-body-01" data-toggle="collapse" data-parent="#accordion-01" aria-expanded="false" aria-controls="accordion-01-body-01">Estimate shipping
                        <span class="ml-3 fa fa-angle-down"></span></a>
                    </h5>
                  </div>
                  <div id="accordion-01-body-01" class="collapse" role="tabpanel" aria-labelledby="accordion-01-heading-01">
                    <div class="g-py-10">
                      <div class="mb-3">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Country</label>
                        <select name="country_id" class="form-control">
                          <option value="1">Indonesia</option>
                        </select>
                      </div>

                      <div class="mb-3">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Province or State</label>
                        <select name="province_id" class="form-control">
                          <option value="0">-- Select Province --</option>
                          @foreach ($provinces as $province)
                            <option value="{{ $province['province_id'] }}">{{ $province['province'] }}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="mb-3">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">City</label>
                        <select name="city_id" class="form-control">
                        <option value="0">-- Select City --</option>
                        </select>
                      </div>
                      
                      <div class="mb-3">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Courier</label>
                        <select name="courier_id" class="form-control" onchange="on_calculate();">
                          <option value="0">-- Select Courir --</option>
                          <option value="jne">JNE</option>
                          <option value="tiki">TIKI</option>
                          <option value="pos">POS</option>
                        </select>
                      </div>

                      <input type="text" name="total_weight" hidden="hidden" value="{{ $weight }}">

                      <div class="mb-3">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13 mb-3">Services</label>
                        <div class="row g-mb-30 g-font-size-13">
                          <table class="mb-5" style="width: 100%" id="table-service">
                            <thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-gray-dark-v3 g-font-size-13">
                              <tr>
                                <th class="g-width-70 g-font-weight-500 g-pa-0 g-pb-10">Name</th>
                                <th class="g-width-70 g-font-weight-500 g-pa-0 g-pb-10">Description</th>
                                <th class="g-width-110 g-font-weight-500 g-pa-0 g-pb-10">Estimate</th>
                                <th class="g-width-70 g-font-weight-500 text-right g-pa-0 g-pb-10">Cost</th>
                              </tr>
                            </thead>
                            <tbody>
                            <tr class="g-color-gray-dark-v4 g-font-size-13" id="empty-row">
                              <td colspan="4" class="text-center align-top g-py-10">No services</td>
                            </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>

                      <!-- <div class="mb-3 text-right">
                          <input type="text" name="cost" hidden="hidden">
                          <button class="btn btn-sm btn-primary" type="submit" name="submit_shipping">Update Shipping</button>
                      </div> -->

                      <hr>

                      <div class="d-flex justify-content-between mb-2">
                        <span class="g-color-black">Total Weight</span>
                        <span class="g-color-black g-font-weight-300">
                          {{ $weight }}gr
                        </span>
                      </div>

                    </div>
                  </div>
                </div>
                <!-- End Accordion -->

                <div class="d-flex justify-content-between mb-2">
                  <span class="g-color-black">Tax</span>
                  <span class="g-color-black g-font-weight-300">
                  	{{ Helper::currency(LaraCart::taxTotal($formatted = false)) }}
          		</span>
                </div>

                <div class="d-flex justify-content-between mb-2">
                  <span class="g-color-black">Shipping Fee</span>
                  <span class="g-color-black g-font-weight-300">
                    {{ Helper::currency(LaraCart::getFee('shippingFee')->amount) }}
                  </span>
                </div>

                <div class="d-flex justify-content-between mb-2">
                  <span class="g-color-black">Discount</span>
                  <span class="g-color-black g-font-weight-300">
                    @if (count(LaraCart::getCoupons()) > 0 )
                    @foreach (LaraCart::getCoupons() as $coupon)
                    -{{ Helper::currency($coupon->discount()) }}
                    @endforeach
                    @else
                      {{ Helper::currency(0) }}
                    @endif
                  </span>
                </div>

                <div class="d-flex justify-content-between mb-2">
                  <span class="g-color-black">Subtotal</span>
                  <span class="g-color-black g-font-weight-300">
                  	{{ Helper::currency(LaraCart::subTotal($format = false, $withDiscount = true)) }}
                  </span>
                </div>

                <div class="d-flex justify-content-between">
                  <span class="g-color-black">Order Total</span>
                  <span class="g-color-black g-font-weight-300">
                  	{{ Helper::currency(LaraCart::total($format = false, $withDiscount = true, $withTax = true, $withFees = true)) }}
          		</span>
                </div>
              </div>
              <!-- End Summary -->

              <button class="btn btn-block u-btn-outline-black g-brd-gray-light-v1 g-bg-black--hover g-font-size-13 text-uppercase g-py-15 mb-4" type="submit" name="submit_cart">Update Shopping Cart</button>
              <a class="btn btn-block u-btn-primary g-font-size-13 text-uppercase g-py-15 mb-4" href="{{ route('shipping.index') }}">Proceed to Checkout</a>

              <!-- Accordion -->
              <div id="accordion-02" role="tablist" aria-multiselectable="true">
                <div id="accordion-02-heading-02" role="tab">
                  <h5 class="g-font-weight-400 g-font-size-default mb-0">
                    <a class="g-color-black g-text-underline--none--hover" href="#accordion-02-body-02" data-toggle="collapse" data-parent="#accordion-02" aria-expanded="{{ session()->has('error') ? 'true' : 'false' }}" aria-controls="accordion-02-body-02">Apply discount code
                      <span class="ml-3 fa fa-angle-down"></span></a>
                  </h5>
                </div>
                <div id="accordion-02-body-02" class="collapse {{ session()->has('error') ? 'show' : '' }}" role="tabpanel" aria-labelledby="accordion-02-heading-02">
                  <div class="input-group rounded g-pt-15">
                    <input class="form-control {{ session()->has('error') ? 'g-brd-red' : 'g-brd-gray-light-v1' }} g-brd-right-none g-color-gray-dark-v3 g-placeholder-gray-dark-v3" type="text" placeholder="Enter discount code" name="coupon_code">
                    <span class="input-group-addon {{ session()->has('error') ? 'g-brd-red' : 'g-brd-gray-light-v1' }} g-bg-white">
                      <button class="btn u-btn-primary rounded" type="submit" name="submit_coupon">Apply</button>
                    </span>
                  </div>
                  @if(session()->has('error'))
                    <div class="text-danger mt-2">{{ session()->get('error') }}</div>
                  @endif
                </div>
              </div>
              <!-- End Accordion -->
            </div>
          </div>
        </div>
        <!-- End Shopping Cart -->

        <!-- End Shipping -->
      </div>
    </form>
  </div>

<form action="{{ route('cart.update', 0) }}" method="post">
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  <input type="text" name="row_id" hidden="hidden">
  <input type="text" name="id" hidden="hidden">
  <div class="modal fade" tabindex="-1" role="dialog" id="modal-change-attribute">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
          <h4 class="modal-title text-uppercase g-brd-gray-light-v3 g-color-black" style="font-weight: lighter;">Change Attributes</h4>
        </div>
        <div class="modal-body">
        	<div class="col-md-12" id="attribute-form">
        		
        	</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update Cart</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
</form>

@else

<div class="container text-center g-py-100">
	<div class="mb-5">
	  <span class="d-block g-color-gray-light-v1 g-font-size-70 g-font-size-90--md mb-4">
	    <i class="icon-hotel-restaurant-105 u-line-icon-pro"></i>
	  </span>
	  <h2 class="g-mb-30">Your Cart is Currently Empty</h2>
	  <p>Before proceed to checkout you must add some products to your shopping cart.<br>You will find a lot of interesting products on our "Shop" page.</p>
	</div>
	<a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" href="{{ route('shop.index') }}">Start Shopping</a>
</div>

@endif

@endsection

@push('js')

<script src="{{ url('assets/frontend/js/components/hs.count-qty.js') }}"></script>
<script src="{{ url('assets/frontend/js/pages/cart.js') }}"></script>
<script src="{{ url('assets/frontend/js/pages/shipping.js') }}"></script>

@endpush