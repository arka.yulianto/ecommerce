@foreach ($product->attributes as $attribute)
	<label class="control-label">{{ $attribute->attribute_name }}</label>
	<select name="attribute_value[]" class="form-control">
		@foreach (explode(',', $attribute->attribute_value) as $value)
			<option value="{{ $value }}">{{ $value }}</option>
		@endforeach
	</select>
	<input type="text" name="attribute_name[]" value="{{ $attribute->attribute_name }}" hidden="hidden">
@endforeach