@extends('frontend.layouts.master_left_sidebar')
@section('title')
Cart
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Home</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Pages</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>Checkout</span>
        </li>
      </ul>
    </div>
</section>

@if (session()->has('message'))
<div class="container g-py-100">
  <div class="u-shadow-v19 g-max-width-645 g-brd-around g-brd-gray-light-v4 text-center rounded mx-auto g-pa-30 g-pa-50--md">

    @if (session()->get('message')['status'] == 'success')
    <span class="u-icon-v3 u-icon-size--lg g-color-white g-bg-primary rounded-circle g-pa-15 mb-5">
       <i class="icon-check"></i>
    </span>
    @else
    <span class="u-icon-v3 u-icon-size--lg g-color-white g-bg-red rounded-circle g-pa-15 mb-5">
      <i class="icon-ban"></i>
    </span>
    @endif

    <div class="mb-5">
      <h2 class="mb-4">{{ session()->get('message')['status'] == 'success' ? 'Success' : 'Failed' }}</h2>
      <p>{{ session()->get('message')['content'] }}</p>
    </div>

    <a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" href="{{ route('shop.index') }}">Continue Shopping</a>
  </div>
</div>
@endif

@endsection