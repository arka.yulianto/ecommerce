@extends('frontend.layouts.master_left_sidebar')
@section('title')
Cart
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Home</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Pages</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>Checkout</span>
        </li>
      </ul>
    </div>
</section>


<div class="container g-pt-100 g-pb-70">
      <div class="g-mb-100">
        <!-- Step Titles -->
       <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
          <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm g-checked">
            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-primary g-color-primary g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
              <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
              <i class="fa fa-check g-show-check"></i>
            </span>
            <h4 class="g-font-size-16 text-uppercase mb-0">Shopping Cart</h4>
          </li>

          <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm g-checked">
            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
              <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
              <i class="fa fa-check g-show-check"></i>
            </span>
            <h4 class="g-font-size-16 text-uppercase mb-0">Shipping</h4>
          </li>

          <li class="col-3 list-inline-item active">
            <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
              <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
              <i class="fa fa-check g-show-check"></i>
            </span>
            <h4 class="g-font-size-16 text-uppercase mb-0">Payment &amp; Review</h4>
          </li>
        </ul>
        <!-- End Step Titles -->
      </div>
     <form action="{{ route('checkout.store') }}" method="post" class="js-validate">
      {{ csrf_field() }}

        <div id="step3">
          <div class="row">
            <div class="col-md-8 g-mb-30">

            @if (session()->has('message'))
              <div class="alert alert-dismissible fade show {{ session()->get('message')['status'] == 'success' ? 'g-bg-teal' : 'g-bg-red' }} g-color-white rounded-0" role="alert">
                <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>

                <div class="media">

                  @if (session()->get('message')['status'] == 'success')
                  <span class="d-flex g-mr-10 g-mt-5">
                    <i class="icon-check g-font-size-25"></i>
                  </span>
                  <span class="media-body align-self-center">
                    <strong>Well done!</strong> {{ session()->get('message')['content'] }}
                  </span>
                  @else
                    <span class="d-flex g-mr-10 g-mt-5">
                      <i class="icon-ban g-font-size-25"></i>
                    </span>
                    <span class="media-body align-self-center">
                      <strong>Oh snap!</strong> {{ session()->get('message')['content'] }}
                    </span>
                  @endif

                </div>

              </div>
            @endif

              <!-- Payment Methods -->
              <ul class="list-unstyled mb-5 mt-5">
                <li class="g-brd-bottom g-brd-gray-light-v3 pb-3 my-3">
                  <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="payment_method" type="radio" checked="checked" value="paypal">
                      <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                        <i class="fa" data-check-icon="&#xf00c"></i>
                      </span>
                      Pay with
                    <img class="g-width-70 ml-2" src="{{ url('assets/frontend/img/icons/paypal.png') }}" alt="PayPal">
                  </label>
                </li>
                <!-- <li class="my-3">
                  <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="radInline1_1" type="radio">
                      <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                        <i class="fa" data-check-icon="&#xf00c"></i>
                      </span>
                      Pay with Credit Card
                    <img class="g-width-50 ml-2" src="assets/img-temp/200x55/img2.jpg" alt="Image Description">
                  </label>
                </li> -->
              </ul>
              <!-- End Payment Methods -->

              <!-- Alert -->
              <div class="alert g-brd-around g-brd-gray-dark-v5 rounded-0 g-pa-0 mb-4" role="alert">
                <div class="media">
                  <div class="d-flex g-brd-right g-brd-gray-dark-v5 g-pa-15">
                    <span class="u-icon-v1 u-icon-size--xs g-color-black">
                      <i class="align-middle icon-media-065 u-line-icon-pro"></i>
                    </span>
                  </div>
                  <div class="media-body g-pa-15">
                    <p class="g-color-black m-0">My billing and shipping address are the correct</p>
                  </div>
                </div>
              </div>
              <!-- End Alert -->

              <!-- Shipping Details -->
              <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-15 g-pl-70 mb-5">
                <li class="g-my-3">{{ $shipping['first_name'] }} {{ $shipping['last_name'] }}</li>
                <li class="g-my-3">{{ $shipping['email'] }}</li>
                <li class="g-my-3">{{ $shipping['address'] }}</li>
                <li class="g-my-3">{{ $state['province'] }}, {{ $city['type'].' '.$city['city_name'] }}</li>
                <li class="g-my-3">{{ $shipping['zip'] }}</li>
                <li class="g-my-3">{{ $shipping['country_id'] }}</li>
                <li class="g-my-3">{{ $shipping['phone_number'] }}</li>
              </ul>
              <!-- End Shipping Details -->

              <div class="g-brd-bottom g-brd-gray-light-v3 g-pb-30 g-mb-30">
                <div class="text-right">
                  <button class="btn u-btn-primary g-font-size-13 text-uppercase g-px-40 g-py-15" type="submit">Make Payment</button>
                </div>
              </div>

            </div>

            <div class="col-md-4 g-mb-30">

              <!-- Order Summary -->
              <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                <div class="g-brd-bottom g-brd-gray-light-v3 g-mb-15">
                  <h4 class="h6 text-uppercase mb-3">Order summary</h4>
                </div>

                <div class="d-flex justify-content-between mb-3">
                  <span class="g-color-black">Cart Subtotal</span>
                  <span class="g-color-black g-font-weight-300">{{ Helper::currency(LaraCart::subTotal($format = false, $withDiscount = true)) }}</span>
                </div>
                <div class="mb-3">
                  <div class="d-flex justify-content-between mb-1">
                    <span class="g-color-black">Shipping</span>
                    <span class="g-color-black g-font-weight-300">{{ Helper::currency(LaraCart::getFee('shippingFee')->amount) }}</span>
                  </div>
                  <p class="g-font-size-13">{{ $shipping['service_name'] }} ({{ $shipping['service_description'] }}) - {{ $shipping['estimate_delivery'] }} Day(s)</p>
                </div>

                <div class="d-flex justify-content-between mb-2">
                  <span class="g-color-black">Discount</span>
                  <span class="g-color-black g-font-weight-300">
                    @if (count(LaraCart::getCoupons()) > 0 )
                    @foreach (LaraCart::getCoupons() as $coupon)
                    -{{ Helper::currency($coupon->discount()) }}
                    @endforeach
                    @else
                      {{ Helper::currency(0) }}
                    @endif
                  </span>
                </div>

                <div class="d-flex justify-content-between mb-3">
                  <span class="g-color-black">Order Total</span>
                  <span class="g-color-black g-font-weight-300">{{ Helper::currency(LaraCart::total($format = false, $withDiscount = true, $withTax = true, $withFees = true)) }}</span>
                </div>

                <!-- Accordion -->
                <div id="accordion-05" class="mb-4" role="tablist" aria-multiselectable="true">
                  <div id="accordion-05-heading-05" class="g-brd-y g-brd-gray-light-v2 py-3" role="tab">
                    <h5 class="g-font-weight-400 g-font-size-default mb-0">
                      <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#accordion-05-body-05" data-toggle="collapse" data-parent="#accordion-05" aria-expanded="false" aria-controls="accordion-05-body-05">{{ count($products) }} items in cart
                        <span class="ml-3 fa fa-angle-down"></span></a>
                    </h5>
                  </div>
                  <div id="accordion-05-body-05" class="collapse" role="tabpanel" aria-labelledby="accordion-05-heading-05">
                    <div class="g-py-15">
                      <ul class="list-unstyled mb-3">
                        @foreach($products as $product)
                        <!-- Product -->
                        <li class="d-flex justify-content-start">
                          <img class="g-width-100 g-height-100 mr-3" src="{{ url('uploads/images/150x150/'.$product->picture) }}" alt="{{ $product->name }}">
                          <div class="d-block">
                            <h4 class="h6 g-color-black">{{ $product->name }}</h4>
                            <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_4 mb-1">
                              @foreach ($product->attributes as $attribute)
                                <li>{{ $attribute['name'] }} : {{ $attribute['value'] }} </li>
                              @endforeach
                            </ul>
                            <span class="d-block g-color-black g-font-weight-400">{{ Helper::currency($product->price) }}</span>
                          </div>
                        </li>
                        <!-- End Product -->
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- End Accordion -->
              </div>
              <!-- End Order Summary -->

              <!-- Ship To -->
              <div class="g-px-20 mb-5">
                <div class="d-flex justify-content-between g-brd-bottom g-brd-gray-light-v3 g-mb-15">
                  <h4 class="h6 text-uppercase mb-3">Ship to</h4>
                  <a class="g-color-gray-dark-v4 g-color-black--hover g-cursor-pointer" href="{{ route('shipping.index') }}">
                    <i class="fa fa-pencil"></i>
                  </a>
                </div>
                <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-15">
                  <li class="g-my-3">{{ $shipping['first_name'] }} {{ $shipping['last_name'] }}</li>
                  <li class="g-my-3">{{ $shipping['email'] }}</li>
                  <li class="g-my-3">{{ $shipping['address'] }}</li>
                  <li class="g-my-3">{{ $state['province'] }}, {{ $city['type'].' '.$city['city_name'] }}</li>
                  <li class="g-my-3">{{ $shipping['zip'] }}</li>
                  <li class="g-my-3">{{ $shipping['country_id'] }}</li>
                  <li class="g-my-3">{{ $shipping['phone_number'] }}</li>
                </ul>
              </div>
              <!-- End Ship To -->

              <!-- Shipping Method -->
              <div class="g-px-20 mb-5">
                <div class="d-flex justify-content-between g-brd-bottom g-brd-gray-light-v3 g-mb-15">
                  <h4 class="h6 text-uppercase mb-3">Shipping Method</h4>
                </div>
                <p class="g-color-gray-dark-v4 g-font-size-15">{{ $shipping['service_name'] }} ({{ $shipping['service_description'] }}) - {{ $shipping['estimate_delivery'] }} Day(s)</p>
              </div>
              <!-- End Shipping Method -->
            </div>
          </div>
        </div>
    </form>
  </div>

@endsection

@push('js')

<script src="{{ url('assets/frontend/vendor/chosen/chosen.jquery.js') }}"></script>
<script src="{{ url('assets/frontend/vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.validation.js') }}"></script>

<script src="{{ url('assets/frontend/js/components/hs.select.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.count-qty.js') }}"></script>
<script src="{{ url('assets/frontend/js/pages/cart.js') }}"></script>
<script src="{{ url('assets/frontend/js/pages/shipping2.js') }}"></script>
<script>
  $.HSCore.components.HSSelect.init('.js-custom-select');
  $.HSCore.components.HSValidation.init('.js-validate');
</script>

@endpush