@extends('frontend.layouts.master_home')

@section('title')
Home
@endsection

@section('content')
		    <div class="text-center mx-auto g-max-width-600 g-mb-50">
          <h2 class="g-color-black mb-4">Featured Products</h2>
          <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
        </div>

        <div id="carouselCus1" class="js-carousel g-pb-100 g-mx-minus-10"
             data-infinite="true"
             data-slides-show="4"
             data-arrows-classes="u-arrow-v1 g-pos-abs g-bottom-0 g-width-45 g-height-45 g-color-gray-dark-v5 g-bg-secondary g-color-white--hover g-bg-primary--hover rounded"
             data-arrow-left-classes="fa fa-angle-left g-left-10"
             data-arrow-right-classes="fa fa-angle-right g-right-10"
             data-pagi-classes="u-carousel-indicators-v1 g-absolute-centered--x g-bottom-20 text-center">
          @foreach ($products as $product)
            <div class="js-slide">
              <div class="g-px-10">
                <!-- Product -->
                <figure class="g-pos-rel g-mb-20">
                  <img class="img-fluid" src="{{ url('uploads/images/480x700/'.$product->media->name) }}" alt="Image Description">

                  @if (!empty($product->discount) and $product->stock > 0)
                  <figcaption class="w-100 g-bg-yellow g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                    <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="{{ url('shop/sale') }}">Sale</a>
                  </figcaption>
                  @endif

                  @if ($product->stock < 1)
                  <figcaption class="w-100 g-bg-lightred g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                    <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover">Out of stock</a>
                  </figcaption>
                  @endif

                  @if ($product->created_at > date('Y-m-d') and empty($product->discount))
                  <figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
                    <a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover">New Arrival</a>
                  </figcaption>
                  @endif

                  @if (!empty($product->discount))

                   <span class="u-ribbon-v1 g-width-40 g-height-40 g-color-white g-bg-primary g-font-size-13 text-center text-uppercase g-rounded-50x g-top-10 g-right-minus-10 g-px-2 g-py-10">-{{ round(( ($product->price - $product->discount) / $product->price) * 100) }}%</span>

                  @endif

                </figure>

                <div class="media">
                  <!-- Product Info -->
                  <div class="d-flex flex-column">
                    <h4 class="h6 g-color-black mb-1">
                      <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{ route('shop.details', $product->slug) }}">
                        {{ $product->name }}
                      </a>
                    </h4>


                    @foreach ($product->categories as $category)
                      <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13" href="{{ url('shop/category/'.$category->id) }}">{{ $category->name }}</a>
                    @endforeach

                    <span class="d-block g-color-black g-font-size-17">
                      @if (!empty($product->discount))
                        <sup class="text-muted"><s>{{ Helper::currency($product->price) }}</s></sup>
                        {{ Helper::currency($product->discount) }}
                      @else
                        {{ Helper::currency($product->price) }}
                      @endif
                    </span>
                  </div>
                  <!-- End Product Info -->

                  <!-- Products Icons -->
                  <ul class="list-inline media-body text-right">
                    <li class="list-inline-item align-middle mx-0">
                        <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle"
                            href="{{ route('cart.store') }}"
                            onclick="event.preventDefault();
                             document.getElementById('form-cart-{{ $product->id }}').submit();"
                           data-toggle="tooltip"
                           data-placement="top"
                           title="Add to Cart">
                          <i class="icon-finance-100 u-line-icon-pro"></i>
                        </a>
                      <form action="{{ route('cart.store') }}" method="post" id="form-cart-{{ $product->id }}" style="display: none">
                        <input type="text" name="id" value="{{ $product->id }}" hidden="hidden">
                        {{ csrf_field() }}
                      </form>
                    </li>
                    <li class="list-inline-item align-middle mx-0">
                      <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
                         data-toggle="tooltip"
                         data-placement="top"
                         title="Add to Wishlist">
                        <i class="icon-medical-022 u-line-icon-pro"></i>
                      </a>
                    </li>
                  </ul>
                  <!-- End Products Icons -->
                </div>
                <!-- End Product -->
              </div>
            </div>
          @endforeach 
        </div>
@endsection


@section('content_bottom')
      <div class="text-center mx-auto g-max-width-600 g-mb-50">
          <h2 class="g-color-black mb-4">New Arrivals</h2>
          <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
        </div>

        <div class="row g-mx-minus-10 g-mb-50">
          @foreach ($products as $product)
          <div class="col-md-6 col-lg-4 g-px-10">
            <!-- Article -->
            <article class="media g-brd-around g-brd-gray-light-v4 g-bg-white rounded g-pa-10 g-mb-20">
              <!-- Article Image -->
              <div class="g-max-width-100 g-mr-15">
                <img class="d-flex w-100" src="{{ url('uploads/images/150x150/'.$product->media->name) }}" alt="{{ $product->name }}">
              </div>
              <!-- End Article Image -->

              <!-- Article Info -->
              <div class="media-body align-self-center">
                <h4 class="h5 g-mb-7">
                  <a class="g-color-black g-color-primary--hover g-text-underline--none--hover" href="{{ route('shop.details', $product->slug) }}">{{ $product->name }}</a>
                </h4>

                @foreach ($product->categories as $category)
                  <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13 g-mb-10" href="#!">{{ $category->name }}</a>
                @endforeach

                
                <!-- End Article Info -->

                <!-- Article Footer -->
                <footer class="d-flex justify-content-between g-font-size-16">
                  <span class="g-color-black g-line-height-1">
                    @if (!empty($product->discount))
                      <sup class="text-muted"><s>{{ Helper::currency($product->price) }}</s></sup>
                      {{ Helper::currency($product->discount) }}
                    @else
                      {{ Helper::currency($product->price) }}
                    @endif
                  </span>
                  <ul class="list-inline g-color-gray-light-v2 g-font-size-14 g-line-height-1">
                    <li class="list-inline-item align-middle g-brd-right g-brd-gray-light-v3 g-pr-10 g-mr-6">
                      <a class="g-color-gray-dark-v5 g-color-primary--hover g-text-underline--none--hover" href="#!" title="Add to Cart"
                         data-toggle="tooltip"
                         data-placement="top">
                        <i class="icon-finance-100 u-line-icon-pro"></i>
                      </a>
                    </li>
                    <li class="list-inline-item align-middle">
                      <a class="g-color-gray-dark-v5 g-color-primary--hover g-text-underline--none--hover" href="#!" title="Add to Wishlist"
                         data-toggle="tooltip"
                         data-placement="top">
                        <i class="icon-medical-022 u-line-icon-pro"></i>
                      </a>
                    </li>
                  </ul>
                </footer>
                <!-- End Article Footer -->
              </div>
            </article>
            <!-- End Article -->
          </div>

          @endforeach

        </div>

        <div class="text-center">
          <a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" href="#!">All New Arrivals</a>
        </div>
@endsection

@push('js')
<script src="{{ url('assets/frontend/js/pages/home.js') }}"></script>
@endpush