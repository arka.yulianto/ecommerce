
@extends('frontend.layouts.master_left_sidebar')
@section('title')
	{{ $product->name }}
@endsection

@section('content')

 <!-- Breadcrumbs -->
      <section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
        <div class="container">
          <ul class="u-list-inline">
            <li class="list-inline-item g-mr-5">
              <a class="u-link-v5 g-color-text" href="#!">Home</a>
              <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
            </li>
            <li class="list-inline-item g-mr-5">
              <a class="u-link-v5 g-color-text" href="#!">Pages</a>
              <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
            </li>
            <li class="list-inline-item g-color-primary">
              <span>Single Product 2</span>
            </li>
          </ul>
        </div>
      </section>
      <!-- End Breadcrumbs -->

      <!-- Product Description -->
      <div class="container g-pt-50 g-pb-100">
        <div class="row">
          <div class="col-lg-7">
            <!-- Carousel -->
            <div id="carouselCus1" class="js-carousel g-pt-10 g-mb-10"
                 data-infinite="true"
                 data-fade="true"
                 data-arrows-classes="u-arrow-v1 g-brd-around g-brd-white g-absolute-centered--y g-width-45 g-height-45 g-font-size-14 g-color-white g-color-primary--hover rounded-circle"
                 data-arrow-left-classes="fa fa-angle-left g-left-40"
                 data-arrow-right-classes="fa fa-angle-right g-right-40"
                 data-nav-for="#carouselCus2">

             @foreach ($product->galleries as $gallery)
              <div class="js-slide g-bg-cover g-bg-black-opacity-0_1--after">
                <img class="img-fluid w-100" src="{{ url('uploads/images/650x750/'.$gallery->name) }}" alt="{{ $gallery->alt }}">
              </div>
              @endforeach
            </div>

            <div id="carouselCus2" class="js-carousel text-center u-carousel-v3 g-mx-minus-5"
                 data-infinite="true"
                 data-center-mode="true"
                 data-slides-show="3"
                 data-is-thumbs="true"
                 data-nav-for="#carouselCus1">

                
               	@foreach ($product->galleries as $gallery)
               	<div class="js-slide g-cursor-pointer g-px-5">
                	<img class="img-fluid" src="{{ url('uploads/images/700x467/'.$gallery->name) }}" alt="{{ $gallery->alt }}">
              	</div>
              	@endforeach
             

            </div>
            <!-- End Carousel -->
          </div>

          <div class="col-lg-5">
            <div class="g-px-40--lg g-pt-70">
              <!-- Product Info -->
              <div class="g-mb-30">
                <h1 class="g-font-weight-300 mb-4">{{ $product->name }}</h1>

                <div class="col-md-12 g-mb-10">
                  <span class="rate">
                    {!! Helper::getRate($rate) !!}
                  </span>
                </div>

                <div class="col-md-12">
                  {!! $product->description !!}
                </div>
              </div>
              <!-- End Product Info -->

              <!-- Price -->
              <div class="g-mb-30">
                <h2 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-12 text-uppercase mb-2">Price</h2>
                @if (!empty($product->discount))
                <span class="g-color-black g-font-weight-500 g-font-size-30 mr-2">{{ Helper::currency($product->discount) }}</span>
                <s class="g-color-gray-dark-v4 g-font-weight-500 g-font-size-16">{{ Helper::currency($product->price) }}</s>
                @else
                <span class="g-color-black g-font-weight-500 g-font-size-30 mr-2">{{ Helper::currency($product->price) }}</span>
                @endif
              </div>
              <!-- End Price -->

              <form action="{{ route('cart.store') }}" method="post">

              {{ csrf_field() }}

              <input type="text" name="id" value="{{ $product->id }}" hidden="hidden">
             
             @if (count($product->attributes) > 0)

             	@foreach ($product->attributes as $attribute)

	             <div class="d-flex justify-content-between align-items-center g-brd-bottom g-brd-gray-light-v3 py-3 g-mb-30" role="tab">
	                <h5 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-default mb-0">{{ $attribute->attribute_name }}</h5>

	                <input type="text" name="attribute_name[]" value="{{ $attribute->attribute_name }}" hidden="hidden">

	                <div class="form-group">
	                	
	                	<select name="attribute_value[]" class="form-control">
	                		@foreach (explode(',', $attribute->attribute_value ) as $value)
	                			<option value="{{ $value }}">{{ $value }}</option>
	                		@endforeach
	                	</select>

	                </div>

	              </div>
	            @endforeach
	        @endif
              
              <!-- Quantity -->
              <div class="d-flex justify-content-between align-items-center g-brd-bottom g-brd-gray-light-v3 py-3 g-mb-30" role="tab">
                <h5 class="g-color-gray-dark-v5 g-font-weight-400 g-font-size-default mb-0">Quantity</h5>

                <div class="js-quantity input-group u-quantity-v1 g-width-80 g-brd-primary--focus">
                  <input class="js-result form-control text-center g-font-size-13 rounded-0" type="text" value="1" readonly name="qty">

                  <div class="input-group-addon d-flex align-items-center g-width-30 g-brd-gray-light-v2 g-bg-white g-font-size-13 rounded-0 g-pa-5">
                    <i class="js-plus g-color-gray g-color-primary--hover fa fa-angle-up"></i>
                    <i class="js-minus g-color-gray g-color-primary--hover fa fa-angle-down"></i>
                  </div>
                </div>
              </div>
              <!-- End Quantity -->

              <!-- Buttons -->
              <div class="row g-mx-minus-5 g-mb-20">
                <div class="col g-px-5 g-mb-10">
                  <button class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-15 g-px-25" type="submit">
                    Add to Cart <i class="align-middle ml-2 icon-finance-100 u-line-icon-pro"></i>
                  </button>
                </div>
                <div class="col g-px-5 g-mb-10">
                  <button class="btn btn-block u-btn-outline-black g-brd-gray-dark-v5 g-brd-black--hover g-color-gray-dark-v4 g-color-white--hover g-font-size-12 text-uppercase g-py-15 g-px-25" type="button">
                    Add to Wishlist <i class="align-middle ml-2 icon-medical-022 u-line-icon-pro"></i>
                  </button>
                </div>
              </div>
              <!-- End Buttons -->

              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- End Product Description -->

      <!-- Review -->
      <div class="container">
        <div class="g-brd-y g-brd-gray-light-v4 g-py-100">
          <div class="row justify-content-center">
            <div class="col-lg-9">
              <h2 class="h4 mb-5">Review</h2>

              @if (count($product->comments) > 0)

              @foreach ($product->comments as $comment)
              <!-- Review -->
              <div class="g-brd-bottom g-brd-gray-light-v4 g-pb-30 g-mb-50">
                <!-- Media -->
                <div class="media g-mb-30">
                  <img class="d-flex g-width-60 g-height-60 rounded-circle g-mt-3 g-mr-20" src="{{ $comment->user->user_data->picture }}" alt="Image Description">
                  <div class="media-body">
                    <div class="d-flex align-items-start g-mb-15 g-mb-10--sm">
                      <div class="d-block">
                        <h5 class="h6">{{ $comment->user->user_data->fullname }}</h5>

                        <!-- Rating -->
                        <span class="rate">
                          {!! Helper::getRate($comment->rate) !!}
                      	</span>
                        <!-- End Rating -->

                        <span class="d-block g-color-gray-dark-v5 g-font-size-11">{{ Carbon\Carbon::parse($comment->created_at)->format('F jS, Y') }}</span>
                      </div>
                    </div>

                    <p>{{ $comment->content }}</p>
                  </div>
                </div>
                <!-- End Media -->
              </div>
              <!-- End Review -->
              @endforeach
              @else

              <div class="g-brd-bottom g-brd-gray-light-v4 g-pb-30 g-mb-50">
              	<p>No reviews yet</p>
              </div>

              @endif

              @if (Auth::check())
              <h2 class="h4 mb-5">Add Review</h2>

              <form action="{{ route('review.store') }}" method="post">
              {{ csrf_field() }}
              <input type="text" name="product_id" hidden="hidden" value="{{ $product->id }}">
              <!-- End Comment Form -->
              <div class="form-group">
	  			<fieldset class="rating">
				    <input type="radio" id="star5" name="rate" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
				    <input type="radio" id="star4" name="rate" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
				    <input type="radio" id="star3" name="rate" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
				    <input type="radio" id="star2" name="rate" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
				    <input type="radio" id="star1" name="rate" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
				</fieldset>
			</div>

              <div class="g-mb-30">
                <textarea class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-brd-primary--focus g-resize-none rounded-3 g-py-13 g-px-15" rows="12" placeholder="Your message" name="content"></textarea>
              </div>

              <div class="row align-items-center">
                <div class="col-5 col-sm-4 col-md-3">
                  <button class="btn u-btn-primary g-font-size-12 text-uppercase g-py-15 g-px-25" type="submit" role="button">Add Comment</button>
                </div>
              </div>

              </form>

              @else

          		<div class="g-brd-bottom g-brd-gray-light-v4 g-pb-30 g-mb-50">
	              	<p>You must sign in to give a rivew</p>
             	</div>

              @endif
            </div>
          </div>
        </div>
      </div>
      <!-- End Review -->

      <!-- Products -->
      <div class="container g-pt-100 g-pb-70">
        <div class="text-center mx-auto g-max-width-600 g-mb-50">
          <h2 class="g-color-black mb-4">Related Products</h2>
          <p class="lead">We want to create a range of beautiful, practical and modern outerwear that doesn't cost the earth – but let's you still live life in style.</p>
        </div>

        <!-- Products -->
        <div class="row">

        	@foreach ($relateds as $related)
	          <div class="col-6 col-lg-3 g-mb-30">
                  <!-- Product -->
                  <figure class="g-pos-rel g-mb-20">
                    <img class="img-fluid" src="{{ url('uploads/images/480x700/'.$related->media->name) }}" alt="Image Description">

                    @if (!empty($related->discount) and $related->stock > 0)
					<figcaption class="w-100 g-bg-yellow g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
					<a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover" href="{{ url('shop/sale') }}">Sale</a>
					</figcaption>
					@endif

					@if ($related->stock < 1)
					<figcaption class="w-100 g-bg-lightred g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
					<a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover">Out of stock</a>
					</figcaption>
					@endif

					@if ($related->created_at > date('Y-m-d') and empty($related->discount))
					<figcaption class="w-100 g-bg-primary g-bg-black--hover text-center g-pos-abs g-bottom-0 g-transition-0_2 g-py-5">
					<a class="g-color-white g-font-size-11 text-uppercase g-letter-spacing-1 g-text-underline--none--hover">New Arrival</a>
					</figcaption>
					@endif

				 	@if (!empty($related->discount))

	                   <span class="u-ribbon-v1 g-width-40 g-height-40 g-color-white g-bg-primary g-font-size-13 text-center text-uppercase g-rounded-50x g-top-10 g-right-minus-10 g-px-2 g-py-10">-{{ round(( ($related->price - $related->discount) / $related->price) * 100) }}%</span>

                  	@endif


                  </figure>

                  <div class="media">
                    <!-- Product Info -->
                    <div class="d-flex flex-column">
                      <h4 class="h6 g-color-black mb-1">
                        <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{ route('shop.details', $related->slug) }}">
                          {{ $related->name }}
                        </a>
                      </h4>
                      @foreach ($related->categories as $category)
                      <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13" href="#!">{{ $category->name }}</a>
                      @endforeach

                      <span class="d-block g-color-black g-font-size-17">
                      @if (!empty($related->discount))
                        <sup class="text-muted"><s>{{ Helper::currency($related->price) }}</s></sup>
                        {{ Helper::currency($related->discount) }}
                      @else
                        {{ Helper::currency($related->price) }}
                      @endif
                    </span>
                    </div>
                    <!-- End Product Info -->

                    <!-- Products Icons -->
		              <ul class="list-inline media-body text-right">
		                <li class="list-inline-item align-middle mx-0">
		                    <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle"
		                        href="{{ route('cart.store') }}"
		                        onclick="event.preventDefault();
		                         document.getElementById('form-cart-{{ $related->id }}').submit();"
		                       data-toggle="tooltip"
		                       data-placement="top"
		                       title="Add to Cart">
		                      <i class="icon-finance-100 u-line-icon-pro"></i>
		                    </a>
		                  <form action="{{ route('cart.store') }}" method="post" id="form-cart-{{ $related->id }}" style="display: none">
		                    <input type="text" name="id" value="{{ $related->id }}" hidden="hidden">
		                    {{ csrf_field() }}
		                  </form>
		                </li>
		                <li class="list-inline-item align-middle mx-0">
		                  <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" href="#!"
		                     data-toggle="tooltip"
		                     data-placement="top"
		                     title="Add to Wishlist">
		                    <i class="icon-medical-022 u-line-icon-pro"></i>
		                  </a>
		                </li>
		              </ul>
                    <!-- End Products Icons -->
                  </div>
                  <!-- End Product -->
                </div>
	        @endforeach
        </div>
        <!-- End Products -->
      </div>
      <!-- End Products -->
@endsection

@push('js')

<script src="{{ url('assets/frontend/js/components/hs.count-qty.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.tabs.js') }}"></script>


 <script>
      $(document).on('ready', function () {
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu plugin
        $('.js-mega-menu').HSMegaMenu({
          event: 'hover',
          pageContainer: $('.container'),
          breakpoint: 991
        });

        // initialization of HSDropdown component
        $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
          afterOpen: function () {
            $(this).find('input[type="search"]').focus();
          }
        });

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of HSScrollBar component
        $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

        // initialization of quantity counter
        $.HSCore.components.HSCountQty.init('.js-quantity');

        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of rating
        $.HSCore.helpers.HSRating.init();
      });

      $(window).on('resize', function () {
        setTimeout(function () {
          $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
      });
    </script>
 @endpush