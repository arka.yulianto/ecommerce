@extends('frontend.layouts.master_left_sidebar')

@section('title')
Dashboard
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
	<div class="container">
		<div class="d-sm-flex text-center">
			<div class="align-self-center">
				<h1 class="h3 mb-0">Dashboard</h1>
			</div>

			<div class="align-self-center ml-auto">
				<ul class="u-list-inline">
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Home</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Pages</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-color-primary">
						<span>Dashboard</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="container g-pt-70 g-pb-30">
    <div class="row">
      <!-- Profile Settings -->
      <div class="col-lg-3 g-mb-50">
        @include('frontend.layouts.sidebar_nav')
      </div>
      <!-- End Profile Settings -->

      <!-- Wallet -->
        <div class="col-lg-9 g-mb-50">
          @if (session()->has('message'))
             <div class="alert alert-dismissible fade show g-bg-teal g-color-white rounded-0" role="alert">
                <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>

                <div class="media">
                    <span class="d-flex g-mr-10 g-mt-5">
                    <i class="icon-check g-font-size-25"></i>
                  </span>
                  <span class="media-body align-self-center">
                    <strong>Well done!</strong> {{ session()->get('message') }}
                  </span>
               </div>

            </div>
          @endif
              <!-- Info -->
          <form class="js-validate" action="{{ route('user.profile_update', $user->user_data->id) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="g-brd-around g-brd-gray-light-v4 rounded g-pa-30 g-mb-30">

            <div class="row">
              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1"><span class="text-danger">*</span>First Name:</span>
                <input type="text" name="first_name" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" placeholder="eg: John" value="{{ $user->user_data->first_name }}" required="" data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
              </div>

              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1">Last Name:</span>
                <input type="text" name="last_name" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" placeholder="eg: Doe" value="{{ $user->user_data->last_name }}">
              </div>

            </div>
            <hr class="g-brd-gray-light-v4 g-my-20">

            <div class="row">
              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1">Identity Number:</span>
                <input type="text" name="identity_number" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" placeholder="ID / Driver License / Other" value="{{ $user->user_data->identity_number }}">
              </div>

              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1">Phone Number:</span>
                <input type="text" name="phone_number" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" placeholder="eg: 08xxxxxxxx" value="{{ $user->user_data->phone_number }}">
              </div>

            </div>
            <hr class="g-brd-gray-light-v4 g-my-20">

            <div class="row">
              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1">Place of Birth:</span>
                <input type="text" name="place_of_birth" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" placeholder="eg: Jakarta" value="{{ $user->user_data->place_of_birth }}">
              </div>

              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1">Date of Birth:</span>
                <input type="text" name="date_of_birth" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15 datepicker" placeholder="yyyy-mm-dd" value="{{ $user->user_data->date_of_birth }}">
              </div>

            </div>
            <hr class="g-brd-gray-light-v4 g-my-20">


            <div class="row">
              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1"><span class="text-danger">*</span> Gender:</span>
                <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;" data-placeholder="Choose your State" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" name="sex">
                  <option value="Male" {{ $user->user_data->sex == 'Male' ? 'selected=selected' : '' }}>Male</option>
                  <option value="Female" {{ $user->user_data->sex == 'Female' ? 'selected=selected' : '' }}>Female</option>
                </select>
              </div>

              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1"><span class="text-danger">*</span> Country:</span>
                <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;" data-placeholder="Choose your Country" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" name="country_id">
                  <option value="Indonesia">Indonesia</option>
                </select>
              </div>

            </div>
            <hr class="g-brd-gray-light-v4 g-my-20">

            <div class="row">
              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1"><span class="text-danger">*</span> State or Province:</span>
                <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;" data-placeholder="Choose your State" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" name="province_id">
                  <option></option>
                  @foreach ($provinces as $province)
                    <option value="{{ $province['province_id'] }}" {{ $user->user_data->province_id == $province['province_id'] ? 'selected=selected' : '' }}>{{ $province['province'] }}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-6">
                <span class="d-block g-color-text g-font-size-13 mb-1"><span class="text-danger">*</span> City:</span>
                <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;" data-placeholder="Choose your City" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" name="city_id">
                  <option></option>
                  @if(count($cities) > 0)
                    @foreach ($cities as $city)
                    <option value="{{ $city['city_id'] }}" {{ $user->user_data->city_id == $city['city_id'] ? 'selected=selected' : '' }}>{{ $city['type'] }} {{ $city['city_name'] }}</option>
                    @endforeach
                  @endif
                </select>
              </div>

            </div>
            <hr class="g-brd-gray-light-v4 g-my-20">

            <div class="row">
              <div class="col-8">
                <span class="d-block g-color-text g-font-size-13 mb-1">Address:</span>
                <textarea class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="address" rows="5" placeholder="eg: 120 South Carolina Street ...">{{ $user->user_data->address }}</textarea>
              </div>

              <div class="col-4">
                <span class="d-block g-color-text g-font-size-13 mb-1">Zip or Postal Code:</span>
                <input type="text" name="zip" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" placeholder="Your Zip Code" value="{{ $user->user_data->zip }}">
              </div>

            </div>
            <hr class="g-brd-gray-light-v4 g-my-20">

          </div>
          <!-- End Info -->

          <button class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit">Update</button>
          </form>
        </div>

      <!-- End Wallet -->
    </div>
  </div>

@endsection

@push('js')
<script src="{{ url('assets/frontend/vendor/chosen/chosen.jquery.js') }}"></script>
<script src="{{ url('assets/frontend/vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.validation.js') }}"></script>

<script src="{{ url('assets/frontend/js/components/hs.select.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.count-qty.js') }}"></script>
<script src="{{ url('assets/frontend/js/pages/profile.js') }}"></script>
<script type="text/javascript">
  $.HSCore.components.HSSelect.init('.js-custom-select');
  $.HSCore.components.HSValidation.init('.js-validate');
</script>
@endpush