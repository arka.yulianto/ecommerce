@extends('frontend.layouts.master_left_sidebar')

@section('title')
Dashboard
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
	<div class="container">
		<div class="d-sm-flex text-center">
			<div class="align-self-center">
				<h1 class="h3 mb-0">Dashboard</h1>
			</div>

			<div class="align-self-center ml-auto">
				<ul class="u-list-inline">
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Home</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Pages</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-color-primary">
						<span>Dashboard</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="container g-pt-70 g-pb-30">
    <div class="row">
      <!-- Profile Settings -->
      <div class="col-lg-3 g-mb-50">
        @include('frontend.layouts.sidebar_nav')
      </div>
      <!-- End Profile Settings -->

      <!-- Wallet -->
      <div class="col-lg-9 g-mb-50">
        <div class="row justify-content-end g-mb-20 g-mb-0--md">
          <div class="col-md-7 g-mb-30">
            <!-- Search Form -->
            <form class="input-group g-pos-rel">
              <span class="g-pos-abs g-top-0 g-left-0 g-z-index-3 g-px-13 g-py-10">
                <i class="g-color-gray-dark-v4 g-font-size-12 icon-education-045 u-line-icon-pro"></i>
              </span>
              <input class="form-control u-form-control g-brd-around g-brd-gray-light-v3 g-brd-primary--focus g-font-size-13 g-rounded-3 g-pl-35 g-pa-0" type="search" placeholder="Search all orders">
              <div class="input-group-addon g-brd-none g-py-0">
                <button class="btn u-btn-black g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit">Search Orders</button>
              </div>
            </form>
            <!-- End Search Form -->
          </div>
        </div>

        <p class="g-brd-bottom g-brd-2 g-brd-transparent g-color-main g-color-gray-dark-v4 g-color-primary--hover g-text-underline--none--hover g-px-10 g-pb-13 g-font-weight-600">Orders</p>
        <!-- End Links -->

        <!-- Order Block -->
        @if (count($orders) > 0)
        @foreach ($orders as $order)
        <div class="g-brd-around g-brd-gray-light-v4 rounded g-mb-30">
          <header class="g-bg-gray-light-v5 g-pa-20">
            <div class="row">
              <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Order Date</h4>
                <span class="g-color-black g-font-weight-300 g-font-size-13">{{ Carbon\Carbon::parse($order->created_at)->format('M jS, Y') }}</span>
              </div>

              <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Total</h4>
                <span class="g-color-black g-font-weight-300 g-font-size-13">{{ Helper::currency($order->total) }}</span>
              </div>

              <div class="col-sm-3 col-md-2 g-mb-20 g-mb-0--sm">
                <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Ship to</h4>
                <span class="g-color-black g-font-weight-300 g-font-size-13">{{ $order->shipping->first_name.' '.$order->shipping->last_name }}</span>
              </div>

              <div class="col-sm-3 col-md-4 ml-auto text-sm-right">
                <h4 class="g-color-gray-dark-v4 g-font-weight-400 g-font-size-12 text-uppercase g-mb-2">Order #{{ $order->order_number }}</h4>
                <span class="g-font-weight-300 g-font-size-13" style="color: #fff !important">
                  {!! Helper::getStatusTransaction($order->status) !!}
                </span>
                <!-- <a class="g-font-weight-300 g-font-size-13" href="#!">Invoice</a> -->
              </div>
            </div>
          </header>

          <!-- Order Content -->
          <div class="g-pa-20">
            <div class="row">
              <div class="col-md-8"> 
                @foreach ($order->details as $detail)
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-4 col-sm-3 g-mb-30">
                        <img class="img-fluid" src="{{ url('uploads/images/150x150/'.$detail->product->media->name) }}" alt="{{ $detail->product->name }}">
                      </div>

                      <div class="col-8 col-sm-9 g-mb-30">
                        <h4 class="h6 g-font-weight-400"><a href="{{ route('shop.details', $detail->product->slug) }}">{{ $detail->product->name }}</a></h4>
                        @foreach ($detail->attributes as $attribute)
                        <span class="d-block g-color-gray-dark-v4 g-font-size-13 mb-2">{{ $attribute->attribute_name }}: {{ $attribute->attribute_value }}</span>
                        @endforeach
                        <span class="d-block g-color-lightred mb-2">{{ Helper::currency($detail->price) }} x({{ $detail->qty }})</span>
                      </div>
                    </div>
                  </div>
                @endforeach
                </div>
                <div class="col-md-4">
                  <div class="g-mb-50">
                    <h3 class="h5 mb-3 g-font-weight-300">Shipping Information</h3>

                    <address class="media">
                      <div class="d-flex mr-3">
                        <span class="u-icon-v1 g-color-gray-dark-v4"><i class="fa fa-truck"></i></span>
                      </div>

                      <div class="media-body g-color-text">
                        {{ strtoupper($order->shipping->courier_id) }}
                        <br>
                         {{ $order->shipping->service_name }} ({{ $order->shipping->service_description }})
                        <br>
                        {{ $order->shipping->estimate_delivery }} (Days)
                      </div>
                    </address>

                    <!-- Address -->
                    <address class="media">
                      <div class="d-flex mr-3">
                        <span class="u-icon-v1 g-color-gray-dark-v4"><i class="icon-real-estate-027 u-line-icon-pro"></i></span>
                      </div>

                      <div class="media-body g-color-text">
                        {{ $order->shipping->address }}
                        <br>
                        {{ Helper::getCity($order->shipping->city_id) }}
                        <br>
                        {{ Helper::getProvince($order->shipping->province_id) }}
                        <br>
                        {{ $order->shipping->country_id }}
                        <br>
                      </div>
                    </address>
                    <!-- End Address -->

                    <!-- Phone -->
                    <div class="media">
                      <div class="d-flex mr-3">
                        <span class="u-icon-v1 g-color-gray-dark-v4"><i class="icon-electronics-005 u-line-icon-pro"></i></span>
                      </div>
                      <div class="media-body g-color-text">
                        {{ $order->shipping->phone_number }}
                      </div>
                    </div>
                    <!-- End Phone -->

                    @if (!empty($order->shipping->receipt_number))
                     <small class="text-muted">Receipt Number : #{{ $order->shipping->receipt_number }}</small>
                    @endif

                  </div>
                </div>

            </div>
          </div>
          <!-- End Order Content -->
        </div>
        @endforeach
        <!-- End Order Block -->

        <!-- Pagination -->
        {{ $orders->links('vendor.pagination.shop') }}
        <!-- End Pagination -->
        @else
          <div class="col-md-12 text-center">
            <p class="text-muted">No order yet</p>
          </div>
        @endif

      </div>
      <!-- End Wallet -->
    </div>
  </div>

@endsection