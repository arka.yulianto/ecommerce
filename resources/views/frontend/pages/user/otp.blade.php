@extends('frontend.layouts.master_left_sidebar')

@section('title')
Dashboard
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
  <div class="container">
    <div class="d-sm-flex text-center">
      <div class="align-self-center">
        <h1 class="h3 mb-0">Dashboard</h1>
      </div>

      <div class="align-self-center ml-auto">
        <ul class="u-list-inline">
          <li class="list-inline-item g-mr-5">
            <a class="u-link-v5 g-color-text" href="#!">Home</a>
            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
          </li>
          <li class="list-inline-item g-mr-5">
            <a class="u-link-v5 g-color-text" href="#!">Pages</a>
            <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
          </li>
          <li class="list-inline-item g-color-primary">
            <span>Dashboard</span>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>

<div class="container g-pt-70 g-pb-30">
    <div class="row">
      <!-- Profile Settings -->
      <div class="col-lg-3 g-mb-50">
        @include('frontend.layouts.sidebar_nav')
      </div>
      <!-- End Profile Settings -->

      <!-- Wallet -->
      <div class="col-lg-9 g-mb-50">
        <p>We just send OTP to {{ auth()->user()->user_data->phone_number }}, to make sure that real you</p>

        <form action="{{ route('user.otp_submit') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group{{ session()->get('message') ? ' u-has-error-v1' : '' }}">
            <label class="control-label">Enter OTP<span class="text-danger">*</span></label>
            <input type="text" name="otp" class="u-form-control g-placeholder-gray-light-v1 form-control" required="required">
             @if (session()->has('message'))
                <label class="error">{{ session()->get('message') }}</label>
              @endif
          </div>

          <button class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit" name="resend_otp">Resend OTP</button>
          <button class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit" name="submit_otp">Submit OTP</button>
        </form>

      </div>
      <!-- End Wallet -->
    </div>
  </div>

@endsection