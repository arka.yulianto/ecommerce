@extends('frontend.layouts.master_left_sidebar')

@section('title')
Dashboard
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
	<div class="container">
		<div class="d-sm-flex text-center">
			<div class="align-self-center">
				<h1 class="h3 mb-0">Dashboard</h1>
			</div>

			<div class="align-self-center ml-auto">
				<ul class="u-list-inline">
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Home</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Pages</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-color-primary">
						<span>Dashboard</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="container g-pt-70 g-pb-30">
    <div class="row">
      <!-- Profile Settings -->
      <div class="col-lg-3 g-mb-50">
        @include('frontend.layouts.sidebar_nav')
      </div>
      <!-- End Profile Settings -->

      <!-- Wallet -->
      <div class="col-lg-9 g-mb-50">
        <p>Change Password</p>

        @if (session()->has('message'))
           <div class="alert alert-dismissible fade show {{ session()->get('message')['type'] == 'warning' ? 'bg-warning' : 'bg-success' }} g-color-white rounded-0" role="alert">
              <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>

              <div class="media">
                  <span class="d-flex g-mr-10 g-mt-5">
                  <i class="fa fa-info-circle g-font-size-20"></i>
                </span>
                <span class="media-body align-self-center">
                  <strong>{{ session()->get('message')['type'] == 'warning' ? 'Warning' : 'Well Done' }}</strong> {{ session()->get('message')['content'] }}
                </span>
             </div>

          </div>
        @endif

        <form action="{{ route('user.change_password') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group{{ $errors->has('password') ? ' u-has-error-v1' : '' }}">
            <label class="control-label">Enter new password <span class="text-danger">*</span></label>
            <input type="password" name="password" class="u-form-control g-placeholder-gray-light-v1 form-control" required="required">
             @if ($errors->has('password'))
                <label class="error">{{ $errors->first('password') }}</label>
              @endif
          </div>

          <div class="form-group{{ $errors->has('repeat_password') ? ' u-has-error-v1' : '' }}">
            <label class="control-label">Repeat new password <span class="text-danger">*</span></label>
            <input type="password" name="repeat_password" class="u-form-control g-placeholder-gray-light-v1 form-control" required="required">
            @if ($errors->has('repeat_password'))
              <label class="error">{{ $errors->first('repeat_password') }}</label>
            @endif
          </div>

          <button class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" type="submit">Change Password</button>
        </form>

      </div>
      <!-- End Wallet -->
    </div>
  </div>

@endsection