@extends('frontend.layouts.master_left_sidebar')

@section('title')
Dashboard
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
	<div class="container">
		<div class="d-sm-flex text-center">
			<div class="align-self-center">
				<h1 class="h3 mb-0">Dashboard</h1>
			</div>

			<div class="align-self-center ml-auto">
				<ul class="u-list-inline">
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Home</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Pages</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-color-primary">
						<span>Dashboard</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="container g-pt-70 g-pb-30">
    <div class="row">
      <!-- Profile Settings -->
      <div class="col-lg-3 g-mb-50">
        @include('frontend.layouts.sidebar_nav')
      </div>
      <!-- End Profile Settings -->

      <!-- Wallet -->
      <div class="col-lg-9 g-mb-50">
        <p class="g-brd-bottom g-brd-2 g-brd-transparent g-color-main g-color-gray-dark-v4 g-color-primary--hover g-text-underline--none--hover g-px-10 g-pb-13 g-font-weight-600">Reviews</p>
        <!-- End Links -->

        <!-- Order Block -->
        <div class="g-brd-around g-brd-gray-light-v4 rounded g-mb-30">
          <!-- Order Content -->
          <div class="g-pa-20">
            <div class="row">
              @foreach ($reviews as $review)
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-4 col-sm-3 g-mb-30">
                      <img class="img-fluid" src="{{ url('uploads/images/150x150/'.$review->product->media->name) }}" alt="{{ $review->product->name }}">
                    </div>

                    <div class="col-8 col-sm-9 g-mb-30">
                      {!! Helper::getRate($review->rate) !!}
                      <p>{{ $review->content }}</p>
                      <small class="text-muted">{{ Carbon\Carbon::parse($review->created_at)->format('M jS, Y') }}</small>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
          <!-- End Order Content -->
        </div>
        <!-- End Order Block -->

        <!-- Pagination -->
        {{ $reviews->links('vendor.pagination.shop') }}
        <!-- End Pagination -->
      </div>
      <!-- End Wallet -->
    </div>
  </div>

@endsection