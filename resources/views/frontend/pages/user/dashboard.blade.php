@extends('frontend.layouts.master_left_sidebar')

@section('title')
Dashboard
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
	<div class="container">
		<div class="d-sm-flex text-center">
			<div class="align-self-center">
				<h1 class="h3 mb-0">Dashboard</h1>
			</div>

			<div class="align-self-center ml-auto">
				<ul class="u-list-inline">
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Home</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-mr-5">
						<a class="u-link-v5 g-color-text" href="#!">Pages</a>
						<i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
					</li>
					<li class="list-inline-item g-color-primary">
						<span>Dashboard</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<div class="container g-pt-70 g-pb-30">
    <div class="row">
      <!-- Profile Settings -->
      <div class="col-lg-3 g-mb-50">
        @include('frontend.layouts.sidebar_nav')
      </div>
      <!-- End Profile Settings -->

      <!-- Wallet -->
      <div class="col-lg-9 g-mb-50">
        <!-- Balance & Rewards -->
        <div class="g-brd-around g-brd-gray-light-v4 rounded g-px-30 g-pt-30">
          <h3 class="h5 mb-3">Dashboard</h3>

          <div class="row">

            <div class="col-sm-4 g-mb-30">
            @if (count($orders) > 0)
              
              @foreach ($orders as $order)
              <!-- Payment History -->
              <h3 class="h5 mb-3"># {{ $order->order_number }} <span style="color: #fff">{!! Helper::getStatusTransaction($order->status) !!}</span></h3>
                <p class="mb-0">On {{ Carbon\Carbon::parse($order->created_at)->format('F jS, Y') }}</p>
                <a class="g-font-size-13" href="#!">View Order</a>
              <!-- End Payment History -->
              @endforeach

            @else
              <p>No order yet</p>
            @endif

            </div>

            <div class="col-sm-8 g-mb-30">
              <h3 class="h5 mb-3">Last Reviews</h3>

              @if (count($comments) > 0)

              @foreach ($comments as $comment)
              <!-- Media -->
              <div class="media g-mb-30">
                <div class="d-flex mr-4">
                  <span class="u-icon-v2 u-icon-size--sm g-brd-gray-dark-v5 g-color-main rounded-circle">
                    <!-- <img src="{{ url('uploads/images/150x150/'.$comment->product->media->name) }}" class="img img-responsive"> -->
                    {{ $comment->rate }}
                  </span>
                </div>
                <div class="media-body">
                  <h4 class="h6 mb-1">{{ $comment->product->name }}</h4>
                  <p class="mb-1">
                    {{ $comment->content }}
                  </p>
                  <a class="g-font-size-13" href="{{ route('shop.details', $comment->product->slug) }}">Read More</a>
                </div>
              </div>
              <!-- End Media -->
              @endforeach

              @else

              <div class="media-body">
                <p class="text-muted">No reviews yet</p>
              </div>

              @endif

            </div>
          </div>
        </div>
        <!-- End Balance & Rewards -->
      </div>
      <!-- End Wallet -->
    </div>
  </div>

@endsection