@extends('frontend.layouts.master_left_sidebar')
@section('title')
Cart
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Home</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Pages</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>Checkout</span>
        </li>
      </ul>
    </div>
</section>


<div class="container g-pt-100 g-pb-70">
      <div class="g-mb-100">
        <!-- Step Titles -->
        <ul id="stepFormProgress" class="js-step-progress row justify-content-center list-inline text-center g-font-size-17 mb-0">
            <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm g-checked">
              <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-primary g-color-primary g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                <i class="g-font-style-normal g-font-weight-700 g-hide-check">1</i>
                <i class="fa fa-check g-show-check"></i>
              </span>
              <h4 class="g-font-size-16 text-uppercase mb-0">Shopping Cart</h4>
            </li>

            <li class="col-3 list-inline-item g-mb-20 g-mb-0--sm active">
              <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                <i class="g-font-style-normal g-font-weight-700 g-hide-check">2</i>
                <i class="fa fa-check g-show-check"></i>
              </span>
              <h4 class="g-font-size-16 text-uppercase mb-0">Shipping</h4>
            </li>

            <li class="col-3 list-inline-item">
              <span class="d-block u-icon-v2 u-icon-size--sm g-rounded-50x g-brd-gray-light-v2 g-color-gray-dark-v5 g-brd-primary--active g-color-white--parent-active g-bg-primary--active g-color-white--checked g-bg-primary--checked mx-auto mb-3">
                <i class="g-font-style-normal g-font-weight-700 g-hide-check">3</i>
                <i class="fa fa-check g-show-check"></i>
              </span>
              <h4 class="g-font-size-16 text-uppercase mb-0">Payment &amp; Review</h4>
            </li>
          </ul>
        <!-- End Step Titles -->
      </div>
     <form action="{{ route('shipping.store') }}" method="post" class="js-validate">
      {{ csrf_field() }}
      <div id="stepFormSteps">
        <!-- Shopping Cart -->
        <div id="step2" style="" class="active">
              <div class="row">
                <div class="col-md-8 g-mb-30">
                  <div class="row">
                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">First Name</label>
                        <input id="inputGroup4" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="first_name" type="text" placeholder="eg: John" required="" data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true" value="{{ !empty($shipping['first_name']) ? $shipping['first_name'] : '' }}">
                      </div>
                    </div>

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Last Name</label>
                        <input id="inputGroup5" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="last_name" type="text" placeholder="eg: Doe" required="" data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true" value="{{ !empty($shipping['last_name']) ? $shipping['last_name'] : '' }}">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Email Address</label>
                        <input id="inputGroup6" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="email" type="email" placeholder="youremail@example.com" required="" data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true" value="{{ !empty($shipping['email']) ? $shipping['email'] : '' }}">
                      </div>
                    </div>

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Phone Number</label>
                        <input id="inputGroup6" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="phone_number" type="text" placeholder="Country Code + Phone Number" required="" data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true"  value="{{ !empty($shipping['phone_number']) ? $shipping['phone_number'] : '' }}">
                      </div>
                    </div>

                  </div>

                  <div class="row">

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Country</label>
                        <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;" data-placeholder="Choose your Country" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" name="country_id">
                          <option value="Indonesia">Indonesia</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">State/Province</label>
                        <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;" data-placeholder="Choose your State" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" name="province_id">
                          <option></option>
                          @foreach ($provinces as $province)
                            <option value="{{ $province['province_id'] }}" {{ !empty($shipping['province_id']) ? $shipping['province_id'] == $province['province_id'] ? 'selected=selected' : '' : '' }}>{{ $province['province'] }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">City</label>
                        <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;" data-placeholder="Choose your City" data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" name="city_id">
                          <option></option>
                          @if(count($cities) > 0)
                            @foreach ($cities as $city)
                            <option value="{{ $city['city_id'] }}" {{ $shipping['city_id'] == $city['city_id'] ? 'selected=selected' : '' }}>{{ $city['type'] }} {{ $city['city_name'] }}</option>
                            @endforeach
                          @endif
                        </select>
                      </div>
                    </div>

                    <div class="col-sm-6 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Zip/Postal Code</label>
                        <input id="inputGroup9" class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="zip" type="text" placeholder="eg: AB123" required="" data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true" value="{{ !empty($shipping['zip']) ? $shipping['zip'] : '' }}">
                      </div>
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-sm-12 g-mb-20">
                      <div class="form-group">
                        <label class="d-block g-color-gray-dark-v2 g-font-size-13">Address</label>
                        <textarea class="form-control u-form-control g-placeholder-gray-light-v1 rounded-0 g-py-15" name="address" placeholder="Street name, Street Number and other thing" required="" data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true" rows="5">{{ !empty($shipping['address']) ? $shipping['address'] : '' }}</textarea>
                      </div>
                    </div>
                  </div>

                  <hr class="g-mb-50">

                  <h4 class="h6 text-uppercase mb-5">Shipping method</h4>

                  <div class="form-group mb-5">
                    <label class="d-block g-color-gray-dark-v2 g-font-size-13">Courier</label>
                    <select class="js-custom-select u-select-v1 g-brd-gray-light-v2 g-color-gray-light-v1 g-py-12" style="width: 100%;" data-placeholder="Choose your Courier " data-open-icon="fa fa-angle-down" data-close-icon="fa fa-angle-up" required data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" name="courier_id">
                      <option></option>
                      <option value="jne" {{ !empty($shipping['courier_id']) ? $shipping['courier_id'] == 'jne' ? 'selected=selected' : '' : '' }}>JNE</option>
                      <option value="tiki" {{ !empty($shipping['courier_id']) ? $shipping['courier_id'] == 'tiki' ? 'selected=selected' : '' : '' }}>TIKI</option>
                      <option value="pos" {{ !empty($shipping['courier_id']) ? $shipping['courier_id'] == 'pos' ? 'selected=selected' : '' : '' }}>POS Indonesia</option>
                    </select>
                  </div>

                  <input type="text" name="total_weight" value="{{ $weight }}" hidden="hidden">
                  <input type="text" name="cost" value="{{ !empty($shipping['cost']) ? $shipping['cost'] : '' }}" hidden="hidden">
                  <input type="text" name="service_name" value="{{ !empty($shipping['service_name']) ? $shipping['service_name'] : '' }}" hidden="hidden">
                  <input type="text" name="service_description" value="{{ !empty($shipping['service_description']) ? $shipping['service_description'] : '' }}" hidden="hidden">
                  <input type="text" name="estimate_delivery" value="{{ !empty($shipping['estimate_delivery']) ? $shipping['estimate_delivery'] : '' }}" hidden="hidden">
                  <input type="text" name="cost" value="{{ !empty($shipping['cost']) ? $shipping['cost'] : '' }}" hidden="hidden">

                  <!-- Shipping Mehtod -->
                  <table class="mb-5" style="width: 100%" id="table-service">
                    <thead class="h6 g-brd-bottom g-brd-gray-light-v3 g-color-gray-dark-v3 g-font-size-13">
                      <tr>
                        <th class="g-width-70 g-font-weight-500 g-pa-0 g-pb-10">Services Name</th>
                        <th class="g-width-70 g-font-weight-500 g-pa-0 g-pb-10">Services Description</th>
                        <th class="g-width-110 g-font-weight-500 g-pa-0 g-pb-10">Estimate Delivery</th>
                        <th class="g-width-70 g-font-weight-500 text-right g-pa-0 g-pb-10">Cost</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if (count($services) > 0)
                        @foreach ($services[0]['costs'] as $index => $cost)
                          <tr class="g-color-gray-dark-v4 g-font-size-13">
                            <td class="align-top g-py-10">
                              <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                              <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="service_id" type="radio" onclick="get_cost()" data-cost-value="{{ Helper::setCurrency($cost['cost'][0]['value'], 'idr') }}" data-service-name="{{ $cost['service'] }}" data-service-description="{{ $cost['description'] }}" data-estimate="{{ $cost['cost'][0]['etd'] }}" {{ $cost['service'] == $shipping['service_name'] ? 'checked=checked' : '' }} required="" data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                                <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                  <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                {{ $cost['service'] }}
                              </label>
                            </td>
                            <td class="align-top g-py-10">{{ $cost['description'] }}</td>
                            <td class="align-top g-py-10">{{ $cost['cost'][0]['etd'] }} Day(s)</td>
                            <td class="align-top g-py-10 text-right">{{ Helper::currency(Helper::setCurrency($cost['cost'][0]['value'], 'idr')) }}</td>
                          </tr>
                        @endforeach
                      @else
                      <tr class="g-color-gray-dark-v4 g-font-size-13" id="empty-row">
                        <td colspan="4" class="text-center align-top g-py-10">No services</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                  <!-- End Shipping Mehtod -->

                  <hr>
                  <button class="btn u-btn-primary g-font-size-13 text-uppercase g-px-40 g-py-15" type="submit" name="submit_shipping">Proceed to Payment</button>
                </div>

                <div class="col-md-4 g-mb-30">
                  <!-- Order Summary -->
                  <div class="g-bg-gray-light-v5 g-pa-20 g-pb-50 mb-4">
                    <h4 class="h6 text-uppercase mb-3">Order summary</h4>

                    <!-- Accordion -->
                    <div id="accordion-03" class="mb-4" role="tablist" aria-multiselectable="true">
                      <div id="accordion-03-heading-03" class="g-brd-y g-brd-gray-light-v2 py-3" role="tab">
                        <h5 class="g-font-weight-400 g-font-size-default mb-0">
                          <a class="g-color-gray-dark-v4 g-text-underline--none--hover" href="#accordion-03-body-03" data-toggle="collapse" data-parent="#accordion-03" aria-expanded="false" aria-controls="accordion-03-body-03">{{ count($products) }} items in cart
                            <span class="ml-3 fa fa-angle-down"></span></a>
                        </h5>
                      </div>
                      <div id="accordion-03-body-03" class="collapse" role="tabpanel" aria-labelledby="accordion-03-heading-03">
                        <div class="g-py-15">
                          <ul class="list-unstyled mb-3">
                            @foreach($products as $product)
                            <!-- Product -->
                            <li class="d-flex justify-content-start">
                              <img class="g-width-100 g-height-100 mr-3" src="{{ url('uploads/images/150x150/'.$product->picture) }}" alt="{{ $product->name }}">
                              <div class="d-block">
                                <h4 class="h6 g-color-black">{{ $product->name }}</h4>
                                <ul class="list-unstyled g-color-gray-dark-v4 g-font-size-12 g-line-height-1_4 mb-1">
                                  @foreach ($product->attributes as $attribute)
                                    <li>{{ $attribute['name'] }} : {{ $attribute['value'] }} </li>
                                  @endforeach
                                </ul>
                                <span class="d-block g-color-black g-font-weight-400">{{ Helper::currency($product->price) }}</span>
                              </div>
                            </li>
                            <!-- End Product -->
                            @endforeach

                          </ul>
                        </div>
                      </div>
                    </div>
                    <!-- End Accordion -->

                    <div class="d-flex justify-content-between mb-2">
                      <span class="g-color-black">Subtotal</span>
                      <span class="g-color-black g-font-weight-300">{{ Helper::currency(LaraCart::subTotal($format = false, $withDiscount = true)) }}</span>
                    </div>
                    <div class="d-flex justify-content-between">
                      <span class="g-color-black">Order Total</span>
                      <span class="g-color-black g-font-weight-300">{{ Helper::currency(LaraCart::total($format = false, $withDiscount = true, $withTax = true, $withFees = true)) }}</span>
                    </div>
                  </div>
                  <!-- End Order Summary -->
                </div>
              </div>
            </div>

        <!-- End Shipping -->
      </div>
    </form>
  </div>

@endsection

@push('js')

<script src="{{ url('assets/frontend/vendor/chosen/chosen.jquery.js') }}"></script>
<script src="{{ url('assets/frontend/vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.validation.js') }}"></script>

<script src="{{ url('assets/frontend/js/components/hs.select.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.count-qty.js') }}"></script>
<script src="{{ url('assets/frontend/js/pages/cart.js') }}"></script>
<script src="{{ url('assets/frontend/js/pages/shipping2.js') }}"></script>
<script>
  $.HSCore.components.HSSelect.init('.js-custom-select');
  $.HSCore.components.HSValidation.init('.js-validate');
</script>

@endpush