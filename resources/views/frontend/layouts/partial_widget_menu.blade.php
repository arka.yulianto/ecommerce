@if (!empty($menu->tag_open))
{!! $menu->tag_open !!}
@else
<div class="col-sm-6 col-lg-2 g-mb-30 g-mb-0--md">
@endif

@if ($menu->widget->type == 'categories')
  
  <!-- Links -->
  <div class="mb-5">
    <span class="d-block g-font-weight-500 text-uppercase mb-2">{{ $menu->widget->categories->name }}</span>
    <ul class="list-unstyled">
      @foreach ($menu->widget->categories->child as $category)
      <li><a class="d-block g-color-text g-color-primary--hover g-text-underline--none--hover g-py-5" href="#!">{{ $category->name }}</a></li>
      @endforeach
    </ul>
  </div>

  @if (count($menu->child) > 0)

    @foreach ($menu->child as $menu)

      <div class="mb-5">
        <span class="d-block g-font-weight-500 text-uppercase mb-2">{{ $menu->widget->categories->name }}</span>
        <ul class="list-unstyled">
          @foreach ($menu->widget->categories->child as $category)
          <li><a class="d-block g-color-text g-color-primary--hover g-text-underline--none--hover g-py-5" href="#!">{{ $category->name }}</a></li>
          @endforeach
        </ul>
      </div>

    @endforeach

  @endif

@elseif ($menu->widget->type == 'text')
  {!! $menu->widget->content !!}
@else

@php($product = App\Product::randomProduct($menu->widget->category_id))

  <article class="g-pos-rel">
    <img class="img-fluid" src="{{ url('uploads/images/'.$menu->widget->image_dimension .'/'.$product->media->name) }}" alt="{{ $product->name }}">

    <div class="g-pos-abs g-bottom-30 g-left-30">
      <span class="d-block g-color-gray-dark-v4 mb-2">{{ $product->name }}</span>
      @foreach ($product->categories as $category)
        <span class="d-block h4">{{ $category->name }}</span>
      @endforeach

      @if (!empty($product->discount))
        <sup class="text-muted"><s>{{ Helper::currency($product->price) }}</s></sup>
        <span class="d-block g-color-gray-dark-v3 g-font-size-16 mb-4">{{ Helper::currency($product->discount) }}</span>
      @else
        <span class="d-block g-color-gray-dark-v3 g-font-size-16 mb-4">{{ Helper::currency($product->price) }}</span>
      @endif
      <a class="btn u-btn-primary u-shadow-v29 g-font-size-12 text-uppercase g-py-10 g-px-20" href="#!">Add to Cart</a>
    </div>
  </article>

@endif

@if (!empty($menu->tag_close))
{!! $menu->tag_close !!}
@else
</div>
@endif