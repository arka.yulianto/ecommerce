<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title> Ecommerce | @yield('title') </title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url('assets/frontend/favicon.ico') }}">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C500%2C700%7CPlayfair+Display%7CRaleway%7CSpectral%7CRubik">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/icon-line/css/simple-line-icons.css') }}">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/icon-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/icon-line-pro/style.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/slick-carousel/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/icon-hs/style.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/animate.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/hamburgers/hamburgers.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/hs-megamenu/src/hs.megamenu.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/dzsparallaxer/dzsparallaxer.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/dzsparallaxer/dzsscroller/scroller.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/dzsparallaxer/advancedscroller/plugin.css') }}">

    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/chosen/chosen.css') }}">


    <!-- Revolution Slider -->
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/revolution-slider/revolution/css/settings.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/revolution-slider/revolution/css/layers.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/revolution-slider/revolution/css/navigation.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/chosen/chosen.css') }}">

    <!-- CSS Unify Theme -->
    <link rel="stylesheet" href="{{ url('assets/frontend/css/styles.e-commerce.css') }}">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ url('assets/frontend/css/custom.css') }}">
    <script>
        var SITE_URL = "{{ url('/') }}";
    </script>
  </head>

  <body>
    <main>
      <!-- Header -->
      <header id="js-header" class="u-header u-header--static u-shadow-v19">
        @include('frontend.layouts.header')
      </header>
      <!-- End Header -->


      @yield('content')
    

      <!-- Social Icons -->
      <div class="container g-mb-30">
        @include('frontend.layouts.social')
      </div>
      <!-- End Social Icons -->

      <!-- Footer -->
      <footer>
        <!-- Content -->
        @include('frontend.layouts.footer')
        <!-- End Copyright -->
      </footer>
      <!-- End Footer -->

      <a class="js-go-to u-go-to-v2" href="#!"
         data-type="fixed"
         data-position='{
           "bottom": 15,
           "right": 15
         }'
         data-offset-top="400"
         data-compensation="#js-header"
         data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
      </a>
    </main>

    <div class="u-outer-spaces-helper"></div>

    <!-- JS Global Compulsory -->
    <script src="{{ url('assets/frontend/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/jquery-migrate/jquery-migrate.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/popper.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/bootstrap/bootstrap.min.js') }}"></script>

    <!-- JS Implementing Plugins -->
    <script src="{{ url('assets/frontend/vendor/jquery.countdown.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/slick-carousel/slick/slick.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/hs-megamenu/src/hs.megamenu.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/jquery-ui/ui/widget.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/jquery-ui/ui/widgets/menu.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/jquery-ui/ui/widgets/mouse.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/jquery-ui/ui/widgets/slider.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/dzsparallaxer/dzsparallaxer.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/dzsparallaxer/dzsscroller/scroller.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/dzsparallaxer/advancedscroller/plugin.js') }}"></script>
    <!-- JS Master Slider -->
    <script src="{{ url('assets/frontend/vendor/master-slider/source/assets/js/masterslider.min.js') }}"></script>

    <!-- JS Unify -->
    <script src="{{ url('assets/frontend/js/hs.core.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.header.js') }}"></script>
    <script src="{{ url('assets/frontend/js/helpers/hs.hamburgers.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.dropdown.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.scrollbar.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.countdown.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.carousel.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.go-to.js') }}"></script>
    <script src="{{ url('assets/frontend/js/helpers/hs.rating.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.slider.js') }}"></script>

    <!-- JS Revolution Slider -->
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>

    <!-- JS Customization -->
    <script src="{{ url('assets/frontend/js/custom.js') }}"></script>

    <!-- JS Plugins Init. -->
    @stack('js')
  </body>
</html>
