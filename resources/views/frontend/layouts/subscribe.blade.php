<div class="g-px-30 g-px-50--md">
  <div class="mb-3">
    <span class="d-inline-block g-width-3 g-height-10 g-bg-gray-light-v1 g-mb-4"></span><br>
    <span class="d-inline-block g-width-3 g-height-16 g-bg-gray-dark-v5"></span><br>
    <span class="d-inline-block g-color-gray-dark-v4 fa fa-arrow-down"></span>
  </div>

  <h2 class="h3 g-mb-30">Subscribe</h2>

  <form class="input-group u-shadow-v21 rounded g-mb-50">
    <input class="form-control g-brd-gray-light-v4 g-brd-right-none g-color-gray-dark-v4 g-placeholder-gray-dark-v3 rounded g-px-25 g-py-17" type="email" placeholder="Enter your email">
    <span class="input-group-addon u-shadow-v19 g-brd-gray-light-v4 g-bg-white">
      <button class="btn u-btn-primary g-font-size-12 rounded text-uppercase g-py-12 g-px-25" type="submit">Submit</button>
    </span>
  </form>
</div>