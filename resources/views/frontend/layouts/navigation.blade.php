<ul class="navbar-nav mx-auto">
  @foreach (App\Menu::getMenu(0) as $menu)
    @if (count($menu->child) > 0)
      @if ($menu->is_mega)
        @include('frontend.layouts.partial_mega', $menu)
      @else
        @include('frontend.layouts.partial_menu', $menu)
      @endif
    @else
      <li class="nav-item g-mx-10--lg g-mx-15--xl">
        <a id="nav-link--home" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20" href="{{ $menu->url }}">
          {{ $menu->name }}
        </a>
      </li>
    @endif
  @endforeach
</ul>