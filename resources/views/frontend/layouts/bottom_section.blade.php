<div class="row align-items-center no-gutters">
  @foreach (App\Category::getCategory(1) as $category)
  <div class="col-md-7 col-lg-8 u-block-hover g-bg-size-cover g-min-height-500 g-flex-centered" data-bg-img-src="{{ url('uploads/images/1920x1080/'.$category->media->name) }}">
    <div class="g-flex-centered-item text-right g-pa-50">
      <h2 class="g-font-weight-700 g-font-size-50 text-uppercase g-line-height-1 mb-4" style="color: #aaa;">{!! collect(explode(' ', $category->name))->implode('<br>') !!}
      </h2>
      <span class="u-link-v5 g-color-black g-color-primary--hover g-font-size-18">Shop Now</span>
    </div>

    <a class="u-link-v2" href="#!"></a>

    @php ($category_id = $category->id)
  </div>
  @endforeach

  @php ($random_product = App\Product::randomProduct($category_id))
  <div class="col-md-5 col-lg-4 u-block-hover g-bg-size-cover g-min-height-500 g-flex-centered" data-bg-img-src="{{ url('assets/frontend/img/bg/secondary.png') }}">
    <div class="text-center">
      <img class="img-fluid mb-3" src="{{ url('uploads/images/700x467/'.$random_product->media->name) }}" alt="{{ $random_product->name }}">
      <h3 class="h5 u-link-v5 g-color-primary--hover g-font-weight-400 mb-3">{{ $random_product->name }}</h3>
      <span class="g-color-primary g-font-weight-700 g-font-size-20">
        @if (!empty($random_product->discount))
          <sup class="text-muted"><s>{{ Helper::currency($random_product->price) }}</s></sup>
          {{ Helper::currency($random_product->discount) }}
        @else
          {{ Helper::currency($random_product->price) }}
        @endif
      </span>
    </div>

    <a class="u-link-v2" href="#!"></a>
  </div>
</div>