<aside class="g-brd-around g-brd-gray-light-v4 rounded g-px-20 g-py-30">
  <!-- Profile Picture -->
  <div class="text-center g-pos-rel g-mb-30">
    <div class="g-width-100 g-height-100 mx-auto mb-3">
      <img class="img-fluid rounded-circle" src="{{ url(auth()->user()->user_data->picture) }}" alt="{{ auth()->user()->name }}">
    </div>

    <span class="d-block g-font-weight-500">{{ auth()->user()->user_data->fullname }}</span>

    <span class="u-icon-v3 u-icon-size--xs g-color-white--hover g-bg-primary--hover rounded-circle g-pos-abs g-top-0 g-right-15 g-cursor-pointer" title="" data-toggle="tooltip" data-placement="top" data-original-title="Change Profile Picture">
      <i class="icon-finance-067 u-line-icon-pro"></i>
    </span>
  </div>
  <!-- End Profile Picture -->

  <hr class="g-brd-gray-light-v4 g-my-30">

  <!-- Profile Settings List -->

  {!! $userNavbar->asUl(['class' => 'list-unstyled mb-0 navbar-user']) !!}

  <!-- End Profile Settings List -->
</aside>