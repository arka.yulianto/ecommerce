        <!-- Top Bar -->
        <div class="u-header__section g-brd-bottom g-brd-gray-light-v4 g-transition-0_3">
          <div class="container">
            <div class="row justify-content-between align-items-center g-mx-0--lg">
              <div class="col-12 col-sm-auto order-sm-2 g-py-5 g-py-20--sm text-center">
                <!-- Logo -->
                <a class="navbar-brand" href="home-page-1.html">
                  <img src="{{ url('assets/frontend/img/logo/logo-1.png') }}" alt="Image Description">
                </a>
                <!-- End Logo -->
              </div>

              <div class="col-6 col-sm-auto order-sm-1 g-pl-0--sm g-py-5 g-py-20--sm">
                <!-- Search -->
                <div class="d-inline-block g-valign-middle">
                  <div class="g-py-10 g-pr-40">
                    <a class="g-color-text g-color-black--hover g-color-black--focus g-font-size-16 g-text-underline--none--hover" href="#!"
                       aria-haspopup="true"
                       aria-expanded="false"
                       aria-controls="searchform-1"
                       data-dropdown-event="hover"
                       data-dropdown-target="#searchform-1"
                       data-dropdown-type="css-animation"
                       data-dropdown-duration="300"
                       data-dropdown-animation-in="fadeInUp"
                       data-dropdown-animation-out="fadeOutDown">
                      <i class="align-middle mr-2 icon-education-045 u-line-icon-pro"></i>
                      <span class="g-font-size-default g-pos-rel g-top-minus-2">Search</span>
                    </a>
                  </div>

                  <!-- Search Form -->
                  <form id="searchform-1" class="u-searchform-v1 u-dropdown--css-animation u-dropdown--hidden u-shadow-v20 g-max-width-400 g-brd-around g-brd-gray-light-v4 g-bg-white rounded g-left-0 g-pa-10">
                    <div class="input-group">
                      <input class="form-control rounded-0 u-form-control border-0 g-font-size-13 g-py-12" type="search" placeholder="Enter Your Search Here...">
                      <div class="input-group-addon g-brd-none p-0">
                        <button class="btn u-btn-primary g-font-size-12 rounded text-uppercase g-py-12 g-px-15" type="submit">Go</button>
                      </div>
                    </div>
                  </form>
                  <!-- End Search Form -->
                </div>
                <!-- End Search -->
              </div>

              <div class="col-6 col-sm-auto order-sm-3 g-py-5 g-pr-0 g-py-20--sm">
                <!-- Login -->
                <!-- End Login -->
                <ul class="d-inline-block">
                  <li class="list-inline-item">
                    <a id="account-dropdown-invoker-2" class="u-icon-v1 g-color-text g-color-black--hover g-color-black--focus g-font-size-16 g-text-underline--none--hover" href="#!"
                       aria-controls="account-dropdown-2"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-dropdown-event="hover"
                       data-dropdown-target="#account-dropdown-2"
                       data-dropdown-type="css-animation"
                       data-dropdown-duration="300"
                       data-dropdown-hide-on-scroll="false"
                       data-dropdown-animation-in="fadeIn"
                       data-dropdown-animation-out="fadeOut">
                       <i class="icon-finance-067 u-line-icon-pro"></i>
                    </a>
                    <ul id="account-dropdown-2" class="list-unstyled u-shadow-v29 g-pos-abs g-bg-white g-width-160 g-pb-5 g-mt-19 g-z-index-2"
                        aria-labelledby="account-dropdown-invoker-2">

                      @if (Auth::check())
                        <li>
                          <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ route('user.dashboard') }}">
                            Dahsboard
                          </a>
                        </li>
                        <li>
                          <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();"
                             class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20">
                            Logout
                          </a>
                        </li>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                        </form>

                      @else
                        <li>
                          <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ url('login') }}">
                            Login
                          </a>
                        </li>
                        <li>
                          <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ url('register') }}">
                            Signup
                          </a>
                        </li>
                      @endif
                    </ul>
                  </li>
                </ul>

                <!-- Wishlist -->
                <div class="d-inline-block">
                  <a class="u-icon-v1 g-color-text g-color-black--hover g-color-black--focus g-font-size-16 g-text-underline--none--hover" href="page-wishlist-1.html">
                    <i class="icon-medical-022 u-line-icon-pro"></i>
                  </a>
                </div>
                <!-- End Wishlist -->

                <!-- Basket -->
                <div class="u-basket d-inline-block">
                  <a id="basket-bar-invoker" class="u-icon-v1 g-color-text g-color-black--hover g-color-black--focus g-font-size-16 g-text-underline--none--hover g-ml-10" href="#!"
                     aria-controls="basket-bar"
                     aria-haspopup="true"
                     aria-expanded="false"
                     data-dropdown-event="hover"
                     data-dropdown-target="#basket-bar"
                     data-dropdown-type="css-animation"
                     data-dropdown-duration="300"
                     data-dropdown-hide-on-scroll="false"
                     data-dropdown-animation-in="fadeIn"
                     data-dropdown-animation-out="fadeOut">
                    <i class="d-inline-block icon-hotel-restaurant-105 u-line-icon-pro"></i>
                    <span class="align-bottom g-font-size-11">{{ LaraCart::count() > 0 ? '('.LaraCart::count().')' : '' }}</span>
                  </a>

                  <div id="basket-bar" class="u-basket__bar u-dropdown--css-animation u-dropdown--hidden g-text-transform-none g-bg-white g-brd-around g-brd-gray-light-v4 g-mt-5 g-mt-25--sm"
                       aria-labelledby="basket-bar-invoker">
                    <div class="g-brd-bottom g-brd-gray-light-v4 g-pa-15 g-mb-10">
                      <span class="d-block h6 text-center text-uppercase mb-0">Shopping Cart</span>
                    </div>
                    <div class="js-scrollbar g-height-200">

                    @if (LaraCart::count() > 0)

                      @foreach (LaraCart::getItems() as $item )
                        <!-- Product -->
                        <div class="u-basket__product g-brd-none g-px-20">
                          <div class="row no-gutters g-pb-5">
                            <div class="col-4 pr-3">
                              <a class="u-basket__product-img" href="#!">
                                <img class="img-fluid" src="{{ url('uploads/images/150x150/'.$item->picture) }}" alt="{{ $item->name }}">
                              </a>
                            </div>

                            <div class="col-8">
                              <h6 class="g-font-weight-400 g-font-size-default">
                                <a class="g-color-black g-color-primary--hover g-text-underline--none--hover" href="#!">{{ $item->name }}</a>
                              </h6>
                              <small class="g-color-primary g-font-size-12">{{ $item->qty }} x {{ Helper::currency($item->price) }}</small>
                            </div>
                          </div>
                          <form class="form-inline" method="post" action="{{ route('cart.destroy', $item->getHash()) }}">
                            {{ csrf_field() }}
                            {{ method_field('delete') }}
                            <button class="u-basket__product-remove" type="submit" data-toggle="tooltip" data-original-title="Remove">&times;</button>
                          </form>
                        </div>
                        <!-- End Product -->

                      @endforeach

                       </div>

                    <div class="clearfix g-px-15">
                      <div class="row align-items-center text-center g-brd-y g-brd-gray-light-v4 g-font-size-default">
                        <div class="col g-brd-right g-brd-gray-light-v4">
                          <strong class="d-block g-py-10 text-uppercase g-color-main g-font-weight-500 g-py-10">Total</strong>
                        </div>
                        <div class="col">
                          <strong class="d-block g-py-10 g-color-main g-font-weight-500 g-py-10">{{ Helper::currency(LaraCart::total(false)) }}</strong>
                        </div>
                      </div>
                    </div>

                    <div class="g-pa-20">
                      <div class="text-center g-mb-15">
                        <a class="text-uppercase g-color-primary g-color-main--hover g-font-weight-400 g-font-size-13 g-text-underline--none--hover" href="{{ route('cart.index') }}">
                          View Cart
                          <i class="ml-2 icon-finance-100 u-line-icon-pro"></i>
                        </a>
                      </div>
                      <a class="btn btn-block u-btn-black g-brd-primary--hover g-bg-primary--hover g-font-size-12 text-uppercase rounded g-py-10" href="{{ route('checkout.index') }}">Proceed to Checkout</a>
                    </div>

                    

                    @else
                      <div class="col-md-12 text-center">
                        <p class="text-muted">Your cart is currenlty empty</p>
                      </div>

                      </div>
                        
                    @endif  


                  </div>
                </div>
                <!-- End Basket -->

              </div>
            </div>
          </div>
        </div>
        <!-- End Top Bar -->

        <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3">
          <nav class="js-mega-menu navbar navbar-expand-lg">
            <div class="container">
              <!-- Responsive Toggle Button -->
              <button type="button" class="navbar-toggler navbar-toggler-right btn ml-auto g-line-height-1 g-brd-none g-pa-0"
                      aria-label="Toggle navigation"
                      aria-expanded="false"
                      aria-controls="navBar"
                      data-toggle="collapse"
                      data-target="#navBar">
                <span class="hamburger hamburger--slider">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </span>
              </button>
              <!-- End Responsive Toggle Button -->

              <!-- Navigation -->
              <div id="navBar" class="collapse navbar-collapse align-items-center flex-sm-row">
                @include('frontend.layouts.navigation')
              </div>
              <!-- End Navigation -->
            </div>
          </nav>
        </div>