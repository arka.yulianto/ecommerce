        <div id="rev_slider_1086_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="woobig1" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.4.1 auto mode -->
          <div id="rev_slider_1086_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
            <ul>    <!-- SLIDE  -->

            @foreach (App\Category::getCategory(3) as $category)
                @php($product = App\Product::RandomProduct($category->id))
                @php($url = url('shop/'.$product->slug))

                  <li data-index="rs-{{ $category->id }}" data-transition="slideremovehorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{{ url('uploads/images/1920x1080/'.$category->media->name) }}" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="assets/frontend/img/bg/secondary.png" alt="Image description" data-lazyload="{{ url('uploads/images/1920x1080/'.$category->media->name) }}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg">
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme"
                         id="slide-{{ $category->id }}-layer-1"
                         data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"
                         data-y="['top','top','top','top']" data-voffset="['30','30','30','30']"
                         data-width="['430','430','430','420']"
                         data-height="540"
                         data-whitespace="nowrap"

                         data-type="shape"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power3.easeOut"},{"delay":"wait","speed":1200,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 5;text-transform:left;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Woo-TitleLarge   tp-resizeme"
                         id="slide-{{ $category->id }}-layer-2"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['60','60','60','60']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":600,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 6; min-width: 370px; max-width: 370px; white-space: normal;text-transform:left; font-family: 'Roboto', sans-serif;">{{ $product->name }}
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Woo-Rating   tp-resizeme"
                         id="slide-{{ $category->id }}-layer-6"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['160','160','160','160']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":700,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 7; min-width: 370px; max-width: 370px; white-space: normal; line-height: 22px;text-transform:left;">
                      <!-- <i class="fa fa-star" style="color:#ffc321;"></i>
                      <i class="fa fa-star" style="color:#ffc321;"></i>
                      <i class="fa fa-star" style="color:#ffc321;"></i>
                      <i class="fa fa-star" style="color:#ffc321;"></i>
                      <i class="fa fa-star-o" style="color:#ccc;"></i>
                      (3 customer reviews) -->

                      <span class="rate">
                          {!! Helper::getRate($product->comments()->avg('rate')) !!}
                      </span>
                      <br>
                      ({{ $product->comments()->count() }} customer reviews)
                    </div>

                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption Woo-Rating   tp-resizeme"
                         id="slide-{{ $category->id }}-layer-9"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['225','225','225','225']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":800,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 8; min-width: 370px; max-width: 370px; white-space: normal; line-height: 22px;text-transform:left;">{{ substr(strip_tags($product->description), 0, 130) }}
                    </div>

                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption Woo-SubTitle   tp-resizeme"
                         id="slide-{{ $category->id }}-layer-10"
                         data-x="['left','left','left','left']" data-hoffset="['60','59','59','55']"
                         data-y="['top','top','top','top']" data-voffset="['350','350','350','350']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":900,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 9; min-width: 370px; max-width: 370px; white-space: normal;text-transform:left;">STARTING FROM
                    </div>

                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption Woo-PriceLarge   tp-resizeme"
                         id="slide-{{ $category->id }}-layer-11"
                         data-x="['left','left','left','left']" data-hoffset="['60','60','60','55']"
                         data-y="['top','top','top','top']" data-voffset="['380','380','380','380']"
                         data-width="370"
                         data-height="none"
                         data-whitespace="normal"

                         data-type="text"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"

                         style="z-index: 10; min-width: 370px; max-width: 370px; white-space: normal; font-size: 40px; line-height: 40px;text-transform:left;">
                           @if (!empty($product->discount))
                            <sup class="text-muted" style="font-size: .5em"><s>{{ Helper::currency($product->price) }}</s></sup>
                            {{ Helper::currency($product->discount) }}
                          @else
                            {{ Helper::currency($product->price) }}
                          @endif
                    </div>

                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption Woo-ProductInfo rev-btn  tp-resizeme g-brd-gray-light-v4 g-bg-secondary"
                         id="slide-{{ $category->id }}-layer-14"
                         data-x="['left','left','left','left']" data-hoffset="['139','139','139','132']"
                         data-y="['top','top','top','top']" data-voffset="['449','449','449','450']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="button"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1100,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(221, 221, 221, 1.00);bw:1px 1px 1px 1px;"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[12,12,12,12]"
                         data-paddingright="[75,75,75,75]"
                         data-paddingbottom="[12,12,12,12]"
                         data-paddingleft="[50,50,50,50]"
                         onclick="window.location.href='{{ $url }}'"
                         style="z-index: 11; white-space: nowrap;text-transform:left;background-color:rgba(238, 238, 238, 1.00);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                      <i class="pe-7s-look" style="font-size:25px; float: left; margin-top: -6px; margin-right: 6px;"></i>
                      Product Info
                    </div>

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption Woo-AddToCart rev-btn  tp-resizeme g-brd-primary g-color-white g-bg-primary"
                         id="slide-{{ $category->id }}-layer-13"
                         data-x="['left','left','left','left']" data-hoffset="['142','142','142','137']"
                         data-y="['top','top','top','top']" data-voffset="['500','500','500','500']"
                         data-width="none"
                         data-height="none"
                         data-whitespace="nowrap"

                         data-type="button"
                         data-responsive_offset="on"

                         data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":1200,"ease":"Power3.easeOut"},{"delay":"wait","speed":1000,"to":"x:left;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(243, 168, 71, 1.00);bw:1px 1px 1px 1px;"}]'
                         data-textAlign="['left','left','left','left']"
                         data-paddingtop="[12,12,12,12]"
                         data-paddingright="[75,75,75,75]"
                         data-paddingbottom="[12,12,12,12]"
                         data-paddingleft="[50,50,50,50]"

                         style="z-index: 12; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;"
                        onclick="event.preventDefault();
                        document.getElementById('form-cart-{{ $product->id }}').submit();"
                         >
                      <i class="pe-7s-cart" style="font-size:25px; float: left; margin-top: -6px; margin-right: 6px;"></i>
                      Add to Cart
                    </div>
                    <form action="{{ route('cart.store') }}" method="post" id="form-cart-{{ $product->id }}" style="display: none">
                        <input type="text" name="id" value="{{ $product->id }}" hidden="hidden">
                        {{ csrf_field() }}
                    </form>
                  </li>
              @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
          </div>
        </div>