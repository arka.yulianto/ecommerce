<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title> Ecommerce | @yield('title') </title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url('assets/frontend/favicon.ico') }}">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C500%2C700%7CPlayfair+Display%7CRaleway%7CSpectral%7CRubik">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/bootstrap/bootstrap.min.css') }}">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/icon-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/icon-line-pro/style.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/slick-carousel/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/icon-hs/style.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/animate.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/hamburgers/hamburgers.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/hs-megamenu/src/hs.megamenu.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css') }}">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/revolution-slider/revolution/css/settings.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/revolution-slider/revolution/css/layers.css') }}">
    <link rel="stylesheet" href="{{ url('assets/frontend/vendor/revolution-slider/revolution/css/navigation.css') }}">

    <!-- CSS Unify Theme -->
    <link rel="stylesheet" href="{{ url('assets/frontend/css/styles.e-commerce.css') }}">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="{{ url('assets/frontend/css/custom.css') }}">
  </head>

  <body>
    <main>
      <!-- Header -->
      <header id="js-header" class="u-header u-header--static u-shadow-v19">
        @include('frontend.layouts.header')
      </header>
      <!-- End Header -->

      <!-- Revolution Slider -->
      <div class="g-overflow-hidden">
        @include('frontend.layouts.slider')
      </div>
      <!-- End Revolution Slider -->

      <!-- Features -->
      	<div class="g-brd-bottom g-brd-gray-light-v4">
     		
 		</div>
      <!-- End Features -->

      <!-- Categories -->
      <div class="container g-pt-100 g-pb-70">
        @include('frontend.layouts.categories')
      </div>
      <!-- End Categories -->

      <!-- Products -->
      <div class="container g-pb-100">
        @yield('content')
      </div>
      <!-- End Products -->

      <!-- Promo Block -->
      <section class="g-min-height-100vhg-flex-centered g-bg-secondary g-pos-rel">
        @include('frontend.layouts.promo')
      </section>
      <!-- End Promo Block -->

      <!-- Product Blocks -->
      <section class="container g-py-100">
        @yield('content_bottom')
      </section>
      <!-- End Product Blocks -->

      <!-- Categories -->
      <div class="container-fluid px-0">
        @include('frontend.layouts.bottom_section')
      </div>
      <!-- End Categories -->

      <!-- Subscribe -->
      <div class="g-pt-100 g-pb-70">
	      <div class="text-center g-max-width-600 mx-auto g-pb-30">
	        @include('frontend.layouts.subscribe')
	      </div>
	   </div>
      <!-- End Subscribe -->

      <!-- Social Icons -->
      <div class="container g-mb-30">
        @include('frontend.layouts.social')
      </div>
      <!-- End Social Icons -->

      <!-- Footer -->
      <footer>
        <!-- Content -->
        @include('frontend.layouts.footer')
        <!-- End Copyright -->
      </footer>
      <!-- End Footer -->

      <a class="js-go-to u-go-to-v2" href="#!"
         data-type="fixed"
         data-position='{
           "bottom": 15,
           "right": 15
         }'
         data-offset-top="400"
         data-compensation="#js-header"
         data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
      </a>
    </main>

    <div class="u-outer-spaces-helper"></div>

    <!-- JS Global Compulsory -->
    <script src="{{ url('assets/frontend/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/jquery-migrate/jquery-migrate.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/popper.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/bootstrap/bootstrap.min.js') }}"></script>

    <!-- JS Implementing Plugins -->
    <script src="{{ url('assets/frontend/vendor/jquery.countdown.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/slick-carousel/slick/slick.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/hs-megamenu/src/hs.megamenu.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    <!-- JS Master Slider -->
    <script src="{{ url('assets/frontend/vendor/master-slider/source/assets/js/masterslider.min.js') }}"></script>

    <!-- JS Unify -->
    <script src="{{ url('assets/frontend/js/hs.core.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.header.js') }}"></script>
    <script src="{{ url('assets/frontend/js/helpers/hs.hamburgers.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.dropdown.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.scrollbar.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.countdown.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.carousel.js') }}"></script>
    <script src="{{ url('assets/frontend/js/components/hs.go-to.js') }}"></script>

    <!-- JS Revolution Slider -->
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ url('assets/frontend/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>

    <!-- JS Customization -->
    <script src="{{ url('assets/frontend/js/custom.js') }}"></script>

    <!-- JS Plugins Init. -->
    @stack('js')
  </body>
</html>
