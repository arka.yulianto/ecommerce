<ul class="list-inline text-center mb-0">
  <li class="list-inline-item g-mx-2">
    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-facebook--hover" href="#!">
      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-facebook"></i>
      <i class="g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-facebook"></i>
    </a>
  </li>
  <li class="list-inline-item g-mx-2">
    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-twitter--hover" href="#!">
      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-twitter"></i>
      <i class="g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-twitter"></i>
    </a>
  </li>
  <li class="list-inline-item g-mx-2">
    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-instagram--hover" href="#!">
      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-instagram"></i>
      <i class="g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-instagram"></i>
    </a>
  </li>
  <li class="list-inline-item g-mx-2">
    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-google-plus--hover" href="#!">
      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-google-plus"></i>
      <i class="g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-google-plus"></i>
    </a>
  </li>
  <li class="list-inline-item g-mx-2">
    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-linkedin--hover" href="#!">
      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-linkedin"></i>
      <i class="g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-linkedin"></i>
    </a>
  </li>
</ul>
  
  <center>
    <div class="d-inline-block col-sm-auto">
      <hr>
      <ul class="list-inline g-pt-1 mb-0" style="text-align: left !important">
        <!-- Currency -->
        <li class="list-inline-item">
          <a id="currency-dropdown-invoker-2" class="g-color-black-opacity-0_6 g-color-primary--hover g-font-weight-400 g-text-underline--none--hover" href="#!" aria-controls="currency-dropdown-2" aria-haspopup="true" aria-expanded="false" data-dropdown-event="hover" data-dropdown-target="#currency-dropdown-2" data-dropdown-type="css-animation" data-dropdown-duration="300" data-dropdown-hide-on-scroll="false" data-dropdown-animation-in="fadeIn" data-dropdown-animation-out="fadeOut">
            

            @if (session()->has('currency'))
              @php ($sel_currency = session()->get('currency'))
            @else
              @php ($sel_currency = 'usd')
            @endif

            @php($currency = App\Currency::getCurrency($sel_currency))

            {{ $currency->symbol }} {{ $currency->name }}

          </a>
          <ul id="currency-dropdown-2" class="list-unstyled u-shadow-v29 g-pos-abs g-left-0 g-bg-white g-width-160 g-pb-5 g-mt-19 g-z-index-2 u-dropdown--css-animation u-dropdown--hidden" aria-labelledby="currency-dropdown-invoker-2" style="animation-duration: 300ms; left: 0px;">
            @foreach (App\Currency::get() as $currency)
            <li>
              <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="{{ url('currency/'.$currency->alias) }}">
                {{ $currency->symbol }}  {{ $currency->name }}
              </a>
            </li>
            @endforeach
          </ul>
        </li>
        <!-- End Currency -->

        <li class="list-inline-item g-color-black-opacity-0_3 g-mx-4">|</li>

        <!-- Language -->
        <li class="list-inline-item">
          <a id="languages-dropdown-invoker-2" class="g-color-black-opacity-0_6 g-color-primary--hover g-font-weight-400 g-text-underline--none--hover" href="#!" aria-controls="languages-dropdown-2" aria-haspopup="true" aria-expanded="false" data-dropdown-event="hover" data-dropdown-target="#languages-dropdown-2" data-dropdown-type="css-animation" data-dropdown-duration="300" data-dropdown-hide-on-scroll="false" data-dropdown-animation-in="fadeIn" data-dropdown-animation-out="fadeOut">
            <img src="{{ url('assets/frontend/img/icons/flags/United Kingdom(Great Britain).png') }}" style="width: 18px; margin-right: 10px;">
            English
          </a>
          <ul id="languages-dropdown-2" class="list-unstyled u-shadow-v29 g-pos-abs g-bg-white g-width-160 g-pb-5 g-mt-19 g-z-index-2 u-dropdown--css-animation u-dropdown--hidden" aria-labelledby="languages-dropdown-invoker-2" style="animation-duration: 300ms; right: -72px;">
            <li>
              <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="#!">
                <img src="{{ url('assets/frontend/img/icons/flags/United Kingdom(Great Britain).png') }}" style="width: 18px; margin-right: 10px;">
                English
              </a>
            </li>
            <li>
              <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="#!">
                <img src="{{ url('assets/frontend/img/icons/flags/Indonesia.png') }}" style="width: 18px; margin-right: 10px;">
                Indonesian
              </a>
            </li>
          </ul>
        </li>
        <!-- End Language -->
      </ul>
    </div>
  </center>