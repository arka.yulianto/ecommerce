@foreach (App\Product::getProduct(1) as $product)
<div class="container g-pt-100 g-pb-70">
  <div class="row justify-content-between align-items-center">
    <div class="col-md-8 col-lg-6 order-md-2 g-mb-30">
      <div class="g-pos-rel">
        <img class="img-fluid w-100" src="{{ url('uploads/images/700x700/'.$product->media->name) }}" alt="Image Description">
        <span class="u-icon-v1 g-width-85 g-height-85 g-brd-3 g-brd-white g-color-white g-bg-primary g-font-weight-300 g-font-size-22 rounded-circle g-pos-abs g-top-100 g-left-0 g-brd-around">
          <i class="g-font-style-normal">
            @if (!empty($product->discount))
              {{ Helper::currency($product->discount) }}
            @else
              {{ Helper::currency($product->price) }}
            @endif
          </i>
        </span>
      </div>
    </div>

    <div class="col-md-4 order-md-1 g-mb-30">
      <div class="g-mb-30">
        <h1 class="g-color-primary g-font-weight-400 g-font-size-40 mb-0">{{ $product->name }}</h1>
        @foreach ($product->categories as $category)
         <h2 class="g-color-dark g-font-weight-300 g-font-size-75 g-line-height-1 mb-4">{{ $category->name }}</h2>
        @endforeach
        
        <p class="lead">{!! $product->description !!}</p>
      </div>

      <a class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 g-mb-70" href="#!">Shop Now</a>

    </div>
  </div>
</div>
@endforeach