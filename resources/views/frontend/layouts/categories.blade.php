<div class="row g-mx-minus-10">

  @foreach (App\Category::getCategory(3) as $category)
  <div class="col-sm-6 col-md-4 g-px-10 g-mb-30">
    <article class="u-block-hover">
      <img class="w-100 u-block-hover__main--zoom-v1 g-mb-minus-8" src="{{ url('uploads/images/650x750/'.$category->media->name) }}" alt="{{ $category->name }}">
      <div class="g-pos-abs g-bottom-30 g-left-30">
        <span class="d-block g-color-black">{{ $category->parent->name }}</span>
        <h2 class="h1 mb-0">{{ $category->name }}</h2>
      </div>
      <a class="u-link-v2" href="#"></a>
    </article>
  </div>
  @endforeach

</div>