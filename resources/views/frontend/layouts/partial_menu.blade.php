<li class="nav-item {{ count($menu->child) > 0 ? 'hs-has-sub-menu' : '' }} g-mx-10--lg g-mx-15--xl">
  <a id="nav-link--pages" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20" href="{{ $menu->url }}" aria-haspopup="true" aria-expanded="false" aria-controls="nav-submenu--pages">
    {{ $menu->name }}
  </a>

  @if (count($menu->child) > 0)
  <ul class="hs-sub-menu list-unstyled u-shadow-v11 g-min-width-220 g-brd-top g-brd-primary g-brd-top-2 g-mt-7 animated" id="nav-submenu--pages" aria-labelledby="nav-link--pages" style="display: none;">
    @foreach($menu->child as $menu)
      @include('frontend.layouts.partial_menu', $menu)
    @endforeach
  </ul>
  @endif
</li>