
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title>@yield('title') | Ecommerce</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- Favicon -->
   <link rel="shortcut icon" href="{{ url('assets/frontend/favicon.ico') }}">
  <!-- Google Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="{{ url('assets/frontend/vendor/bootstrap/bootstrap.min.css') }}">
  <!-- CSS Unify -->
  <link rel="stylesheet" href="{{ url('assets/frontend/css/unify-core.css') }}">
  <link rel="stylesheet" href="{{ url('assets/frontend/css/unify-components.css') }}">
  <link rel="stylesheet" href="{{ url('assets/frontend/css/unify-globals.css') }}">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="{{ url('assets/frontend/css/custom.css') }}">
  
</head>

<body>
  <main class="g-min-height-100vh g-flex-centered g-pa-15">
   @yield('content')
  </main>

</body>

</html>
