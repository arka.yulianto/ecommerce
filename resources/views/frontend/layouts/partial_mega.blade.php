<li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-position="right">
  <a id="mega-menu-label-{{ $menu->id }}" class="nav-link text-uppercase g-color-primary--hover g-px-5 g-py-20" href="#!" aria-haspopup="true" aria-expanded="false">
    {{ $menu->name }}
    <i class="hs-icon hs-icon-arrow-bottom g-font-size-11 g-ml-7"></i>
  </a>

  <div class="w-100 hs-mega-menu u-shadow-v11 g-text-transform-none g-brd-top g-brd-primary g-brd-top-2 g-bg-white g-pa-30 g-mt-7" aria-labelledby="mega-menu-label-{{ $menu->id }}">
    <div class="row">
      @foreach (App\Menu::getMenu($menu->id) as $menu)

        @if (count($menu->widget) > 0)
          
          @include('frontend.layouts.partial_widget_menu', $menu)

        @else

          <div class="col-sm-6 col-lg-2 g-mb-30 g-mb-0--md">
            <!-- Links -->
            <div class="mb-5">
              <span class="d-block g-font-weight-500 text-uppercase mb-2">{{ $menu->name }}</span>

              @if (count($menu->child) > 0)
                <ul class="list-unstyled">
                  @foreach ($menu->child as $menu)
                  <li><a class="d-block g-color-text g-color-primary--hover g-text-underline--none--hover g-py-5" href="#!">{{ $menu->name }}</a></li>
                  @endforeach
                </ul>
              @endif
            </div>

          </div>

        @endif

      @endforeach

    </div>
  </div>
</li>