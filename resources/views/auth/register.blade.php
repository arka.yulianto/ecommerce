
@extends('frontend.layouts.master_left_sidebar')

@section('title')
    Register
@endsection

@section('content')

<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Home</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Pages</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>Signup</span>
        </li>
      </ul>
    </div>
</section>
<section class="container g-pt-100 g-pb-20">
  <div class="row">
    <div class="col-lg-5 order-lg-2 g-mb-80">
      <div class="g-brd-around g-brd-gray-light-v3 g-bg-white rounded g-px-30 g-py-50 mb-4">
        <header class="text-center mb-4">
          <h1 class="h4 g-color-black g-font-weight-400">Create New Account</h1>
        </header>

        <!-- Form -->
        <form class="g-py-15 js-validate" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
          <div class="row">
            <div class="col g-mb-20">
                <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" type="text" placeholder="First name" name="first_name" value="{{ old('first_name') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                    @if ($errors->has('first_name'))
                    <label class="error">{{ $errors->first('first_name') }}</label>
                    @endif
                </div>
            </div>

            <div class="col g-mb-20">
                <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" type="text" placeholder="Last name" name="last_name" value="{{ old('last_name') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                    @if ($errors->has('last_name'))
                    <label class="error">{{ $errors->first('last_name') }}</label>
                    @endif
                </div>
            </div>
          </div>

          <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }} g-mb-20">
            <select class="js-custom-select u-select-v1 g-brd-gray-light-v3 g-color-gray-dark-v5 rounded g-py-12" style="width: 100%;"
                          data-placeholder="Gender"
                          data-open-icon="fa fa-angle-down"
                          data-close-icon="fa fa-angle-up" name="sex" value="{{ old('sex') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                <option></option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Other">Other</option>
            </select>
            @if($errors->has('sex'))
               <label class="error"> {{ $errors->first('sex') }}</label>
            @endif
          </div>

          <div class="row">
            <div class="col-sm-6 col-md-12 col-lg-6 g-mb-20">
                <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                  <select class="js-custom-select u-select-v1 g-brd-gray-light-v3 g-color-gray-dark-v5 rounded g-py-12" style="width: 100%;"
                                data-placeholder="Month"
                                data-open-icon="fa fa-angle-down"
                                data-close-icon="fa fa-angle-up" name="month" value="{{ old('month') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                      <option></option>
                      <option value="01">January</option>
                      <option value="02">February</option>
                      <option value="03">March</option>
                      <option value="04">April</option>
                      <option value="05">May</option>
                      <option value="06">June</option>
                      <option value="07">July</option>
                      <option value="08">August</option>
                      <option value="09">September</option>
                      <option value="10">October</option>
                      <option value="11">November</option>
                      <option value="12">December</option>
                    </select>
                    @if ($errors->has('month'))
                    <label class="error">{{ $errors->first('month') }}</label>
                    @endif
                </div>
            </div>

            <div class="col g-mb-20">
                <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" type="text" placeholder="Day" name="day" value="{{ old('day') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                    @if ($errors->has('day'))
                    <label class="error">{{ $errors->first('day') }}</label>
                    @endif
                </div>
            </div>

            <div class="col g-mb-20">
                <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" type="text" placeholder="Year" name="year" value="{{ old('year') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                    @if ($errors->has('year'))
                    <label class="error">{{ $errors->first('year') }}</label>
                    @endif
                </div>
            </div>
          </div>

          <div class="g-mb-20">
            <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" type="text" placeholder="Username" name="name" value="{{ old('name') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                @if ($errors->has('name'))
                <label class="error">{{ $errors->first('name') }}</label>
                @endif
            </div>
          </div>

          <div class="g-mb-20">
            <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" type="email" placeholder="Email address" name="email" value="{{ old('email') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                @if ($errors->has('email'))
                <label class="error">{{ $errors->first('email') }}</label>
                @endif
            </div>
          </div>

          <div class="g-mb-20">
            <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" type="password" placeholder="Password" name="password" value="{{ old('password') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                @if ($errors->has('password'))
                <label class="error">{{ $errors->first('password') }}</label>
                @endif
            </div>
          </div>

          <div class="g-mb-20">
            <div class="form-group{{ $errors->has('name') ? ' u-has-error-v1' : '' }}">
                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 rounded g-py-15 g-px-15" type="password" placeholder="Confirm password" name="password_confirmation" value="{{ old('password_confirmation') }}" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true">
                @if ($errors->has('password_confirmation'))
                <label class="error">{{ $errors->first('password_confirmation') }}</label>
                @endif
            </div>
          </div>

          <div class="mb-1">
            <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-13 g-pl-25 mb-2">
              <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="accept" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true" value="1">
              <span class="d-block u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                <i class="fa" data-check-icon=""></i>
              </span>
              I accept the <a href="#!">Terms and Conditions</a>
            </label>
          </div>

          <div class="mb-3">
            <label class="form-check-inline u-check g-color-gray-dark-v5 g-font-size-13 g-pl-25 mb-2">
              <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox" name="newsletter" required="required" " data-msg="This field is required" data-error-class="u-has-error-v1" data-success-class="u-has-success-v1" aria-required="true" value="1">
              <span class="d-block u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                <i class="fa" data-check-icon=""></i>
              </span>
              Subscribe to our newsletter
            </label>
          </div>

          <button class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25 disabled" type="submit" disabled="disabled" id="btn-submit">Signup</button>
        </form>
        <!-- End Form -->
      </div>

      <div class="text-center">
        <p class="g-color-gray-dark-v5 mb-0">Already have an account?
          <a class="g-font-weight-600" href="page-login-1.html">signin</a></p>
      </div>
    </div>

    <div class="col-lg-7 order-lg-1 g-mb-80">
      <div class="g-pr-20--lg">
        <div class="mb-5">
          <h2 class="h1 g-font-weight-400 mb-3">Welcome to Yulianto's Merchant</h2>
          <p class="g-color-gray-dark-v4">The time has come to bring those ideas and plans to life. This is where we really begin to visualize your napkin sketches and make them into beautiful pixels.</p>
        </div>

        <div class="row text-center mb-5">
          <div class="col-sm-4 g-mb-10">
            <!-- Counters -->
            <div class="g-bg-gray-light-v5 g-pa-20">
              <div class="js-counter g-color-gray-dark-v5 g-font-weight-300 g-font-size-25 g-line-height-1">52147</div>
              <div class="d-inline-block g-width-10 g-height-2 g-bg-gray-dark-v5 mb-1"></div>
              <h4 class="g-color-gray-dark-v4 g-font-size-12 text-uppercase">Product Sold</h4>
            </div>
            <!-- End Counters -->
          </div>

          <div class="col-sm-4 g-mb-10">
            <!-- Counters -->
            <div class="g-bg-gray-light-v5 g-pa-20">
              <div class="js-counter g-color-gray-dark-v5 g-font-weight-300 g-font-size-25 g-line-height-1">24583</div>
              <div class="d-inline-block g-width-10 g-height-2 g-bg-gray-dark-v5 mb-1"></div>
              <h4 class="g-color-gray-dark-v4 g-font-size-12 text-uppercase">New Customer</h4>
            </div>
            <!-- End Counters -->
          </div>

          <div class="col-sm-4 g-mb-10">
            <!-- Counters -->
            <div class="g-bg-gray-light-v5 g-pa-20">
              <div class="js-counter g-color-gray-dark-v5 g-font-weight-300 g-font-size-25 g-line-height-1">7348</div>
              <div class="d-inline-block g-width-10 g-height-2 g-bg-gray-dark-v5 mb-1"></div>
              <h4 class="g-color-gray-dark-v4 g-font-size-12 text-uppercase">New Transaction</h4>
            </div>
            <!-- End Counters -->
          </div>
        </div>

        <div class="text-center">
          <h2 class="h4 g-font-weight-400 mb-4">Join more than
            <span class="g-color-primary">33,000</span> customers worldwide</h2>
          <img class="img-fluid g-opacity-0_6" src="{{ url('assets/frontend/img/maps/map.png') }}" alt="Image Description">
        </div>
      </div>
    </div>
  </div>
</section>

@endsection

@push('js')

<script src="{{ url('assets/frontend/vendor/appear.js') }}"></script>
<script src="{{ url('assets/frontend/vendor/chosen/chosen.jquery.js') }}"></script>
<script src="{{ url('assets/frontend/vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>

<script src="{{ url('assets/frontend/js/components/hs.select.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.counter.js') }}"></script>
<script src="{{ url('assets/frontend/js/components/hs.validation.js') }}"></script>
<script src="{{ url('assets/frontend/js/pages/register.js') }}"></script>

@endpush