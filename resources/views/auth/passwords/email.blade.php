@extends('frontend.layouts.master_left_sidebar')

@section('title')
Reset Password
@endsection

@section('content')
    
<section class="g-brd-bottom g-brd-gray-light-v4 g-py-30">
    <div class="container">
      <ul class="u-list-inline">
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Home</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-mr-5">
          <a class="u-link-v5 g-color-text" href="#!">Pages</a>
          <i class="g-color-gray-light-v2 g-ml-5 fa fa-angle-right"></i>
        </li>
        <li class="list-inline-item g-color-primary">
          <span>Password Recovery</span>
        </li>
      </ul>
    </div>
</section>

<section class="container g-py-100">
    <div class="row justify-content-center">
      <div class="col-sm-9 col-md-7 col-lg-5">
        <div class="g-brd-around g-brd-gray-light-v3 g-bg-white rounded g-px-30 g-py-50 mb-4">
            @if (session('message'))
               <div class="alert alert-dismissible fade show g-bg-teal g-color-white rounded-0" role="alert">
                  <button type="button" class="close u-alert-close--light" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <div class="media">
                      <span class="d-flex g-mr-10 g-mt-5">
                      <i class="icon-check g-font-size-25"></i>
                    </span>
                    <span class="media-body align-self-center">
                      <strong>Well done!</strong> {{ session('message') }}
                    </span>
                 </div>
              </div>
            @endif

          <header class="text-center mb-4">
            <h1 class="h4 g-color-black g-font-weight-400">Forgot Password?</h1>
            <p>Enter your e-mail address to reset your password.</p>
          </header>

          <!-- Form -->
          <form class="g-py-15" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="mb-4">
              <div class="input-group g-rounded-left-5">
                <span class="input-group-addon g-width-45 g-brd-gray-light-v3 g-color-gray-dark-v5">
                  <i class="icon-finance-067 u-line-icon-pro"></i>
                </span>
                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v3 g-rounded-left-0 g-rounded-right-5 g-py-15 g-px-15" type="email" placeholder="Email Adress" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block text-danger">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>

            <button class="btn btn-block u-btn-primary g-font-size-12 text-uppercase g-py-15 g-px-25" type="submit">Get New Password</button>
          </form>
          <!-- End Form -->
        </div>

        <div class="row justify-content-between mb-5">
          <div class="col align-self-center text-center">
            <p class="g-font-size-13"><a href="{{ route('login') }}">Back to Login</a></p>
          </div>
        </div>
      </div>
    </div>
</section>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
