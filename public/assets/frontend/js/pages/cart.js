$(document).on('ready', function () {
	$.HSCore.components.HSCountQty.init('.js-quantity');
	$('.btn-change').click(function(){
		id = $(this).data('value');
		rowId = $(this).data('row-id');
		$('[name="row_id"]').val(rowId);
		$('[name="id"]').val(id);
		$.ajax({
			url: SITE_URL + '/cart/'+id+'/edit',
			type: 'get',
			dataType: 'html',
			success: function(data) {
				$('#attribute-form').html(data);
				$('#modal-change-attribute').modal('show');
			}
		});

	});
});