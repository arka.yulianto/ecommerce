$(document).ready(function(){
	$('[name="province_id"]').change(function(){

		$('[name="city_id"]').find('option').remove();
		var id = $(this).val();
		
		if (id != 0) {

			city = get_city(id);
			option_city = '<option></option>';
			$.each(city, function(i,v){
				option_city += '<option value="'+v.city_id+'">'+v.type + ' '+v.city_name+'</option>';
			});

			$('[name="city_id"]').append(option_city);
			$('[name="city_id"]').trigger('chosen:updated');
		}
	});	
});

function get_city(id)
{
	var res = $.ajax({
		url: SITE_URL + '/shipping/'+id,
		dataType: 'json',
		type: 'get',
		async: false
	});

	return res.responseJSON;
}