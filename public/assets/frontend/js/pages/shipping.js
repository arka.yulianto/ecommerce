var province;
var city;
$(document).ready(function(){

	province = get_province();
	option_provice = '';

	$.each(province, function(i,v){
		option_provice += '<option value="'+v.province_id+'">'+v.province+'</option>';
	});

	$('[name="province_id"]').append(option_provice).on('change', function(){
		
		var id = $(this).val();
		
		if (id != 0) {

			$('[name="city_id"]').find('option').remove();

			city = get_city(id);
			option_city = '<option value="0">-- Select City -- </option>';
			$.each(city, function(i,v){
				option_city += '<option value="'+v.city_id+'">'+v.type + ' '+v.city_name+'</option>';
			});

			$('[name="city_id"]').append(option_city).on('change', function(){
				if($('[name="courier_id"]').val() != '0'){
					on_calculate();
				}
			});

		}

	});

});

function get_province()
{
	var res = $.ajax({
		url: SITE_URL + '/shipping',
		dataType: 'json',
		type: 'get',
		async: false
	});

	return res.responseJSON;
}

function get_city(id)
{
	var res = $.ajax({
		url: SITE_URL + '/shipping/'+id,
		dataType: 'json',
		type: 'get',
		async: false
	});

	return res.responseJSON;
}

function on_calculate()
{

	if ($('[name="city_id"]').val() != null) {
		$.ajax({

			url: SITE_URL + '/shipping/get_services',
			type: 'post',
			dataType: 'html',
		 	headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
			data: {
				province_id: $('[name="province_id"]').val(),
				city_id: $('[name="city_id"]').val(),
				courier_id: $('[name="courier_id"]').val(),
				total_weight: $('[name="total_weight"]').val(),
			},
			success: function(data) {
				$('#empty-row').hide();
				$('#table-service tbody').html(data);
			}
		});
	}
	
}

function get_cost()
{
	var service_id = $('[name="service_id"]:checked').val();
	var cost = $('[name="service_id"]:checked').data('cost-value');
	$('[name="cost"]').val(cost);

}