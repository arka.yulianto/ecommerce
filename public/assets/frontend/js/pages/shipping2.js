$(document).ready(function(){
	$('[name="province_id"]').change(function(){
		$('[name="city_id"]').find('option').remove();

		var id = $(this).val();
		
		if (id != 0) {
			city = get_city(id);
			option_city = '<option>-- Select City --</option>';
			$.each(city, function(i,v){
				option_city += '<option value="'+v.city_id+'">'+v.type + ' '+v.city_name+'</option>';
			});

			$('[name="city_id"]').append(option_city).on('change', function(){
				if($('[name="courier_id"]').val() != '0'){
					on_calculate();
				}
			});

			$('[name="city_id"]').trigger('chosen:updated');
		}
	});

	 $('[name="courier_id"]').change(function(){
	 	on_calculate();
	 });
});

function get_city(id)
{
	var res = $.ajax({
		url: SITE_URL + '/shipping/'+id,
		dataType: 'json',
		type: 'get',
		async: false
	});

	return res.responseJSON;
}

function on_calculate()
{

	if ($('[name="city_id"]').val() != "" && $('[name="courier_id"]').val() != "") {
		$.ajax({

			url: SITE_URL + '/shipping',
			type: 'post',
			dataType: 'html',
		 	headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    },
			data: {
				province_id: $('[name="province_id"]').val(),
				city_id: $('[name="city_id"]').val(),
				courier_id: $('[name="courier_id"]').val(),
				total_weight: $('[name="total_weight"]').val(),
			},
			success: function(data) {
				$('#empty-row').hide();
				$('#table-service tbody').html(data);
			}
		});
	}
	
}

function get_cost()
{
	var $this = $('[name="service_id"]:checked');
	var service_id = $this.val();
	var cost = $this.data('cost-value');
	var service_name = $this.data('service-name');
	var service_description = $this.data('service-description');
	var estimate_delivery = $this.data('estimate');
	$('[name="cost"]').val(cost);
	$('[name="service_name"]').val(service_name);
	$('[name="service_description"]').val(service_description);
	$('[name="estimate_delivery"]').val(estimate_delivery);

}