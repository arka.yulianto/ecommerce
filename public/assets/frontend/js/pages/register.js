$(document).ready(function(){
	$.HSCore.components.HSSelect.init('.js-custom-select');
    var counters = $.HSCore.components.HSCounter.init('[class*="js-counter"]');
    $.HSCore.components.HSValidation.init('.js-validate');

	$('[name="accept"]').click(function(){
		if ($(this).prop('checked')) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').removeClass('disabled');
		} else {
			$('#btn-submit').attr('disabled', 'disabled');
			$('#btn-submit').addClass('disabled');
		}
	});
});