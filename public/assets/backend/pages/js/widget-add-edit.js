$(document).ready(function(){

	$('[name="type"]').change(function(){
		if ($(this).val() == 'categories') {
			$('#categories').fadeIn('fast');
			$('#text').fadeOut('fast');
			$('#product').fadeOut('fast');
		} else if ($(this).val() == 'text') {
			$('#categories').fadeOut('fast');
			$('#text').fadeIn('fast');
			$('#product').fadeOut('fast');
		} else {
			$('#categories').fadeOut('fast');
			$('#text').fadeOut('fast');
			$('#product').fadeIn('fast');
		}
	});

});