var tProduct;
$(document).ready(function(){
	tProduct = $('#tbl-product').DataTable({
		ajax: SITE_URL + '/admin/product/get_data',
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        columns: [
            { data: 'name', name: 'name'},
            { data: 'price', name: 'price', class: 'text-right'},
            { data: 'sku', name: 'sku' },
            { data: 'stock', name: 'stock', class: 'text-center' },
            { data: 'categories', name: 'categories.name' },
            { data: 'tags', name: 'tags.name' },
            { data: 'options', name: 'options', class: 'text-center' },
        ],

	});

	$('#btn-confirm').click(function(){

		id = $(this).data('value');

		$.ajax({
			url: SITE_URL + '/admin/product/'+id,
			type: 'delete',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			dataType: 'json',
			beforeSend: function(){
				$('#btn-confirm').html('Loading ...');
				$('#btn-confirm').attr('disabled', 'disabled');
			},
			success: function(data){
				show_notification(data.title, data.type, data.message);
			},
			error: function(error, sts, xhr){
  				show_notification('Error', 'error', xhr);
  			},
  			complete: function(){
  				$('#btn-confirm').html('Yes');
				$('#btn-confirm').removeAttr('disabled');
				$('#modal-delete-confirm').modal('hide');
				tProduct.draw();
  			}
		});

	});

});

function on_delete(val)
{
	$('#modal-delete-confirm').modal('show');
	$('#btn-confirm').data('value', val);
}