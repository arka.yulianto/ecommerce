var tOrder;

$(document).ready(function(){
	tOrder = $('#table-order').DataTable({

		ajax: SITE_URL + '/admin/order/get_data',
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        columns: [
            { data: 'order_number', name: 'order_number', class: 'text-center'},
            { data: 'user.user_data.first_name', name: 'user.user_data.first_name' },
            { data: 'created_at', name: 'created_at' },
            { data: 'payment_method', name: 'payment_method' },
            { data: 'status', name: 'status' },
            { data: 'total', name: 'total', class: 'text-right' },
            { data: 'options', name: 'options', class: 'text-center' },
        ],

	});
});