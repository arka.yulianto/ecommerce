var arr_category;
var arr_category_selected = [];

$(document).ready(function(){

	arr_category = get_category();
	arr_parent = get_parent();

	$('[name="category_id[]"]').select2({
		data: arr_category
	}).on('change', function() {
    	var el = $(this);

    	if (!$.inArray('0', el.val())) {

    		arr_category_selected = el.val();
	    	var itemtoRemove = "0";
	    	arr_category_selected.splice($.inArray(itemtoRemove, arr_category_selected), 1);
	    	el.val(arr_category_selected).trigger('change');
		    $('#modal-add-category').modal('show');
		    console.log(arr_category_selected);

    	}
 
  	});

  	$('[name="parent_id"]').select2({
  		data: arr_parent
  	});

  	$('#btn-add-row').click(function(){

  		$('#no-data').remove();

  		var row_length = $('#tbl-attribute tbody tr').length;

  		var table = '<tr id="'+row_length+'">' +
						'<td>' +
							'<input type="text" name="attribute_name[]" class="form-control" placeholder="eg: Color">' +
						'</td>' +
						'<td>' +
							'<select name="attribute_value['+row_length+'][]" class="select2 attribute_value" data-tags="true" multiple="multiple"></select>' +
						'</td>' +
						'<td>' +
							'<button class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Remove Row" type="button" onclick="remove_row('+row_length+')"><i class="fa fa-times"></i></button>' +
						'</td>' +
					'</tr>';

  		$('#tbl-attribute tbody').append(table);

  		$('.attribute_value').select2({
			tags: true,
			placeholder: 'eg: Bule, Yellow, Green',
			tokenSeparators: [",", " "]
		});

		$('[data-toggle="tooltip"]').tooltip();

  	});

  	$('#btn-save-category').click(function(){

  		var form = $('#form-add-category');
  		data = form.serializeArray();

  		if (form.validate().form()) {
  			$.ajax({
	  			url: SITE_URL + '/admin/category',
	  			type: 'post',
	  			dataType: 'json',
	  			data: data,
	  			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	  			beforeSend: function(){
	  				$('#btn-save-category').html('Loading ...');
	  				$('#btn-save-category').attr('disabled', 'disabled');
	  			},
	  			success: function(data){
	  				show_notification(data.title, data.type, data.message);
	  				arr_category = get_category();
	  				$('[name="category_id[]"]').select2({
						data: arr_category
					});
					arr_category_selected.push(data.added);
	  			},
	  			error: function(error, sts, xhr){
	  				show_notification('Error', 'error', xhr);
	  			},
	  			complete: function() {
	  				$('#btn-save-category').html('Save');
	  				$('#btn-save-category').removeAttr('disabled');
	  				form.trigger('reset').validate().resetForm();
	  				$('#modal-add-category').modal('hide');
	  				$('[name="category_id[]"]').val(arr_category_selected).trigger('change');

	  				arr_parent = get_parent();
	  				
					$('[name="parent_id"]').select2({
				  		data: arr_parent
				  	});
	  			}
	  		});
  		}

  	});

  	$('#form-add-edit-product').validate();

});

function get_category()
{
	var res = $.ajax({
		url: SITE_URL + '/admin/category/get_category',
		dataType: 'json',
		type: 'get',
		async: false
	});

	return res.responseJSON;
}

function get_parent()
{
	var res = $.ajax({
		url: SITE_URL + '/admin/category/get_parent',
		dataType: 'json',
		type: 'get',
		async: false
	});

	return res.responseJSON;
}


function remove_row(length)
{

	$('#tbl-attribute tbody').find('tr#'+length).remove();

	if ($('#tbl-attribute tbody tr').length < 1) {
		$('#tbl-attribute tbody').append('<tr id="no-data">' +
												'<td colspan="3" class="text-center">No data</td>' +
											'</tr>');
	}
}