var tReview;
$(document).ready(function(){
	tReview = $('#tbl-review').DataTable({
		ajax: SITE_URL + '/admin/review/get_data',
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        columns: [
            { data: 'user.user_data.fullname', name: 'user.user_data.fullname'},
            { data: 'product.name', name: 'product.name'},
            { data: 'rate', name: 'rate', class: 'text-center'},
            { data: 'content', name: 'content' },
            { data: 'options', name: 'options', class: 'text-center' },
        ],
        drawCallback: function(d){
        	$('[data-toggle="popover"]').popover(); 
        }
	});

	$('#btn-confirm').click(function(){
		id = $(this).data('value');

		$.ajax({
			url: SITE_URL + '/admin/review/'+id,
			type: 'delete',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			dataType: 'json',
			beforeSend: function(){
				$('#btn-confirm').html('Loading ...');
				$('#btn-confirm').attr('disabled', 'disabled');
			},
			success: function(data){
				show_notification(data.title, data.type, data.message);
			},
			error: function(error, sts, xhr){
  				show_notification('Error', 'error', xhr);
  			},
  			complete: function(){
  				$('#btn-confirm').html('Yes');
				$('#btn-confirm').removeAttr('disabled');
				$('#modal-delete-confirm').modal('hide');
				tReview.draw();
  			}
		});
	});

});

function on_delete(val)
{
	$('#modal-delete-confirm').modal('show');
	$('#btn-confirm').data('value', val);
}