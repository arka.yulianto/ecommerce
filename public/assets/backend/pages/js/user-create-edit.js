/*

@author: Fuadz
@created at: 1511539711

*/

var previewNode = document.querySelector('.template');
previewNode.id = '';
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var mediaDropzone = new Dropzone('.small-drag-drop', { // Make the whole body a dropzone
	url: SITE_URL + '/admin/user/upload',
	thumbnailWidth: 400,
	thumbnailHeight: 400,
	parallelUploads: 1,
	maxFiles: 1,
	autoQueue: false,
	previewTemplate: previewTemplate,
	previewsContainer: ".preview",
	clickable: "#browse-file",
	acceptedFiles: '.jpg',
	params: { _token:  $('meta[name="csrf-token"]').attr('content')},
});

mediaDropzone.on('addedfile', function(file) {

	if (file.type != 'image/jpeg'){
		show_notification('Error', 'error', 'File not supported');
		mediaDropzone.removeFile(file);
		console.log(file.type);
	}

	else if (file.size >= 1000000){
		show_notification('Error', 'error', 'File too big');
		mediaDropzone.removeFile(file);
		console.log(file.size);
	}

	else{
		$('.small-drag-drop').hide();
		$('.progress-wrapper').hide();
		//on_upload();
		console.log(file);
	}

});

mediaDropzone.on('maxfilesexceeded', function(file){
    mediaDropzone.removeAllFiles();
    mediaDropzone.addFile(file);
});

mediaDropzone.on('error', function(xhr, status, error) {
    if (error != undefined) {
      show_notification('Error', 'error', error.statusText);
      mediaDropzone.removeAllFiles();
    }
});

mediaDropzone.on('sending', function(file, xhr, formData) {
	$('.progress-wrapper').show();
  	formData.append('email', $('[name="email"]').val());
});


mediaDropzone.on("removedfile", function(file) {
  $('.small-drag-drop').show();
  $('.small-drag-drop').css('border', '2px dashed #e1e1e1');
});

mediaDropzone.on('success', function(file, response) {
  $('.progress-wrapper').hide();
  $('[name="avatar"]').val(response.avatar);
  $('#form-add-edit').submit();
});

function on_upload()
{
	var form = $('#form-add-edit').validate();
	if (form.form()) {
		if (mediaDropzone.files.length > 0) {
			mediaDropzone.enqueueFiles(mediaDropzone.getFilesWithStatus(Dropzone.ADDED));
				
		} else {
			$('#form-add-edit').submit();
		}
	}
}

$(document).ready(function(){

	$('.small-drag-drop').on('dragover', function(){
	    $(this).css('border', '2px dashed #5b90bf');
	});

	$('.small-drag-drop').on('dragleave', function(){
	    $(this).css('border', '2px dashed #e1e1e1');
	});


	$('#form-add-edit').validate();

	$('#btn-save').click(function(){
		var form = $('#form-add-edit').validate();
		if (form.form()) {
			$(this).attr('disabled', 'disabled');
			on_upload();
		}
	})

});