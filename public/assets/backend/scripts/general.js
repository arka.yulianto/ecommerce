// DataTable
$(document).ready(function () {

    $.extend(true, $.fn.dataTable.defaults, {
        processing: true,
        serverSide: true,
        autoWidth: false,
        pagingType: 'full_numbers',
        dom: "<'row'<'col-xs-12 table-menu'tr>>" +
                "<'row m-t-20 '<'col-md-5'l> <'col-md-7 pull-right'p>>" +
                "<'row'<'col-xs-12'i>>",
        pageLength: 5,
        lengthMenu: [
            [5, 10, 25, 50, 100, 200, -1],
            [5, 10, 25, 50, 100, 200, 'All']
          ],
        language: {
            thousands: '.',
            processing: 'Proccessing. Please wait...',
            emptyTable: 'No data available.',
            sZeroRecords: 'Data not found.',
            lengthMenu: '_MENU_',
            infoEmpty: 'Showing 0 until 0 from 0 rows',
            info: 'Showing _START_ - _END_ until _TOTAL_ rows',
            loadingRecords: 'Please wait',
            paginate: {
                first: '<<',
                last: '>>',
                next: '>',
                previous: '<'
            },
            infoFiltered: '',
            pagination: {
                classes: {
                    ul: 'pagination pagination-sm'
                }
            }

        },
        drawCallback: function () {
            $('.dataTables_paginate > .pagination').addClass('pagination-sm');
        }
    });

    // dataToggle
    $('[data-toggle="tooltip"]').tooltip();

    // select2
    $('.select2').select2({
        width: 'element',
        minimumResultsForSearch: Infinity
    });

    // datepicker
    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true
    });

    // $('.datetimepicker').datetimepicker({
    //     format: 'YYYY-MM-DD HH:mm:ss', 
    //     locale: 'id'
    // });

    $('.number').keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
                   return false;
        }
    });

    $('.autonumeric').autoNumeric('init', {
        aSep: ',' ,
        mDec : 2,
        vMin: 0
    });

    tinymce.init({
        selector: '.tinymce',
        height: 300,
        themes: 'modern',
        menubar: false,
        plugins: [
        'advlist autolink lists link image charmap print preview anchor textcolor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table contextmenu paste code help'
        ],
        toolbar: ' formatselect | undo redo  | bold italic  | insertimage | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat ',
        setup: function(editor) {
    
            
            function insertImage() {

            var fired = false;

            $('#modal-media').modal({
                backdrop: 'static',
                keyboard: false
            });

            //When the flag is false declare the click event
            if(!fired) {
                //mark as fired
                fired = true;
                $("#btn-use").one('click', function (e) {
                /*$(this).off(e);*/
                    var img = $('.image-browser').find('.selected').children().attr('src');
                    if (img != undefined) {
                        editor.insertContent('<img src="'+img+'">');
                    }
                    //unflag so it can be fired again
                    fired = false;
                });
            }

              $('.image-browser img').click(function(){
                $('.image-browser').children().removeClass('selected');
                $(this).parent().addClass('selected');
              });

            }

            editor.addButton('insertimage', {
              icon: 'image',
              tooltip: "Insert Images",
              onclick: insertImage
            });
        }
    });

    $('.ckeditor').ckeditor({
        toolbar: [
            { name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'clipboard', items : [ 'Cut','Copy','Paste','-','Undo','Redo' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','Image',
            '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
        ]
    });

});

// Jquery Validation
$.extend(jQuery.validator.messages, {
    required: 'This field is required.',
    email: 'Please input correct email address.',
    url: 'Worng URL format.',
    date: 'Wrong date format.',
    number: 'Input number only.',
    maxlength: jQuery.validator.format('Can not more than {0} character.'),
    minlength: jQuery.validator.format('Can not less than {0} character.'),
    max: jQuery.validator.format('Value cannot more than {0}.'),
    min: jQuery.validator.format('Value cannot less than {0}.'),
    minNumeric: jQuery.validator.format('Value cannot less than {0}.'),
});

$.validator.setDefaults({
    errorElement: 'em',
    errorPlacement: function (error, element) {
       element.closest('.form-group').find('.help-block').html(error.text());
    },
    highlight: function (element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parent('.form-group').removeClass('has-error');
        $(element).closest('.form-group').removeClass('has-error');
        $(element).closest('.form-group').find('.help-block').html('');
    },
    success: function (element) {
        $(element).closest('.form-group').removeClass('has-error');
        $(element).closest('.form-group').find('.help-block').html('');
    },
    // onkeyup: function(element){$(element).valid()},
    onChange: function(element){$(element).valid()},
});


// toast
function show_notification(title, type, message){
    $.toast({
         heading: title,
         text: message,
         position: 'top-right',
         loaderBg: '#fff',
         icon: type,
         hideAfter: 3500,
         stack: 6
    });
}


/*

added by : Ыулианто (2017-11-24)

*/

$(document).on('click', '.panel-heading span.clickable', function(e){
    var $this = $(this);
    if(!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('fa-angle-up').addClass('fa-angle-down');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    }
});