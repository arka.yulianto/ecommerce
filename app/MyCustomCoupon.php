<?php

namespace App\Coupons;

use LukePOLO\LaraCart\Contracts\CouponContract;
use LukePOLO\LaraCart\Traits\CouponTrait;

/**
 * Class MyCustomCoupon
 *
 * @package App\Coupons
 */
class MyCustomCoupon implements CouponContract
{
    use CouponTrait;

    /**
     * @param $code
     * @param $value
     */
    public function __construct($code, $value, $options = [])
    {
        $this->code = $code;
        $this->value = $value / 100;

        // this allows you to access your variables via $this->$option
        $this->setOptions($options);
    }

     /**
     * Gets the discount amount
     *
     * @param $throwErrors this allows us to capture errors in our code if we wish,
     * that way we can spit out why the coupon has failed
     *
     * @return string
     */
    public function discount($throwErrors = false)
    {
        // $this->minAmount was passed to the $options when constructing the coupon class
        $this->checkMinAmount($this->minAmount, $throwErrors);
        return \LaraCart::subTotal(false) * $this->value;
    }

    /**
     * Displays the type of value it is for the user
     *
     * @return mixed
     */
    public function displayValue($locale = null, $internationalFormat = null)
    {
        return \LaraCart::formatMoney($this->value, $locale, $internationalFormat);
    }
}