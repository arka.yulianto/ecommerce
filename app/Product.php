<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Helper;

class Product extends Model
{
    protected $table = 'tb_m_products';

    public function categories()
    {
    	return $this->belongsToMany(
    									'App\Category',
    									'tb_m_product_categories',
    									'product_id',
    									'category_id'
									);
    }

    public function attributes()
    {
    	return $this->hasMany(
								'App\AttributeProduct',
								'product_id'
							);
    }

    public function tags()
    {
    	 return $this->belongsToMany(
    	 								'App\Tag',
    	 								'tb_m_product_tags',
    	 								'product_id',
    	 								'tag_id'
	 								);
    }

    public function galleries()
    {
    	return $this->belongsToMany(
    	 								'App\Media',
    	 								'tb_m_product_galleries',
    	 								'product_id',
    	 								'media_id'
	 								);
    }

    public function scopeSlug($query, $slug)
    {
    	return $query->where('name', $slug);
    }

    public function media()
    {
        return $this->belongsTo('App\Media', 'media_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'product_id');
    }

    public function scopeRandomProduct($query, $category_id)
    {
        return $query->with(['categories', 'comments', 'categories.parent', 'categories.child'])->whereHas('categories', function($query) use ($category_id){
            $query->where('category_id', $category_id)
                    ->orWhere('parent_id', $category_id);
        })
        ->first();

    }

    public function scopeGetProduct($query, $limit)
    {
        return $query->orderByRaw('RAND()')
                    ->take($limit)
                    ->get();
    }

    public function scopeGetProductAttributes($query, $product_id)
    {
        return $query->find($product_id);
    }

    // mutator

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = Helper::setCurrency($value);
    }

    public function setDiscountAttribute($value)
    {
        if ($value != 0) {
            $this->attributes['discount'] = Helper::setCurrency($value);
        }
    }
}
