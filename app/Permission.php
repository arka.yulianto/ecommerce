<?php

namespace App;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
	protected $table = 'tb_m_permission';

	public function children()
	{
		return $this->hasMany('App\Permission', 'parent_id');
	}

	public function parent()
	{
		return $this->hasOne('App\Permission', 'id', 'parent_id');
	}
}
