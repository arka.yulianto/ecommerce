<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Media extends Model
{
    protected $table = 'tb_m_medias';

    public function getCreatedAtAttribute($value)
    {
    	return Carbon::parse($value)->format('M j Y');
    }
}
