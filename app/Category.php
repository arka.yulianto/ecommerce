<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'tb_m_categories';

    public function parent()
    {
    	return $this->belongsTo('App\Category', 'parent_id');
    }

    public function child()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public function media()
    {
    	return $this->belongsTo('App\Media', 'media_id');
    }

    public function product()
    {
        return $this->belongsToMany(
                        'App\Product',
                        'tb_m_product_categories',
                        'category_id',
                        'product_id'
                    );
    }

    public function scopeGetCategory($query, $limit)
    {
    	return $query->whereHas('product')
                    ->with(['product'])
                    ->where('parent_id', '!=', 0)
    				->orderByRaw('RAND()')
    				->take($limit)
    				->get();
    }

}
