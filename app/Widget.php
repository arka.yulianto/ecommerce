<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    protected $table = 'tb_m_widget';

    public function categories()
    {
    	return $this->belongsTo('App\Category', 'category_parent', 'id');
    }
}
