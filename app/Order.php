<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'tb_m_orders';

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function details()
    {
    	return $this->hasMany('App\OrderDetails', 'order_id');
    }

    public function shipping()
    {
    	return $this->hasOne('App\Shipping', 'order_id');
    }

}
