<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use GuzzleHttp\Client;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $table = 'tb_m_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'provider', 'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $api_token = '';

    public function __construct()
    {

        $client = new Client;
        $headers['Authorization'] = 'Basic OEJ6UkV1OFgxV20yRk9OZjlrMm1ScjdZbjRZYTppdUpLVzlKWXl2UHhyOEdsQkdHcncxcG1TS1lh';
        $headers['Content-Type'] = 'application/x-www-form-urlencoded';

        $body['grant_type'] = 'client_credentials';

        $res = $client->post(
                'https://api.mainapi.net/token', ['headers' => $headers, 'form_params' => $body]
            );

        $contents = $res->getBody()->getContents();
        $this->api_token = json_decode($contents)->access_token;
    }

    public function user_data()
    {
        return $this->hasOne('App\UserData', 'user_id');
    }

    public function sendPasswordResetNotification($token)
    {
        $client = new Client;
        $headers['Authorization'] = 'Bearer '.$this->api_token;
        $body['token'] = $this->getToken();
        $body['subject'] = 'Reset Password';
        $body['to'] = request()->email;
        $body['body'] = 'Your password was reset, please go to this link '.route('password.reset', $token);

         $res = $client->post(
                'https://api.mainapi.net/helio/1.0.1/sendmail',
                ['headers' => $headers, 'json' => $body]
            );

        $content = $res->getBody()->getContents();
        if (json_decode($content)->status == 200) {
            return redirect()
                    ->back()
                    ->with('message', 'Email reset was sended');
        } else {
            return redirect()
                    ->back()
                    ->with('message', 'There some error I dunno why');
        }
    }

    private function getToken()
    {
        $client = new Client;
        $headers['Authorization'] = 'Bearer '.$this->api_token;
        $body['email'] = 'admin@yuliantosb.xyz';
        $body['password'] = '13-Juli-1995';

        $res = $client->post(
                'https://api.mainapi.net/helio/1.0.1/login',
                ['headers' => $headers, 'json' => $body]
            );

        $content = $res->getBody()->getContents();
        $token = json_decode($content)->result->user->token;

        return $token;

    }


}
