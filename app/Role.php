<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
 	protected $table = 'tb_m_roles';
 	protected $fillable = ['*'];
 	
}