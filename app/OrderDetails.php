<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $table = 'tb_m_order_details';
    protected $fillable = ['*'];
    public $timestamps = false;

    public function attributes()
    {
    	return $this->hasMany('App\OrderAttributes', 'order_detail_id');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product', 'product_id');
    }
}
