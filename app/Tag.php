<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tb_m_tags';
    protected $fillable = ['name', 'slug'];
}
