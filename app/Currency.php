<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'tb_m_currency';

    public function scopeGetCurrency($query, $alias)
    {
    	
		return $query->where('alias', $alias)
    					->first();
    	
    }
}
