<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeProduct extends Model
{
    protected $table = 'tb_m_product_attributes';

    protected $fillable = ['attribute_name', 'attribute_value'];
}
