<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'tb_m_menus';

    public function child()
    {
    	return $this->hasMany('App\Menu', 'parent_id')
					->orderBy('order_number');
    }

    public function widget()
    {
    	return $this->belongsTo('App\Widget', 'widget_id');
    }

    public function scopeGetMenu($query, $parent_id)
    {
    	return $query->where('parent_id', $parent_id)
    				->orderBy('order_number')
    				->get();
    }
}
