<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Widget;

class WidgetController extends Controller
{
    public function index()
    {
    	$widgets = Widget::with(['categories'])->get();
    	return view('backend.pages.widget.index')
    			->with('widgets', $widgets);

    }

    public function create()
    {
    	$categories = Category::get();

		return view('backend.pages.widget.create')
				->with('categories', $categories);
    }

    public function store(Request $request)
    {
    	$widget = new Widget;
    	$widget->name = $request->name;
		$widget->type = $request->type;
		
		if ($request->type == 'categories') {
			$widget->category_parent = $request->category_parent;
			$widget->limit = $request->limit;
		}

		if ($request->type == 'text') {
			$widget->content = $request->content;
		}

		if ($request->type == 'product') {
			$widget->category_id = $request->category_id;
		}
		
		$widget->save();

		$res = [
					'title' => 'Success',
					'type' => 'success',
					'message' => 'Data saved successfully!'

				];

		return redirect()
					->route('widget.index')
					->with($res);
    }

    public function destroy($id)
    {
    	$widget = Widget::find($id);
    	$widget->delete();

    	$res = [
					'title' => 'Success',
					'type' => 'success',
					'message' => 'Data deleted successfully!'

				];

		return redirect()
					->route('widget.index')
					->with($res);

    }
}
