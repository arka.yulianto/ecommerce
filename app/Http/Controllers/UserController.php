<?php

/*
@author: Fuadz
@created at: 1511539711
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Image;
use Helper;
use Carbon\Carbon;
use App\Role;
use Gravatar;
use DB;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index()
    {
    	$users = User::paginate(12);

    	$data = [
    				'users' => $users
    			];

    	return view('backend.pages.user.index')
    			->with($data);
    }

    public function create()
    {
        $roles = Role::get();
        $data = [
                    'roles'  => $roles
                ];

    	return view('backend.pages.user.create')
                ->with($data);
    }

    public function store(Request $request)
    {

        DB::transaction(function() use ($request){

            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $userdata = [

                'picture' => !empty($request->avatar) ? $request->avatar : Gravatar::get($request->email, ['size'=> 200]),
                'fullname' => $request->fullname,
                'identity_number' => $request->identity_number,
                'phone_number' => $request->phone_number,
                'sex' => $request->sex,
                'place_of_birth' => $request->place_of_birth,
                'date_of_birth' => $request->date_of_birth,
                'address' => $request->address,

            ];

            $user->roles()->attach($request->role_id);
            $user->user_data()->create($userdata);

        });

        $res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been saved successfuly!'];

        return redirect()
                ->route('user.index')
                ->with($res);
    }

    public function edit($id)
    {
        $roles = Role::get();
        $user = User::find($id);

        $data = [
                    'roles'  => $roles,
                    'user' => $user
                ];

        return view('backend.pages.user.edit')
                ->with($data);
    }

    public function update(Request $request, $id)
    {

        DB::transaction(function() use ($request, $id){

            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $userdata = [

                'picture' => !empty($request->avatar) ? $request->avatar : Gravatar::get($request->email, ['size'=> 200]),
                'fullname' => $request->fullname,
                'identity_number' => $request->identity_number,
                'phone_number' => $request->phone_number,
                'sex' => $request->sex,
                'place_of_birth' => $request->place_of_birth,
                'date_of_birth' => $request->date_of_birth,
                'address' => $request->address,

            ];

            $user->detachRoles();
            $user->roles()->attach($request->role_id);
            $user->user_data()->delete();
            $user->user_data()->create($userdata);

        });

        $res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been updated successfuly!'];

        return redirect()
                ->route('user.index')
                ->with($res);
    }

    public function show($id)
    {
        $user = User::find($id);

        $data = [
                    'user' => $user
                ];

        return view('backend.pages.user.show')
                ->with($data);
    }

    public function upload(Request $request)
    {
        $file = $request->file;
        $extension = $file->getClientOriginalExtension();
        $dir = public_path('uploads/avatars/');
        $filename = base64_encode($request->email).'.'.$extension;
        $file->move($dir, $filename);

        $image = Image::make($dir.'/'.$filename);
        $image->fit(600, 600);
        $image->save();

        return response()->json(['avatar' => $filename]);
    }
}
