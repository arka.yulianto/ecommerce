<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RajaOngkir;
use Helper;
use LaraCart;
use App\Product;
use Auth;

class ShippingController extends Controller
{
    public function index(Request $request)
    {

        $provinces = RajaOngkir::Provinsi()->all();

    	if ($request->ajax()) {

    		return response()->json($provinces);

    	} else {

            $products = LaraCart::getItems();

            if (count($products) > 0) {

                if (session()->has('shipping')) {
                    $shipping = session()->get('shipping')[0];
                    $cities = RajaOngkir::Kota()->byProvinsi($shipping['province_id'])->get();
                    $services = RajaOngkir::Cost([
                                    'origin'        => 22, // id kota asal
                                    'destination'   => $shipping['city_id'], // id kota tujuan
                                    'weight'        => $shipping['total_weight'], // berat satuan gram
                                    'courier'       => $shipping['courier_id'], // kode kurir pengantar ( jne / tiki / pos )
                                ])->get();

                } else {

                    if (Auth::check()) {

                        $shipping = Auth::user()->user_data;
                        $shipping->email = Auth::user()->email;
                        $cities = RajaOngkir::Kota()->byProvinsi($shipping['province_id'])->get();
                        $services = [];

                    } else {

                        $shipping = [];
                        $cities = [];
                        $services = [];
                    }

                }

                $weight = Product::whereIn('id', collect($products)->pluck('id'))->sum('weight');
                return view('frontend.pages.shipping.index', compact(['provinces', 'products', 'weight', 'shipping', 'cities', 'services']));

            } else {

                return redirect()->route('cart.index');
            }

            // return dd(session()->all());

            // session()->forget('shipping');

        }
    }

    public function show(Request $request, $id)
    {
    	if ($request->ajax()) {
    		$datas = RajaOngkir::Kota()->byProvinsi($id)->get();
    		return response()->json($datas);
    	}
    }

    public function store(Request $request)
    {
    	if ($request->ajax()) {

    		$data = RajaOngkir::Cost([
				'origin' 		=> 22, // id kota asal
				'destination' 	=> $request->city_id, // id kota tujuan
				'weight' 		=> $request->total_weight, // berat satuan gram
				'courier' 		=> $request->courier_id, // kode kurir pengantar ( jne / tiki / pos )
			])->get();

    		
            $result = '';

            foreach ($data[0]['costs'] as $index => $cost){

                $result .= '<tr class="g-color-gray-dark-v4 g-font-size-13">
                            <td class="align-top g-py-10">
                              <label class="form-check-inline u-check d-block u-link-v5 g-color-gray-dark-v4 g-color-primary--hover g-pl-30">
                              <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" name="service_id" type="radio" onclick="get_cost()" data-cost-value="'.Helper::setCurrency($cost['cost'][0]['value'], 'idr').'" data-service-name="'.$cost['service'].'" data-service-description="'.$cost['description'].'" data-estimate="'.$cost['cost'][0]['etd'].'" required="required">
                                <span class="d-block u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                  <i class="fa" data-check-icon="&#xf00c"></i>
                                </span>
                                '.$cost['service'].'
                              </label>
                            </td>
                            <td class="align-top g-py-10">'.$cost['description'].'</td>
                            <td class="align-top g-py-10">'.$cost['cost'][0]['etd'].' Day(s)</td>
                            <td class="align-top g-py-10 text-right">'.Helper::currency(Helper::setCurrency($cost['cost'][0]['value'], 'idr')).'</td>
                          </tr>';
            }

            return $result;

    	} else {

            if ($request->has('submit_shipping')) {

                if (session()->has('shipping')) {
                    session()->forget('shipping');
                }

                $data = [

                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'phone_number' => $request->phone_number,
                    'country_id' => $request->country_id,
                    'province_id' => $request->province_id,
                    'city_id' => $request->city_id,
                    'zip' => $request->zip,
                    'address' => $request->address,
                    'courier_id' => $request->courier_id,
                    'total_weight' => $request->total_weight,
                    'cost' => $request->cost,
                    'service_name' => $request->service_name,
                    'service_description' => $request->service_description,
                    'estimate_delivery' => $request->estimate_delivery,

                ];

                $shipping = session()->push('shipping', $data);
                $fee = LaraCart::addFee('shippingFee', $request->cost, $taxable =  true, $options = []);

                return redirect()->route('checkout.index');

            }

        }
    }

    public function update(Request $request, $id)
    {
    	$fee = LaraCart::addFee('shippingFee', $request->cost, $taxable =  false, $options = []);
    	return back();
    }

    public function getServices(Request $request)
    {

        if ($request->ajax()) {

            $data = RajaOngkir::Cost([
                'origin'        => 22, // id kota asal
                'destination'   => $request->city_id, // id kota tujuan
                'weight'        => $request->total_weight, // berat satuan gram
                'courier'       => $request->courier_id, // kode kurir pengantar ( jne / tiki / pos )
            ])->get();

        $result = '';

        foreach ($data[0]['costs'] as $index => $cost){


            $result .= '<tr class="g-color-gray-dark-v4 g-font-size-13">
                                <td class="align-top g-py-10">
                                    '.$cost['service'].'
                                </td>
                                <td class="align-top g-py-10">'.$cost['description'].'</td>
                                <td class="align-top g-py-10">'.$cost['cost'][0]['etd'].' Day(s)</td>
                                <td class="align-top g-py-10 text-right">'.Helper::currency(Helper::setCurrency($cost['cost'][0]['value'], 'idr')).'</td>
                              </tr>';
        }

            return $result;
        }

    }
}
