<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $products = Product::limit(9)
                            ->orderby('id', 'desc')
                            ->get();

        $data = [
                    'products' => $products
                ];

        return view('frontend.pages.home.index')
                ->with($data);

    }
}
