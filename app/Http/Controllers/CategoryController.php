<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use DataTables;

class CategoryController extends Controller
{

	public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index(Request $request)
    {
    	if ($request->ajax()) {

    		$category = Category::with(['parent'])
    							->get();

    		return DataTables::of($category)

    		->rawColumns(['options', 'description'])

    		->addColumn('options', function($data){
    			return '<a class="btn btn-success btn-xs" href="'.route('category.edit', $data->id).'">Edit</a>
                        <button class="btn btn-danger btn-xs" onclick="on_delete('.$data->id.')">Delete</button>';
    		})

    		->addColumn('parent.name', function($data){
    			return $data->parent_id == 0 ? '-' : $data->parent->name;
    		})

            ->addColumn('description', function($data){
                return substr($data->description, 0, 80);
            })

    		->make(true);
    	} else {
    		return view('backend.pages.category.index');
    	}
    }

    public function create()
    {

    	$parents = Category::get();

    	return view('backend.pages.category.create')
    			->with('parents', $parents);
    }

	public function store(Request $request)
	{


		$category = new Category;
		$category->name = $request->name;
		$category->parent_id = $request->parent_id;
		$category->slug = str_slug($request->name);
		$category->description = $request->description;
		$category->media_id = $request->media_id;
		$category->save();

		$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been saved successfuly!', 'added' => (string) $category->id];

		if($request->ajax()) {

			return response()->json($res);

		} else {

			return redirect()->route('category.index')
						->with($res);

		}
	}

    public function edit($id)
    {
        $parents = Category::get();

        $category = Category::find($id);

        return view('backend.pages.category.edit')
                ->with('parents', $parents)
                ->with('category', $category);
    }

    public function update(Request $request, $id)
    {


        $category = Category::find($id);
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->slug = str_slug($request->name);
        $category->description = $request->description;
        $category->media_id = $request->media_id;
        $category->save();

        $res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been updated successfuly!', 'added' => (string) $category->id];

        if($request->ajax()) {

            return response()->json($res);

        } else {

            return redirect()->route('category.index')
                        ->with($res);

        }
    }

    public function destroy(Request $request, $id)
    {

        if($request->ajax()) {

            $category = Category::find($id);
            $category->delete();

            $res = [
                        'title' => 'Success',
                        'type' => 'success',
                        'message' => 'Data has been deleted successfuly!'

                    ];

            return response()->json($res);

        }
    }

    public function getCategory()
    {
    	$category = Category::select('id', 'name as text')
    					->orderBy('name')
    					->get();

    	return $category->toJson();
    }

    public function getParent(Request $request)
    {

    	if ($request->ajax()) {

    		$no_parent = [['id' => 0, 'text' => 'No Parent']];

    		$category = Category::select('id', 'name as text')
    					->orderBy('name')
    					->where('parent_id', 0)
    					->get();

    		return response()->json(array_merge($no_parent, $category->toArray()));
    	}
    	
    }
}
