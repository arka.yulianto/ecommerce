<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Validator;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)
                        ->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($data, $provider)
    {

        //DB::transaction(function() use ($data, $provider) {

            $authUser = User::where('provider_id', $data->id)->first();
            $check = User::where('email', $data->email)->first();
            
            if ($authUser || $check) {
                return $authUser;
            } else {

                $avatar = $provider == 'google' ? str_replace('?sz=50', '', $data->getAvatar()) : str_replace('type=normal', 'type=large',  $data->getAvatar());

                $fullname = explode(' ', $data->name);

                $user = new User();
                $user->name = strtolower($fullname[0]);
                $user->email = $data->email;
                $user->provider = $provider;
                $user->provider_id = $data->id;
                $user->password = bcrypt(str_random(16));
                $user->save();

                $userdata = [

                    'first_name' => $fullname[0],
                    'last_name' => !empty($fullname[1]) ? $fullname[1] : null,
                    'picture' => $avatar,
                ];

                $user->user_data()->create($userdata);
                $user->attachRole(2);

                return $user;
            }

        //});
        
    }
}
