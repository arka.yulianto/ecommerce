<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use LaraCart;
use App\Product;
use App\Coupon;
use Carbon\Carbon;
use App\MyCustomCoupon;
use GuzzleHttp;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use RajaOngkir;

class CartController extends Controller
{

	public function index()
	{
		$carts = LaraCart::getItems();
        $weight = Product::whereIn('id', collect($carts)->pluck('id'))->sum('weight');
        $provinces = RajaOngkir::Provinsi()->all();

		return view('frontend.pages.cart.index', compact(['carts', 'weight', 'provinces']));
        // return $carts;

	}

    public function store(Request $request)
    {
    	
    	$product = Product::find($request->id);
    	$price = !empty($product->discount) ? $product->discount : $product->price;
        $attribute = [];

        if (count($request->attribute_name) > 0) {

            foreach ($request->attribute_name as $index => $attribute_name) {
                $attribute[] = ['name' => $attribute_name, 'value' => $request->attribute_value[$index]];
            }
        }

        $qty = !empty($request->qty) ? $request->qty : 1;

    	$cart = LaraCart::add(
    							$product->id,
    							$product->name,
    							$qty,
    							$price,
                                [
                                    'picture' => $product->media->name,
                                    'attributes' => $attribute
                                ],
                                true
							);

    	return redirect()->back();

    }

    public function show(Request $request, $id)
    {

    }

    public function edit(Request $request, $id)
    {
        if ($request->ajax()) {

            $product = Product::find($id);
            return view('frontend.pages.cart.change_attribute', compact('product'));
        }
        
    }

    public function update(Request $request, $id)
    {

        $attributes = [];

        foreach ($request->attribute_name as $index => $attribute_name) {
            $attributes[] = ['name' => $attribute_name, 'value' => $request->attribute_value[$index]];
        }

        $cart = LaraCart::updateItem($request->row_id, 'attributes', $attributes);
        return back();
    }


    public function bulkUpdate(Request $request)
    {

        if ($request->has('submit_shipping')) {
                
            $fee = LaraCart::addFee('shippingFee', $request->cost, $taxable =  false, $options = []);
            
            return back();

        } else if ($request->has('submit_coupon')) {

            $get_coupon = Coupon::where('code', $request->coupon_code)
                                ->first();

            if (!empty($get_coupon)) {

                if ($get_coupon->type == 1) {

                    $coupon = new \LukePOLO\LaraCart\Coupons\Percentage($get_coupon->code, ($get_coupon->amount / 100), []);

                } else {
                    $coupon = new \LukePOLO\LaraCart\Coupons\Fixed($get_coupon->code, ($get_coupon->amount), []);
                }

                try {

                    $coupon->checkValidTimes(Carbon::parse($get_coupon->created_at), Carbon::parse($get_coupon->valid_thru));
                    $apply = LaraCart::addCoupon($coupon);
                    
                    return back();

                } catch (\LukePOLO\LaraCart\Exceptions\CouponException $e) {
                    $error = 'Coupon expired';
                    return back()->with('error', $error);
                }
                

            } else {

                $error = 'Coupon not found';
                return back()->with('error', $error);
            }

            
            
        } else {

            foreach ($request->row_id as $index => $row_id) {
                $cart = LaraCart::updateItem($row_id, 'qty', $request->qty[$index]);
            }

            return back();

        }

    }

    public function destroy($id)
    {
        $remove = LaraCart::removeItem($id);
        return redirect()->back();
    }

    public function remove($id)
    {
        $remove = LaraCart::removeItem($id);
        return redirect()->back();
    }

    public function coupon()
    {
        $coupon = new \LukePOLO\LaraCart\Coupons\Percentage('20%FREE', '0.20', [
            'description' => 'Lorem ipsum dolor sit amet'
        ]);

        $check = $coupon->checkMinAmount(300, $throwErrors = false);
        //$coupon->maxDiscount(100, 200, $throwErrors = true);
        // $coupon->checkValidTimes(Carbon::parse('2018-01-01'), Carbon::parse('2018-01-10'), $throwErrors = true);
        // $coupon->checkValidTimes(Carbon::parse('2018-01-17'), Carbon::parse('2018-01-20'), $throwErrors = true);

        dd($check);
        LaraCart::addCoupon($coupon);

    }

}
