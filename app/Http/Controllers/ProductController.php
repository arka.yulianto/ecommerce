<?php

/*
@author: Ыулианто
@created at: 2017-11-22
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use App\Product;
use App\Tag;
use App\Category;
use App\Currency;
use Helper;

class ProductController extends Controller
{

	public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }
    
    public function index()
    {
    	return view('backend.pages.product.index');
    }

    public function create()
    {

    	$tags = Tag::get();

    	if (session()->has('currency')){
              $sel_currency = session()->get('currency');
        } else {
              $sel_currency = 'usd';
        }

        $currency = Currency::getCurrency($sel_currency);

    	$data = [
    				'tags' => $tags,
    				'currency' => $currency
				];

    	return view('backend.pages.product.create')
    			->with($data);
    }

    public function edit($id)
    {

    	$tags = Tag::get();
    	$categories = Category::get();
    	$product = Product::find($id);

    	if (session()->has('currency')){
              $sel_currency = session()->get('currency');
        } else {
              $sel_currency = 'usd';
        }

        $currency = Currency::getCurrency($sel_currency);


    	$data = [
    				'tags' => $tags,
    				'product' => $product,
    				'categories' => $categories,
    				'currency' => $currency
				];

    	return view('backend.pages.product.edit')
    			->with($data);
    }

    public function show($id)
    {
    	$product = Product::find($id);
    	$data = [
    				'product' => $product
				];

    	return view('backend.pages.product.show')
    			->with($data);
    }

    public function store(Request $request)
    {

    	DB::transaction(function() use ($request){

    		$check_slug = Product::slug($request->name);
	    	$slug = $check_slug->count() > 0 ? str_slug($request->name).'-'.$check_slug->count() : str_slug($request->name);

	    	// product
	    	$product = new Product;
	    	$product->name = $request->name;
			$product->description = $request->description;
			$product->media_id = $request->media_id;
			$product->price = !empty($request->price) ? str_replace(',', '', $request->price) : null;
			if ($request->discount != 0 || !empty($request->discount)) {
				$product->discount = str_replace(',', '', $request->discount);
			}
			$product->sku = $request->sku;
			$product->stock = $request->stock;
			$product->weight = $request->weight;
			$product->height = $request->height;
			$product->slug = $slug;
			$product->save();

			// tags

			if (count($request->tags) > 0){

				foreach ($request->tags as $tag) {
					$tag = Tag::firstOrCreate(['name' => $tag, 'slug' => str_slug($tag)]);
					if ($tag) {
						$tagIds[] = $tag->id;
					}
				}

				// tags
				$product->tags()->sync($tagIds);

			}

			if (count($request->attribute_name) > 0) {

				// attributes
				foreach ($request->attribute_name as $index => $attributes) {
					$data[] = [
									'attribute_name' => $attributes,
									'attribute_value' => implode(',', $request->attribute_value[$index])
								];
				}

				// attributes
				$product->attributes()->createMany($data);

			}
			
			if (count($request->category_id) > 0) {
				// categories
				$product->categories()->sync($request->category_id);
			}
			
			if (count($request->galleries) > 0) {
				// galleries
				$product->galleries()->sync($request->galleries);
			}

    	});

    	$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been saved successfuly!'];

    	return redirect()
    			->route('product.index')
    			->with($res);

    }

    public function update(Request $request, $id)
    {
    	DB::transaction(function() use ($request, $id){

    		$check_slug = Product::slug($request->name);

	    	$slug = $check_slug->count() > 0 ? str_slug($request->name).'-'.$check_slug->count() : str_slug($request->name);

	    	// product
	    	$product = Product::find($id);
	    	$product->name = $request->name;
			$product->description = $request->description;
			$product->media_id = $request->media_id;
			$product->price = !empty($request->price) ? str_replace(',', '', $request->price) : null;
			if ($request->discount != 0 || !empty($request->discount)) {
				$product->discount = str_replace(',', '', $request->discount);
			}
			$product->sku = $request->sku;
			$product->stock = $request->stock;
			$product->weight = $request->weight;
			$product->height = $request->height;
			$product->slug = $slug;
			$product->save();

			// tags

			if (count($request->tags) > 0){

				foreach ($request->tags as $tag) {
					$tag = Tag::firstOrCreate(['name' => $tag, 'slug' => str_slug($tag)]);
					if ($tag) {
						$tagIds[] = $tag->id;
					}
				}

				// tags
				$product->tags()->sync($tagIds);

			}

			if (count($request->attribute_name) > 0) {

				// attributes
				foreach ($request->attribute_name as $index => $attributes) {
					$data[] = [
									'attribute_name' => $attributes,
									'attribute_value' => implode(',', $request->attribute_value[$index])
								];
				}

				// attributes
				$product->attributes()->delete();
				$product->attributes()->createMany($data);

			}
			
			if (count($request->category_id) > 0) {
				// categories
				$product->categories()->sync($request->category_id);
			}
			
			if (count($request->galleries) > 0) {
				// galleries
				$product->galleries()->sync($request->galleries);
			}
			

    	});

    	$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been updated successfuly!'];

    	return redirect()
    			->route('product.index')
    			->with($res);
    }

    public function destroy(Request $request, $id)
    {
    	if ($request->ajax()) {

    		DB::transaction(function() use ($request, $id){
	    		$product = Product::find($id);
	    		$product->tags()->detach();
				$product->attributes()->delete();
				$product->categories()->detach();
				$product->galleries()->detach();
				$product->delete();
    		});

			$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been deleted successfuly!'];

			return response()->json($res);
    	}
    }

    public function getData(Request $request)
    {
    	$product = Product::with(['categories', 'tags', 'attributes', 'galleries'])
    						->get();

    	return DataTables::of($product)

    	->rawColumns(['options', 'categories', 'tags', 'price'])

    	->addColumn('options', function($data){

    		return '<a href="'.route('product.edit', $data->id).'" class="btn btn-primary btn-xs">
    					Edit
					</a>
    				<button class="btn btn-danger btn-xs" onclick="on_delete('.$data->id.')">Delete</button>
    				<a class="btn btn-info btn-xs" href="'.route('product.show', $data->id).'">
    					View
					</a>';

    	})


    	->addColumn('categories', function($data){

    		return $data->categories->pluck('name')->implode(', ');


    	})

		->addColumn('tags', function($data){

			$tags =  $data->tags->pluck('name');
			return '<span class="badge bg-info">'.$tags->implode('</span>&nbsp;<span class="badge bg-info">');
		})

		->addColumn('price', function($data){

			return !empty($data->discount) ? '<s><sup>'.Helper::currency($data->price).'</sup></s> '.Helper::currency($data->discount) : Helper::currency($data->price);

		})


    	->make(true);

    }

}
