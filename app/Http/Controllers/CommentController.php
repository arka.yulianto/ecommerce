<?php

/*

@author : Zul Syahar
@created at : Fri, 11 24 17 14 28

*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use DataTables;
use Helper;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }
    
    public function index()
    {
    	return view('backend.pages.comment.index');
    }

    public function show($id)
    {
    	$comment = Comment::find($id);

    	$data = [
    			'comment' => $comment
    		];

    	return view('backend.pages.comment.show')
    			->with($data);
    }

    public function destroy($id)
    {
    	$comment = Comment::find($id);
    	$comment->delete();

    	$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been deleted successfuly!'];

    	return response()->json($res);
    }

    public function getData(Request $request)
    {
    	$comment = Comment::with(['user', 'user.user_data', 'product'])
    					->get();

    	return DataTables::of($comment)

    	->rawColumns(['options', 'rate', 'content'])

    	->addColumn('rate', function($data){

    		return Helper::getRate($data->rate);

    	})

    	->addColumn('content', function($data){

    		return strlen($data->content) >= 30 ? '<span data-toggle="popover" data-content="'.$data->content.'" data-placement="left" data-trigger="hover">'.substr($data->content, 0, 30).'&nbsp;<small class="text-muted">[...]</small></span>' : $data->content;

    	})

    	->addColumn('options', function($data){

    		return '<button class="btn btn-danger btn-xs" onclick="on_delete('.$data->id.')">Delete</button>
    				<a class="btn btn-info btn-xs" href="'.route('review.show', $data->id).'">
    					View
					</a>';

    	})

    	->make(true);

    }
}
