<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DB;
use LaraCart;
use App\Order;

use Auth;

class ApiController extends Controller
{


	public $token = '';

    public function __construct()
    {

    	$client = new Client;
    	$headers['Authorization'] = 'Basic OEJ6UkV1OFgxV20yRk9OZjlrMm1ScjdZbjRZYTppdUpLVzlKWXl2UHhyOEdsQkdHcncxcG1TS1lh';
    	$headers['Content-Type'] = 'application/x-www-form-urlencoded';

    	$body['grant_type'] = 'client_credentials';

    	$res = $client->post(
			    'https://api.mainapi.net/token', ['headers' => $headers, 'form_params' => $body]
			);

		$contents = $res->getBody()->getContents();
		$this->token = json_decode($contents)->access_token;
    }

    public function index()
    {
        $client = new Client;
        $headers['Authorization'] = 'Bearer '.$this->token;
        $headers['Content-Type'] = 'application/x-www-form-urlencoded';
        $body['email'] = 'admin@yuliantosb.xyz';
        $body['password'] = '13-Juli-1995';

        $res = $client->post(
                'https://api.mainapi.net/helio/1.0.1/login',
                ['headers' => $headers, 'form_params' => $body]
            );

        $content = $res->getBody()->getContents();

        dd($content);
    }

    public function notification()
    {
        $client = new Client;

        $headers['Authorization'] = 'Bearer '.$this->token;
        $headers['Content-Type'] = 'application/x-www-form-urlencoded';
        $headers['Accept'] = 'application/json';

        $body['msisdn'] = '085779549820';
        $body['content'] = 'Lorem ipsum dolor sit amet consectetur adipiscing elit';

        $res = $client->post(
                'https://api.mainapi.net/smsnotification/1.0.0/messages',
                ['headers' => $headers, 'form_params' => $body]
            );

        $content = $res->getBody();

        return dd($content);
    }

    public function helio()
    {

        $client = new Client;
        $headers['Authorization'] = 'Bearer '.$this->token;
        $body['token'] = $this->getToken();
        $body['subject'] = 'Reset Password';
        $body['to'] = 'yulianto.saparudin@gmail.com';
        $body['body'] = view('mail.reset_password');

         $res = $client->post(
                'https://api.mainapi.net/helio/1.0.1/sendmail',
                ['headers' => $headers, 'json' => $body]
            );

        $content = $res->getBody()->getContents();
        dd($content);

    }

    public function getToken()
    {
        $client = new Client;
        $headers['Authorization'] = 'Bearer '.$this->token;
        $body['email'] = 'admin@yuliantosb.xyz';
        $body['password'] = '13-Juli-1995';

        $res = $client->post(
                'https://api.mainapi.net/helio/1.0.1/login',
                ['headers' => $headers, 'json' => $body]
            );

        $content = $res->getBody()->getContents();
        $token = json_decode($content)->result->user->token;

        return $token;

    }

}
