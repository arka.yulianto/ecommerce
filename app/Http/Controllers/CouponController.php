<?php

/*

@author: Razaq Hakim
@created at: Fri, Nov 24 2017

*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Coupon;
use DataTables;

class CouponController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }
    
    public function index()
    {
    	return view('backend.pages.coupon.index');
    }

    public function create()
    {

    	$products = Product::get();
    	$categories = Category::get();

    	$data = [

    		'products' => $products,
			'categories' => $categories,

    	];

    	return view('backend.pages.coupon.create')
    			->with($data);
    }

    public function store(Request $request)
    {
    	$coupon = new Coupon;
    	$coupon->code = $request->code;
		$coupon->amount = !empty($request->amount) ? str_replace(',', '', $request->amount) : null;
		$coupon->type = $request->type;
		$coupon->valid_thru = $request->valid_thru;
		$coupon->products = !empty($request->products) ? implode(',', $request->products) : null;
		$coupon->categories = !empty($request->categories) ? implode(',', $request->categories) : null;
		$coupon->exclude_products = !empty($request->exclude_products) ? implode(',', $request->exclude_products) : null;
		$coupon->exclude_categories = !empty($request->exclude_categories) ? implode(',', $request->exclude_categories) : null;
		$coupon->limit_user = $request->limit_user;
		$coupon->limit_coupon = $request->limit_coupon;
		$coupon->limit_item = $request->limit_item;
		$coupon->minimum_spend = !empty($request->minimum_spend) ? str_replace(',', '', $request->minimum_spend) : null;
		$coupon->maximum_spend = !empty($request->maximum_spend) ? str_replace(',', '', $request->maximum_spend) : null;
		$coupon->individual_use_only = $request->individual_use_only;
		$coupon->exclude_sale_items = $request->exclude_sale_items;
		$coupon->allow_free_shipping = $request->allow_free_shipping;
		$coupon->save();

		$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been saved successfuly!'];

		return redirect()
				->route('coupon.index')
				->with($res);
    }

    public function edit($id)
    {
    	$products = Product::get();
    	$categories = Category::get();
    	$coupon = Coupon::find($id);

    	$data = [

    		'products' => $products,
			'categories' => $categories,
			'coupon' => $coupon

    	];

    	return view('backend.pages.coupon.edit')
    			->with($data);
    }


    public function update(Request $request, $id)
    {
    	$coupon = Coupon::find($id);
    	$coupon->code = $request->code;
		$coupon->amount = !empty($request->amount) ? str_replace(',', '', $request->amount) : null;
		$coupon->type = $request->type;
		$coupon->valid_thru = $request->valid_thru;
		$coupon->products = !empty($request->products) ? implode(',', $request->products) : null;
		$coupon->categories = !empty($request->categories) ? implode(',', $request->categories) : null;
		$coupon->exclude_products = !empty($request->exclude_products) ? implode(',', $request->exclude_products) : null;
		$coupon->exclude_categories = !empty($request->exclude_categories) ? implode(',', $request->exclude_categories) : null;
		$coupon->limit_user = $request->limit_user;
		$coupon->limit_coupon = $request->limit_coupon;
		$coupon->limit_item = $request->limit_item;
		$coupon->minimum_spend = !empty($request->minimum_spend) ? str_replace(',', '', $request->minimum_spend) : null;
		$coupon->maximum_spend = !empty($request->maximum_spend) ? str_replace(',', '', $request->maximum_spend) : null;
		$coupon->individual_use_only = $request->individual_use_only;
		$coupon->exclude_sale_items = $request->exclude_sale_items;
		$coupon->allow_free_shipping = $request->allow_free_shipping;
		$coupon->save();

		$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been updated successfuly!'];

		return redirect()
				->route('coupon.index')
				->with($res);
    }

    public function show($id)
    {

    	$coupon = Coupon::find($id);

    	$data = [

    		'coupon' => $coupon

    	];

    	return view('backend.pages.coupon.show')
    			->with($data);
    }

    public function destroy($id)
    {
    	$coupon = Coupon::find($id);
    	$coupon->delete();

    	$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been deleted successfuly!'];

    	return response()->json($res);

    }

    public function getData(Request $request)
    {
    	$coupon = Coupon::get();

    	return DataTables::of($coupon)

    	->rawColumns(['options'])

    	->addColumn('type', function($data){

    		return $data->type == 1 ? 'Percentage' : 'Fixed';

    	})

    	->addColumn('amount', function($data){
    		return number_format($data->amount, 2, '.', ',');
    	})

    	->addColumn('options', function($data){

    		return '<a href="'.route('coupon.edit', $data->id).'" class="btn btn-primary btn-xs">
    					Edit
					</a>
    				<button class="btn btn-danger btn-xs" onclick="on_delete('.$data->id.')">Delete</button>
    				<a class="btn btn-info btn-xs" href="'.route('coupon.show', $data->id).'">
    					View
					</a>';
    	})

    	->make(true);
    }
}
