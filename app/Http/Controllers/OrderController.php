<?php

/*
@author: Ыулианто
@created at: 2017-11-22
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Order;
use Carbon\Carbon;
use Helper;
use App\Shipping;
use DB;
use GuzzleHttp\Client;

class OrderController extends Controller
{

    private $api_token;

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);

        $client = new Client;
        $headers['Authorization'] = 'Basic OEJ6UkV1OFgxV20yRk9OZjlrMm1ScjdZbjRZYTppdUpLVzlKWXl2UHhyOEdsQkdHcncxcG1TS1lh';
        $headers['Content-Type'] = 'application/x-www-form-urlencoded';

        $body['grant_type'] = 'client_credentials';

        $res = $client->post(
                'https://api.mainapi.net/token', ['headers' => $headers, 'form_params' => $body]
            );

        $contents = $res->getBody()->getContents();
        $this->api_token = json_decode($contents)->access_token;

    }
    
    public function index()
    {
    	return view('backend.pages.order.index');
    }

    public function show($id)
    {
    	$order = Order::find($id);

    	$data = [
    				'order' => $order
    			];

    	return view('backend.pages.order.show')
    			->with($data);

    }

    public function getData(Request $request)
    {
    	$order = Order::with(['user', 'user.user_data'])
    				->get();

    	return DataTables::of($order)

    	->rawColumns(['options', 'status'])

    	->addColumn('status', function($data){

    		/*
			0 : In Proccess
			1 : Challange
			2 : Success
			3 : Fraud
    		*/

			$status = Helper::getStatusTransaction($data->status);

			return $status;


    	})

    	->addColumn('created_at', function($data){

    		return Carbon::parse($data->created_at)->format('F jS, Y h:i A');

    	})

    	->addColumn('options', function($data){
    		return '<a class="btn btn-primary btn-xs" href="'.route('order.show', $data->id).'">View</a>';
    	})

        ->addColumn('total', function($data){
            return Helper::currency($data->total);
        })

    	->make(true);
    }

    public function update($id, Request $request)
    {

        DB::transaction(function() use($id, $request){

            $order = Order::find($id);

            if ($request->status == 2) {

                $client = new Client;

                $headers['Authorization'] = 'Bearer '.$this->api_token;
                $headers['Content-Type'] = 'application/x-www-form-urlencoded';
                $headers['Accept'] = 'application/json';

                $body['msisdn'] = $order->user->user_data->phone_number;
                $body['content'] = 'Your order was complete, with receipt number '.$request->receipt_number.' Thanks, for trusting us';

                $res = $client->post(
                        'https://api.mainapi.net/smsnotification/1.0.0/messages',
                        ['headers' => $headers, 'form_params' => $body]
                    );
            }

            $order->status = $request->status;
            $order->shipping()->update(['receipt_number' => $request->receipt_number]);
            $order->save();

        });

        return redirect()
                ->back()
                ->with('message', 'Order was updated');
    }
}
