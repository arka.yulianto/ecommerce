<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RajaOngkir;
use LaraCart;
use Helper;
use Auth;
use DB;

// Paypal

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

// Models

use App\Order;
use App\Shipping;
use App\OrderDetails;
use App\OrderAttributes;

class CheckoutController extends Controller
{


    private $_api_context;

	public function __construct()
	{
		$this->middleware(['auth', 'role:admin|user']);        
        /** setup PayPal api context **/
        $paypal_conf = config()->get('paypal');
        $this->_api_context = new ApiContext(
                                new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));

        $this->_api_context->setConfig($paypal_conf['settings']);

	}

    public function index()
    {
    	if (session()->has('shipping')) {

            $shipping = session()->get('shipping')[0];
            $state = RajaOngkir::Provinsi()->find($shipping['province_id']);
            $city = RajaOngkir::Kota()->find($shipping['city_id']);
            $products = LaraCart::getItems();

            return view('frontend.pages.checkout.index', compact(['shipping', 'state', 'city', 'products']));

    	} else {
    		return redirect()->route('shipping.index');
    	}
    }

    public function store(Request $request)
    {

        $order_number = Auth::user()->id.'-'.time();

        if ($request->payment_method == 'paypal') {
            
            // Payer 
            $payer = new Payer();
            $payer->setPaymentMethod('paypal');

            $collection = [];
            $discount = [];

            foreach (LaraCart::getItems() as $index => $item) {

                $collection[] = (new Item)->setName($item->name)
                                    ->setCurrency('USD')
                                    ->setQuantity($item->qty)
                                    ->setPrice($item->price);

            }

            $shipping_fee = new Item;
            $shipping_fee->setName('Shipping Fee')
                        ->setCurrency('USD')
                        ->setQuantity(1)
                        ->setPrice(LaraCart::getFee('shippingFee')->amount);

            if (count(LaraCart::getCoupons()) > 0 ){


                foreach (LaraCart::getCoupons() as $coupon){
                    $discount[] = (new Item)->setName('Discount '.$coupon->code)
                                            ->setCurrency('USD')
                                            ->setQuantity(1)
                                            ->setPrice(-$coupon->discount());
                }
        
            }

            $subtotal = count($discount) > 0 ? array_merge([$shipping_fee], $discount) : [$shipping_fee];

            $item_list = new ItemList;
            $item_list->setItems(array_merge($collection, $subtotal));


            $amount = new Amount();
            $amount->setCurrency('USD')
                    ->setTotal(Helper::setCurrency(LaraCart::total($format = false, $withDiscount = true, $withTax = true, $withFees = true), 'usd'));

            // $amount = new Amount();
            // $amount->setCurrency('USD')
            //     ->setTotal(19);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                        ->setItemList($item_list)
                        ->setDescription('Transaction for #'.$order_number);

            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(url()->route('checkout.status')) /** Specify return URL **/
                            ->setCancelUrl(url()->route('checkout.status'));

            $payment = new Payment();
            $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));
                /** dd($payment->create($this->_api_context));exit; **/

            try {

                $payment->create($this->_api_context);

            } catch (\PayPal\Exception\PPConnectionException $ex) {
                
                if (config()->get('app.debug')) {

                    return redirect()->route('checkout.index')
                                        ->with('message', ['status' => 'failed', 'content' => 'Error timeout, please try again']);
                    /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                    /** $err_data = json_decode($ex->getData(), true); **/
                    /** exit; **/

                } else {
                    
                    return redirect()->route('checkout.index')
                                    ->with('message', ['status' => 'failed', 'content' => 'Some error occur, sorry for inconvenient']);
                    /** die('Some error occur, sorry for inconvenient'); **/
                }
            }

            foreach($payment->getLinks() as $link) {

                if($link->getRel() == 'approval_url') {
                    $redirect_url = $link->getHref();
                    break;
                }
            }

            /** add payment ID to session **/
            session()->put('paypal_payment_id', $payment->getId());
            session()->put('order_number', $order_number);

            if(isset($redirect_url)) {
                /** redirect to paypal **/
                return redirect()->away($redirect_url);
            }

            return redirect()->route('checkout.index')
                            ->with('message', ['status' => 'failed', 'content' => 'Unknown error occurred']);

        }
    }


    public function status(Request $request)
    {


        $payment_id = session()->get('paypal_payment_id');
        $order_number = session()->get('order_number');


        /** clear the session payment ID **/
        session()->forget('paypal_payment_id');
        if (empty($request->PayerID) || empty($request->token)) {

            return redirect()->route('checkout.payment')
                            ->with('message', ['status' => 'failed', 'content' => 'Your payment was cancelled']);
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId($request->PayerID);
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') { 
            
            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/

            DB::transaction(function() use ($order_number){

                $items = LaraCart::getItems();
                $shipping_data = session()->get('shipping')[0];

                $order = new Order;
                $order->order_number = $order_number;
                $order->sub_total = LaraCart::subTotal($format = false, $withDiscount = false);
                $order->coupon_id = null;
                $order->tax = LaraCart::taxTotal($formatted = false);
                $order->total = LaraCart::total($formatted = false, $withDiscount = true);
                $order->user_id = Auth::user()->id;
                $order->payment_method = 'PayPal';
                $order->save();

                $shipping = new Shipping;
                $shipping->first_name = $shipping_data['first_name'];
                $shipping->last_name = $shipping_data['last_name'];
                $shipping->email = $shipping_data['email'];
                $shipping->phone_number = $shipping_data['phone_number'];
                $shipping->country_id = $shipping_data['country_id'];
                $shipping->province_id = $shipping_data['province_id'];
                $shipping->city_id = $shipping_data['city_id'];
                $shipping->zip = $shipping_data['zip'];
                $shipping->address = $shipping_data['address'];
                $shipping->courier_id = $shipping_data['courier_id'];
                $shipping->total_weight = $shipping_data['total_weight'];
                $shipping->cost = $shipping_data['cost'];
                $shipping->service_name = $shipping_data['service_name'];
                $shipping->service_description = $shipping_data['service_description'];
                $shipping->estimate_delivery = $shipping_data['estimate_delivery'];

                $order->shipping()->save($shipping);

                foreach ($items as $item) {

                  $order_details = new OrderDetails;
                  $order_details->product_id = $item->id;
                  $order_details->price = $item->price;
                  $order_details->qty = $item->qty;
                  $order_details->coupon_id = null;
                  $order_details->total = $item->price * $item->qty;

                  $order->details()->save($order_details);

                    foreach ($item->attributes as $attributes) {

                        $order_attributes = new OrderAttributes;

                        $order_attributes->attribute_name = $attributes['name'];
                        $order_attributes->attribute_value = $attributes['value'];

                        $order_details->attributes()->save($order_attributes);

                    }
                }

            });

            LaraCart::destroyCart();

            return redirect()->route('checkout.payment')
                            ->with('message', ['status' => 'success', 'content' => 'Your payment with order number #'.$order_number.' is success, Thank you for trusting us']);
        }

            return redirect()->route('checkout.payment')
                            ->with('message', ['status' => 'failed', 'content' => 'Payment failed']);

    }

    public function payment()
    {
        if (session()->has('message')) {
            return view('frontend.pages.checkout.payment');
        } else {
            return redirect()->route('home.index');
        }
    }


    public function show($id)
    {
        foreach (LaraCart::getCoupons() as $coupon) {
            echo $coupon->code;
        }
    }
}
