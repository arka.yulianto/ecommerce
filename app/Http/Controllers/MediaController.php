<?php

/*
@author: Ыулианто
@created at: 2017-11-22
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Media;
use Image;
use Helper;
use Carbon\Carbon;

class MediaController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }
    
    public function uploads(Request $request)
    {
    	foreach ($request->file('file') as $file) {

    		$extension = $file->getClientOriginalExtension();
    		$realfilename = $file->getClientOriginalName();
    		$filesize = $file->getClientSize();

	        $dir = public_path('uploads/images/');
	        $filename = time() . '_' . rand() . '.' . $extension;
	        $file->move($dir, $filename);

	  //       $image = Image::make($dir.'/'.$filename);
			// $image->fit(1200, 800, null, 'bottom');
			// $image->save($dir.'1200x800/'.$filename);

	        $thumb1 = Image::make($dir.'/'.$filename);
	        $thumb1->fit(1920, 1080, null, 'bottom');
	        $thumb1->save($dir.'1920x1080/'.$filename);

         	$thumb2 = Image::make($dir.'/'.$filename);
	        $thumb2->fit(700, 700, null, 'bottom');
	        $thumb2->save($dir.'700x700/'.$filename);

	        $thumb3 = Image::make($dir.'/'.$filename);
	        $thumb3->fit(700, 467, null, 'bottom');
	        $thumb3->save($dir.'700x467/'.$filename);

            // $thumb4 = Image::make($dir.'/'.$filename);
            // $thumb4->fit(725, 725, null, 'bottom');
            // $thumb4->save($dir.'725x725/'.$filename);

            // $thumb5 = Image::make($dir.'/'.$filename);
            // $thumb5->fit(500, 320, null, 'bottom');
            // $thumb5->save($dir.'500x320/'.$filename);

            // $thumb6 = Image::make($dir.'/'.$filename);
            // $thumb6->fit(650, 850, null, 'bottom');
            // $thumb6->save($dir.'650x850/'.$filename);

            $thumb7 = Image::make($dir.'/'.$filename);
            $thumb7->fit(650, 750, null, 'bottom');
            $thumb7->save($dir.'650x750/'.$filename);

            $thumb8 = Image::make($dir.'/'.$filename);
            $thumb8->fit(480, 700, null, 'bottom');
            $thumb8->save($dir.'480x700/'.$filename);

            // $thumb9 = Image::make($dir.'/'.$filename);
            // $thumb9->fit(250, 170, null, 'bottom');
            // $thumb9->save($dir.'250x170/'.$filename);

            $thumb10 = Image::make($dir.'/'.$filename);
            $thumb10->fit(150, 150, null, 'bottom');
            $thumb10->save($dir.'150x150/'.$filename);

            if (file_exists($dir.'/'.$filename)) {
                unlink($dir.'/'.$filename);
            }

	        $media = new Media;
	        $media->name = $filename;
	        $media->alt = pathinfo($realfilename, PATHINFO_FILENAME);
	        $media->size = $filesize;
	        $media->save();

	      
	    }

	    $res = ['title' => 'Success', 'type' => 'success', 'message' => count($request->file).' uploaded successfuly'];

		return response()->json($res);

    }

    public function getData(Request $request)
    {
    	$media = Media::orderBy('id', 'desc');

    	if (isset($request->keyword)) {
    		clone $media->where('name', 'like', '%'.$request->keyword.'%')
    					->orWhere('alt', 'like', '%'.$request->keyword.'%')
    					->orWhere('caption', 'like', '%'.$request->keyword.'%')
    					->orWhere('description', 'like', '%'.$request->keyword.'%');
    	}

	 	if ($request->ajax()) {
            return view('backend.pages.media.load_data', ['media' => $media->get()])->render();  
        }
    }

    public function selectData($id)
    {
    	$media = Media::find($id);
    	$media->size = Helper::bytesToHuman($media->size);

    	return $media->toJson();
    }

    public function destroy($id)
    {
    	$media = Media::find($id);



        if (file_exists(public_path('uploads/images/1200x800/'.$media->name))) {
            unlink(public_path('uploads/images/1200x800/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/1920x1080/'.$media->name))) {
            unlink(public_path('uploads/images/1920x1080/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/700x700/'.$media->name))) {
            unlink(public_path('uploads/images/700x700/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/700x467/'.$media->name))) {
            unlink(public_path('uploads/images/700x467/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/725x725/'.$media->name))) {
            unlink(public_path('uploads/images/725x725/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/500x320/'.$media->name))) {
            unlink(public_path('uploads/images/500x320/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/650x850/'.$media->name))) {
            unlink(public_path('uploads/images/650x850/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/650x750/'.$media->name))) {
            unlink(public_path('uploads/images/650x750/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/480x700/'.$media->name))) {
            unlink(public_path('uploads/images/480x700/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/250x170/'.$media->name))) {
            unlink(public_path('uploads/images/250x170/'.$media->name));
        }
        if (file_exists(public_path('uploads/images/150x150/'.$media->name))) {
            unlink(public_path('uploads/images/150x150/'.$media->name));
        }

    	$media->delete();

    	$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Delete success!'];

    	return response()->json($res);

    }
}
