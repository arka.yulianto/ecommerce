<?php

/*

@author: Fuadz
@created at: 1512017980

*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Permission;

class PermissionController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }
    
    public function index(Request $request)
    {
    	if ($request->ajax()) {

    		$permission = Permission::with(['parent', 'children'])
    						->get();

    		return DataTables::of($permission)

    		->rawColumns(['options'])
			->addColumn('options', function($data){

	    		return '<a href="'.route('permission.edit', $data->id).'" class="btn btn-primary btn-xs">
	    					Edit
						</a>
	    				<button class="btn btn-danger btn-xs" onclick="on_delete('.$data->id.')">Delete</button>';
	    	})

	    	->addColumn('parent.name', function($data){
	    		return count($data->parent) > 0 ? $data->parent->name : '';
	    	})

    		->make(true);

    	} else {

    		return view('backend.pages.permission.index');
    	}
    }

    public function create()
    {
    	$permissions = Permission::where('parent_id', 0)
    							->get();

    	$data = [
    				'permissions' => $permissions
    			];

    	return view('backend.pages.permission.create')
    			->with($data);
    }

    public function store(Request $request)
    {
    	$permission = new Permission;
    	$permission->name = $request->name;
    	$permission->display_name = $request->display_name;
    	$permission->parent_id = $request->parent_id;
    	$permission->description = $request->description;
    	$permission->save();

    	$res = [
    				'title' => 'Success',
    				'type' => 'success',
    				'message' => 'Data has been saved successfuly!'
				];

    	return redirect()
    				->route('permission.index')
    				->with($res);
    }

    public function edit($id)
    {
    	$permissions = Permission::where('parent_id', 0)
    							->get();

    	$permission = Permission::find($id);


    	$data = [
    				'permissions' => $permissions,
    				'permission' => $permission
    			];

    	return view('backend.pages.permission.edit')
    			->with($data);
    }

    public function update(Request $request, $id)
    {
    	$permission = Permission::find($id);
    	$permission->name = $request->name;
    	$permission->display_name = $request->display_name;
    	$permission->parent_id = $request->parent_id;
    	$permission->description = $request->description;
    	$permission->save();

    	$res = [
    				'title' => 'Success',
    				'type' => 'success',
    				'message' => 'Data has been updated successfuly!'
				];

    	return redirect()
    				->route('permission.index')
    				->with($res);
    }

    public function destroy(Request $request, $id)
    {
    	if ($request->ajax()) {

    		$permission = Permission::find($id);
    		$permission->delete();

    		$res = [
    				'title' => 'Success',
    				'type' => 'success',
    				'message' => 'Data has been deleted successfuly!'
				];

    		return response()->json($res);

    	}
    }
}
