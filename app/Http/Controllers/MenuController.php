<?php

/*

@author : Kartika Sari
@created at : 1124172022

*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Menu;
use App\Widget;

class MenuController extends Controller
{

	public $arr_dummy = [['id' => 0, 'text' => 'No Parent']];

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index()
    {

    	$categories = Category::get();
    	$menus = Menu::where('parent_id', 0)
                    ->orderBy('order_number')
    				->get();
        $widgets = Widget::get();

    	$data = [

    		'menus' => $menus,
    		'categories' => $categories,
            'widgets' => $widgets

    	];

    	return view('backend.pages.menu.index')
    			->with($data);

    }

    public function store(Request $request)
    {
    	if ($request->ajax()) {

    		if ($request->type == 'category') {

                if (!empty($request->category)) {
                    foreach ($request->category as $category) {
                        $category = Category::find($category);
                        $menu = New Menu;
                        $menu->name = $category->name;
                        $menu->url = url('category/'.$category->id);
                        $menu->order_number = Menu::count() + 1;
                        $menu->save();
                    }
                }

    		} 

            if ($request->type == 'url') {

    			$menu = New Menu;
				$menu->name = $request->name;
				$menu->url = $request->url;
				$menu->order_number = Menu::count() + 1;
                $menu->is_mega = $request->is_mega;
				$menu->save();

    		}

            if ($request->type == 'widget') {

                $widget_id = $request->widget_id;

                $menu = New Menu;
                $menu->widget_id = $widget_id;
                $menu->name = $request->widget_name;
                $menu->tag_open = $request->tag_open;
                $menu->tag_close = $request->tag_close;
                $menu->order_number = Menu::count() + 1;

                $menu->save();
            }

    		$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been saved successfuly!'];

    		return response()->json($res);

    	}
    }

    public function edit(Request $request, $id)
    {
    	if ($request->ajax()) {

    		$menu = Menu::find($id);
    		return $menu->toJson();

    	}
    }

    public function update(Request $request, $id)
    {
    	if ($request->ajax()) {

    		$menu = Menu::find($id);
    		$menu->name = $request->name;
			$menu->url = $request->url;
			$menu->parent_id = $request->parent_id;
			$menu->order_number = $request->order_number;
			$menu->save();

			$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been updated successfuly!'];

    		return response()->json($res);

    	}
    }

    public function destroy(Request $request, $id)
    {
    	if ($request->ajax()) {

    		$menu = Menu::find($id);
    		$menu->delete();

    		$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been deleted successfuly!'];

    		return response()->json($res);
    	}
    }

    public function bulkEdit(Request $request)
    {
    	if ($request->ajax()) {

    		foreach ($request->data as $data) {
    			if (isset($data['id'])) {
    				$menu = Menu::find($data['id']);
	    			$menu->parent_id = $data['parent_id'] == null ? 0 : $data['parent_id'];
	    			$menu->order_number = $data['left'];
	    			$menu->save();
    			}
    		}

    		$res = ['title' => 'Success', 'type' => 'success', 'message' => 'Data has been updated successfuly!'];

    		return response()->json($res);

    	}
    }

    public function getData(Request $request)
    {

    	$menus = Menu::with(['child'])
    				->where('parent_id', 0)
    				->orderBy('order_number', 'asc')
    				->get();

    	if ($request->ajax()){

    		return view('backend.pages.menu.get_data', ['menus' => $menus])->render();  

    	}
    }

    public function getMenu()
    {
    	$menus = Menu::select('id', 'name as text')
    				->where('parent_id', 0)
    				->get();

    	return response()->json(array_merge($this->arr_dummy, $menus->toArray()));
    }
}
