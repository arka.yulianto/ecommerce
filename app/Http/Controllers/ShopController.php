<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Comment;

class ShopController extends Controller
{
    public function index()
    {

    	$products = Product::with(['categories'])->orderBy('id', 'desc')->paginate(9);
    	$categories = Category::getCategory(1);

    	return view('frontend.pages.shop.index', compact(['products', 'categories']));
    }

    public function details($slug)
    {
    	$product = Product::where('slug', $slug)
    						->first();

        if (!empty($product)) {

            $rate = Comment::where('product_id', $product->id)
                            ->avg('rate');

            $relateds = Product::whereHas('categories', function($where) use ($product){
                            $where->whereIn('category_id', $product->categories);       
                        })
                        ->take(4)
                        ->get();

            return view('frontend.pages.shop.details', compact(['product', 'relateds', 'rate']));

        } else {

           abort(404);
        }

    }
}
