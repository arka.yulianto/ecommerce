<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;

class ReviewController extends Controller
{
    public function store(Request $request)
    {
    	$comment = new Comment;
    	$comment->user_id = Auth::user()->id;
		$comment->product_id = $request->product_id;
		$comment->rate = $request->rate;
		$comment->content = $request->content;
		$comment->save();

		return back();
    }
}
