<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Order;
use Auth;
use DB;
use LaraCart;
use App\Shipping;
use App\OrderDetails;
use App\OrderAttributes;
use App\User;
use RajaOngkir;
use App\UserData;
use Validator;
use GuzzleHttp\Client;

class MemberController extends Controller
{

    public $api_token = '';

    public function __construct()
    {
        $this->middleware(['auth', 'role:user|admin']);

        $client = new Client;
        $headers['Authorization'] = 'Basic OEJ6UkV1OFgxV20yRk9OZjlrMm1ScjdZbjRZYTppdUpLVzlKWXl2UHhyOEdsQkdHcncxcG1TS1lh';
        $headers['Content-Type'] = 'application/x-www-form-urlencoded';

        $body['grant_type'] = 'client_credentials';

        $res = $client->post(
                'https://api.mainapi.net/token', ['headers' => $headers, 'form_params' => $body]
            );

        $contents = $res->getBody()->getContents();
        $this->api_token = json_decode($contents)->access_token;
    }

    public function dashboard()
    {

    	$comments = Comment::where('user_id', Auth::user()->id)
    						->orderBy('id', 'desc')
    						->limit(5)
    						->get();

    	$orders = Order::where('user_id', Auth::user()->id)
    					->orderBy('id', 'desc')
    					->limit(5)
    					->get();

    	return view('frontend.pages.user.dashboard', compact(['comments', 'orders']));
    }

    public function order()
    {

        $orders = Order::where('user_id', Auth::user()->id)
                        ->orderBy('id', 'desc')
                        ->paginate(5);


        return view('frontend.pages.user.order', compact(['orders']));
    }

    public function reviews()
    {
        $reviews = Comment::where('user_id', Auth::user()->id)
                        ->orderBy('id', 'desc')
                        ->paginate(5);

        return view('frontend.pages.user.reviews', compact(['reviews']));
    }

    public function profile()
    {
        $user = User::find(Auth::user()->id);
        $provinces = RajaOngkir::Provinsi()->all();
        $cities = RajaOngkir::Kota()->byProvinsi($user->user_data->province_id)->get();

        return view('frontend.pages.user.profile', compact(['user', 'provinces', 'cities']));
    }

    public function profileUpdate($id, Request $request)
    {
        $user_data = UserData::find($id);
        $user_data->first_name = $request->first_name;
        $user_data->last_name = $request->last_name;
        $user_data->identity_number = $request->identity_number;
        $user_data->phone_number = $request->phone_number;
        $user_data->place_of_birth = $request->place_of_birth;
        $user_data->date_of_birth = $request->date_of_birth;
        $user_data->sex = $request->sex;
        $user_data->country_id = $request->country_id;
        $user_data->province_id = $request->province_id;
        $user_data->city_id = $request->city_id;
        $user_data->address = $request->address;
        $user_data->zip = $request->zip;
        $user_data->save();

        return redirect()
                ->back()
                ->with('message', 'Profile updated');
    }

    public function wishlist()
    {
        return abort(404);
    }

    public function settings()
    {
        return view('frontend.pages.user.settings');
    }

    public function changePassword(Request $request)
    {

        if (empty(auth()->user()->user_data->phone_number)) {
            return redirect()
                        ->back()
                        ->with('message', ['type' => 'warning',
                                            'content' => 'You must add your phone number first']);
        } else {

            $client = new Client;
            $headers['Authorization'] = 'Bearer '.$this->api_token;
            $headers['Content-Type'] = 'application/x-www-form-urlencoded';

            $body['phoneNum'] = auth()->user()->user_data->phone_number;
            $body['digit'] = 6;
            $body['content'] = '{{otp}} is your number verification please submit before 60 seconds';


            $res = $client->put(
                    'https://api.mainapi.net/smsotp/1.0.1/otp/'.auth()->user()->id,
                    ['headers' => $headers, 'form_params' => $body]
                );

            $contents = $res->getBody()->getContents();

            if (json_decode($contents)->status == true) {

                session()->put(['new_password' => $request->password]);
                return redirect()->route('user.otp');

            } else {

                return redirect()
                        ->back()
                        ->with('message', ['type' => 'warning',
                                            'content' => 'Error when sending message, I dunno why']);
            }

        }

    }

    public function otp()
    {
        if (!empty(auth()->user()->user_data->phone_number) && session()->has('new_password')) {
            return view('frontend.pages.user.otp');
        } else{
            return redirect()
                    ->route('user.settings');
        }
        
    }

    public function otpSubmit(Request $request)
    {

        if ($request->has('resend_otp')) {

            $client = new Client;
            $headers['Authorization'] = 'Bearer '.$this->api_token;
            $headers['Content-Type'] = 'application/x-www-form-urlencoded';

            $body['phoneNum'] = auth()->user()->user_data->phone_number;
            $body['digit'] = 6;
            $body['content'] = '{{otp}} is your number verification please submit before 60 seconds';


            $res = $client->put(
                    'https://api.mainapi.net/smsotp/1.0.1/otp/'.auth()->user()->id,
                    ['headers' => $headers, 'form_params' => $body]
                );

            $contents = $res->getBody()->getContents();

            if (json_decode($contents)->status == true) {

                return redirect()
                    ->route('user.otp');


            } else {

                return redirect()
                        ->route('user.otp')
                        ->with('message', ['type' => 'warning',
                                            'content' => 'Error when sending message, I dunno why']);
            }
            
        } else {

            $client = new Client;
            $headers['Authorization'] = 'Bearer '.$this->api_token;
            $headers['Content-Type'] = 'application/x-www-form-urlencoded';

            $body['otpstr'] = $request->otp;
            $body['digit'] = 6;

            $res = $client->post(
                    'https://api.mainapi.net/smsotp/1.0.1/otp/'.auth()->user()->id.'/verifications',
                    ['headers' => $headers, 'form_params' => $body]
                );

            $contents = $res->getBody()->getContents();

            if (json_decode($contents)->status == true) {

                $user =  User::find(auth()->user()->id);
                $user->password = bcrypt(session()->get('new_password'));
                $user->save();

                session()->forget('new_password');

                return redirect()
                        ->route('user.settings')
                        ->with('message', ['type' => 'success',
                                            'content' => 'Your password updated']);

            } else {

                return redirect()
                        ->route('user.otp')
                        ->with('message', 'otp false');
            }

        }

    }

    public function test()
    {
        echo $this->api_token;
    }
}
