<?php

/*

@author : Fuadz
@created_at : 1512014970

*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use DB;
use App\Role;
use App\Permission;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }
    
    public function index(Request $request)
    {

    	if ($request->ajax()) {

    		$role = Role::get();

    		return DataTables::of($role)

    		->rawColumns(['options'])
			->addColumn('options', function($data){

	    		return '<a href="'.route('role.edit', $data->id).'" class="btn btn-primary btn-xs">
	    					Edit
						</a>
	    				<button class="btn btn-danger btn-xs" onclick="on_delete('.$data->id.')">Delete</button>
	    				<a class="btn btn-info btn-xs" href="'.route('role.show', $data->id).'">
	    					View
						</a>';
	    	})

    		->make(true);


    	} else {

    		return view('backend.pages.role.index');
    	}
    	
    }

    public function create()
    {
    	$permissions = Permission::where('parent_id', 0)
    							->get();

    	$data = [
	    			'permissions' => $permissions
	    		];

    	return view('backend.pages.role.create')
    			->with($data);
    }

    public function store(Request $request)
    {

    	DB::transaction(function() use ($request) {

    		$role = new Role;
    		$role->name = $request->name;
	    	$role->display_name = $request->display_name;
	    	$role->description = $request->description;
	    	$role->save();

	    	$role->perms()->sync($request->permission_id);

    	});

    	$res = [
    				'title' => 'Success',
    				'type' => 'success',
    				'message' => 'Data has been saved successfuly!'
				];

    	return redirect()
    				->route('role.index')
    				->with($res);
    }

    public function edit($id)
    {
    	$permissions = Permission::where('parent_id', 0)
    							->get();
    	$role = Role::find($id);

    	$data = [
	    			'permissions' => $permissions,
	    			'role' => $role
    			];

    	return view('backend.pages.role.edit')
    			->with($data);


    }

    public function update(Request $request, $id)
    {

    	DB::transaction(function() use ($request, $id) {

    		$role = Role::find($id);
    		$role->name = $request->name;
	    	$role->display_name = $request->display_name;
	    	$role->description = $request->description;
	    	$role->save();

	    	$role->perms()->sync($request->permission_id);

    	});

    	$res = [
    				'title' => 'Success',
    				'type' => 'success',
    				'message' => 'Data has been updated successfuly!'
				];

    	return redirect()
    				->route('role.index')
    				->with($res);
    }

    public function show($id)
    {

    	$role = Role::find($id);

    	$data = [
	    			'role' => $role
    			];


    	return view('backend.pages.role.show')
    			->with($data);
    }

    public function destroy(Request $request, $id)
    {

    	if ($request->ajax()) {

    		DB::transaction(function() use ($id){

	    		$role = Role::find($id);
	    		$role->perms()->sync([]);
	    		$role->delete();

	    	});

	    	$res = [
	    				'title' => 'Success',
	    				'type' => 'success',
	    				'message' => 'Data has been deleted successfuly!'
					];

	    	return response()->json($res);
    	}	
    	
    }
}

