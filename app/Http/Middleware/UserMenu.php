<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class UserMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $menus = [
                    [
                        'name' => 'Dashboard',
                        'url' => 'user.dashboard',
                        'icon' => 'icon-finance-141 u-line-icon-pro'
                    ],
                    [
                        'name' => 'Orders',
                        'url' => 'user.order',
                        'icon' => 'icon-basket'
                    ],
                    [
                        'name' => 'Wishlist',
                        'url' => 'user.wishlist',
                        'icon' => 'icon-medical-022 u-line-icon-pro'
                    ],
                    [
                        'name' => 'Reviews',
                        'url' => 'user.reviews',
                        'icon' => 'icon-bubble'
                    ],
                    [
                        'name' => 'Profile',
                        'url' => 'user.profile',
                        'icon' => 'icon-user'
                    ],
                    [
                        'name' => 'Settings',
                        'url' => 'user.settings',
                        'icon' => 'icon-settings'
                    ]
                ];

        Menu::make('userNavbar', function($data) use ($menus){

            foreach ($menus as $menu) {

                $data->add($menu['name'], ['route' => $menu['url']])
                    ->prepend('<span class="u-icon-v1 g-color-gray-dark-v5 mr-2"><i class="'.$menu['icon'].'"></i></span>')
                    ->link
                    ->attr(['class' => 'd-block align-middle u-link-v5 g-color-text g-color-primary--hover g-bg-gray-light-v5--hover rounded g-pa-3']);
            }

        });

        return $next($request);
    }
}
