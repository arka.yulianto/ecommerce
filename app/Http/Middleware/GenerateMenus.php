<?php

namespace App\Http\Middleware;

use Closure;
use App\Order;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('MyNavBar', function ($menu) {

            $order_count = Order::where('status', 0)
                                ->count();

            $menu->add('Dashboard', ['route' => 'dashboard'])
                    ->prepend('<i class="lnr lnr-home"></i>');
            $menu->add('Product')
                    ->prepend('<i class="lnr lnr-layers"></i> ')
                    ->link
                    ->attr([
                        'class' => 'has-arrow',
                        'href' => 'javascript:void(0)'
                    ]);
            $menu->product->add('All Product', ['route' => 'product.index'])->active('admin/product/*');
            $menu->product->add('Category', ['route' => 'category.index'])->active('admin/category/*');
            $menu->product->add('Tag', ['route' => 'tag.index'])->active('admin/tag/*');
            $menu->add('Comments', ['route' => 'review.index'])
                    ->prepend('<i class="lnr lnr-bubble"></i>')
                    ->active('admin/review/*');
            $menu->add('Orders', ['route' => 'order.index'])
                    ->prepend('<i class="lnr lnr-cart"></i>')
                    ->append($order_count > 0 ? '<span class="badge bg-success">'.$order_count.'</span>' : '')
                    ->active('admin/order/*');
            $menu->add('Coupon', ['route' => 'coupon.index'])
                    ->prepend('<i class="fa fa-ticket"></i>')
                    ->active('admin/coupon/*');
            $menu->add('Menu', ['route' => 'menu.index'])
                    ->prepend('<i class="lnr lnr-menu"></i>')
                    ->active('admin/menu/*');
            $menu->add('Widget', ['route' => 'widget.index'])
                    ->prepend('<i class="lnr lnr-screen"></i>')
                    ->active('admin/widget/*');
            $menu->add('Settings')
                    ->prepend('<i class="fa fa-cog"></i> ')
                    ->link
                    ->attr([
                        'class' => 'has-arrow',
                        'href' => 'javascript:void(0)'
                    ]);
             $menu->settings->add('User', ['route' => 'user.index'])->active('admin/user/*');
             $menu->settings->add('Role', ['route' => 'role.index'])->active('admin/role/*');
             $menu->settings->add('Permission', ['route' => 'permission.index'])->active('admin/permission/*');
        });

        return $next($request);
    }
}
