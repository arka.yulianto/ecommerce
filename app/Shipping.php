<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $table = 'tb_m_shipping';
    protected $fillable = [
							'order_id',
							'first_name',
							'last_name',
							'email',
							'phone_number',
							'country_id',
							'province_id',
							'city_id',
							'zip',
							'address',
							'courier_id',
							'total_weight',
							'cost',
							'service_name',
							'service_description',
							'estimate_delivery',
							'receipt_number'
						];

	public function order()
	{
		return $this->belongsTo('App\Order', 'order_id');
	}
}
