<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $table = 'tb_m_user_datas';
    protected $fillable = ['first_name', 'last_name', 'identity_number', 'phone_number', 'country_id',
    						'province_id', 'city_id', 'sex', 'zip', 'place_of_birth', 'date_of_birth',
							'address', 'picture', 'user_id'];

	public function getFullnameAttribute()
    {
       return "{$this->first_name} {$this->last_name}";
    }
}
