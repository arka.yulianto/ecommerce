<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAttributes extends Model
{
    protected $table = 'tb_m_order_detail_attributes';
    protected $fillable = ['*'];
    public $timestamps = false;
}
