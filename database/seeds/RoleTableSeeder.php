<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\UserData;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	User::truncate();
        Role::truncate();
        UserData::truncate();
        DB::table('tb_m_role_user')->truncate();

        $role1 = new Role;
        $role1->name = 'admin';
        $role1->display_name = 'Administration';
        $role1->description = 'Can access all modules';
        $role1->save();

        $role2 = new Role;
        $role2->name = 'user';
        $role2->display_name = 'Registed User';
        $role2->description = 'Can access what desserve';
        $role2->save();

		//add user
        $users = [
            [
                'name' => 'admin1',
                'email' => 'admin1@local.local',
                'password' => bcrypt('admin1'),
            ],
            [
                'name' => 'user1',
                'email' => 'user1@local.local',
                'password' => bcrypt('user1'),
            ],
        ];

        // userdata

        $userdatas = [
            [
                'first_name' => 'Yulianto',
                'last_name' => 'Saparudin',
                'identity_number' =>  '399103918391',
                'phone_number' => '089611081675',
                'country_id' => 'Indonesia',
                'province_id' => 9,
                'city_id' => 171,
                'sex' => 'Male',
                'zip' => '41362',
                'place_of_birth' => 'Karawang',
                'date_of_birth' => '1995-07-13',
                'address' => 'Jl. Sukaduka No 46',
                'picture' =>  'https://s.gravatar.com/avatar/dbe830afd9174dff8a1bedb0ffcea478?s=80'
            ],
            [
                'first_name' => 'Joel',
                'last_name' => 'Anthonio',
                'identity_number' =>  '399103918392',
                'phone_number' => '089611081676',
                'country_id' => 'Indonesia',
                'province_id' => 9,
                'city_id' => 171,
                'sex' => 'Male',
                'zip' => '41362',
                'place_of_birth' => 'Karawang',
                'date_of_birth' => '1995-07-13',
                'address' => 'Jl. Sukaduka No 46',
                'picture' =>  'https://s.gravatar.com/avatar/09c2bd3799a0310b6402afd2f6d8ab57?s=80'
            ]
        ];

        $n = 1;

        foreach ($users as $key => $value) {
            DB::transaction(function() use ($value, $n, $userdatas, $key){
                $user = User::create($value);
                $user->attachRole($n);
                $user->user_data()->create($userdatas[$key]);
                $n++;
            });            
        }
    }
}
