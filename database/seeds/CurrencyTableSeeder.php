<?php

use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
    					[
    						'symbol' => '&euro;',
    						'name' => 'Euro',
    						'alias' => 'eur',
    						'convertion' => 0.83,
    						'thousand_separator' => ',',
							'decimal_separator' => '.',

    					],
    					[
    						'symbol' => '&dollar;',
    						'name' => 'US Dollar',
    						'alias' => 'usd',
    						'convertion' => 1,
    						'thousand_separator' => ',',
							'decimal_separator' => '.',

    					],
    					[
    						'symbol' => 'Rp',
    						'name' => 'Rupiah',
    						'alias' => 'idr',
    						'convertion' => 13484,
    						'thousand_separator' => '.',
							'decimal_separator' => ',',

    					],

					];

		App\Currency::truncate();

		foreach ($currencies as $currency) {
			App\Currency::create($currency);
		}
		
    }
}
