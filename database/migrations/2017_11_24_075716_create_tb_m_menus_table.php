<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::dropIfExists('tb_m_menus');

        Schema::create('tb_m_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->string('name');
            $table->string('route_name')->nullable();
            $table->string('url')->nullable();
            $table->string('method_name')->nullable();
            $table->integer('order_number')->nullable();
            $table->string('icon')->nullable();
            $table->integer('widget_id')->nullable();
            $table->text('tag_open')->nullable();
            $table->text('tag_close')->nullable();
            $table->boolean('is_mega')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_m_menus');
    }
}
