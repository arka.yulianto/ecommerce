<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::dropIfExists('tb_m_orders');

        Schema::create('tb_m_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_number');
            $table->decimal('sub_total');
            $table->decimal('coupon_id')->nullable();
            $table->decimal('tax')->nullable();
            $table->decimal('total');
            $table->integer('user_id');
            $table->string('payment_method');
            $table->string('bank_name')->nullable();
            $table->string('account_name')->nullable();
            $table->string('bank_receive')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_m_orders');
    }
}
