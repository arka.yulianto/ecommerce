<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {

        Schema::dropIfExists('tb_m_permission_role');
        Schema::dropIfExists('tb_m_permissions');
        Schema::dropIfExists('tb_m_role_user');
        Schema::dropIfExists('tb_m_roles');

        // Create table for storing roles
        Schema::create('tb_m_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('tb_m_role_user', function (Blueprint $table) {

            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->primary(['user_id', 'role_id']);

        });

        // Create table for storing permissions
        Schema::create('tb_m_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('tb_m_permission_role', function (Blueprint $table) {

            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->primary(['permission_id', 'role_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('tb_m_permission_role');
        Schema::dropIfExists('tb_m_permissions');
        Schema::dropIfExists('tb_m_role_user');
        Schema::dropIfExists('tb_m_roles');
    }
}
