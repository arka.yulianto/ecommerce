<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tb_m_products');

        Schema::create('tb_m_products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('media_id')->nullable();
            $table->decimal('price');
            $table->decimal('discount')->nullable();
            $table->string('sku')->nullable();
            $table->bigInteger('stock')->nullable();
            $table->bigInteger('weight')->nullable();
            $table->bigInteger('height')->nullable();
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_m_products');
    }
}
