<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::dropIfExists('tb_m_coupons');
        
        Schema::create('tb_m_coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->decimal('amount', 15, 2);
            $table->integer('type')->nullable()->default(0);
            $table->date('valid_thru');
            $table->string('products')->nullable();
            $table->string('categories')->nullable();
            $table->string('exclude_products')->nullable();
            $table->string('exclude_categories')->nullable();
            $table->integer('limit_user')->nullable()->default(0);
            $table->integer('limit_coupon')->nullable()->default(0);
            $table->integer('limit_item')->nullable()->default(0);
            $table->decimal('minimum_spend', 15, 2)->nullable();
            $table->decimal('maximum_spend', 15, 2)->nullable();
            $table->integer('individual_use_only')->nullable()->default(0);
            $table->integer('exclude_sale_items')->nullable()->default(0);
            $table->integer('allow_free_shipping')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_m_coupons');
    }
}
