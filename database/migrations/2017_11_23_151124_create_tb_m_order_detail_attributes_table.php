<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbMOrderDetailAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::dropIfExists('tb_m_order_detail_attributes');

        Schema::create('tb_m_order_detail_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_detail_id');
            $table->string('attribute_name');
            $table->string('attribute_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_m_order_detail_attributes');
    }
}
