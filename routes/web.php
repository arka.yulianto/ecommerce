<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home.index');

Route::get('login/{driver}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
Route::get('login/{driver}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.provider_callback');
Auth::routes();

// admin page

Route::prefix('/admin')->group(function(){
	
	// admin dashboard
	Route::get('/', function(){
		return redirect()->route('dashboard');
	});

	Route::get('/dashboard', function(){
		return view('backend.pages.dashboard');
	})->name('dashboard')->middleware(['auth', 'role:admin']);


	// media
	Route::prefix('media')->group(function(){
		Route::post('uploads', 'MediaController@uploads')->name('media.uploads');
		Route::get('get_data', 'MediaController@getData')->name('media.get_data');
		Route::get('select_data/{id}', 'MediaController@selectData')->name('media.get_data');
	});

	Route::resource('/media', 'MediaController');

	// product
	Route::get('/product/get_data', 'ProductController@getData')->name('product.get_data');
	Route::resource('/product', 'ProductController');

	// category
	Route::get('/category/get_category', 'CategoryController@getCategory');
	Route::get('/category/get_parent', 'CategoryController@getParent');
	Route::resource('/category', 'CategoryController');

	// tags
	Route::resource('/tag', 'TagController');

	// order
	Route::get('/order/get_data', 'OrderController@getData')->name('order.get_data');
	Route::resource('/order', 'OrderController');

	// coupon
	Route::get('/coupon/get_data', 'CouponController@getData')->name('coupon.get_data');
	Route::resource('/coupon', 'CouponController');

	// comments
	Route::get('/review/get_data', 'CommentController@getData')->name('comment.get_data');
	Route::resource('/review', 'CommentController');

	// menu
	Route::get('/menu/get_data', 'MenuController@getData')->name('menu.get_data');
	Route::post('/menu/bulk_edit', 'MenuController@bulkEdit')->name('menu.bulk_edit');
	Route::get('/menu/get_menu', 'MenuController@getMenu')->name('menu.get_menu');
	Route::resource('/menu', 'MenuController');

	// user
	Route::post('/user/upload', 'UserController@upload');
	Route::resource('/user', 'UserController');

	// role
	Route::resource('/role', 'RoleController');

	// permission
	Route::resource('/permission', 'PermissionController');

	// widget
	Route::resource('/widget', 'WidgetController');


});

// Route::get('/home', 'HomeController@index')->name('home');

// cart
Route::post('/cart/bulk_update', 'CartController@bulkUpdate')->name('cart.bulk_update');
Route::get('/cart/remove/{hash}', 'CartController@remove')->name('cart.remove');
Route::get('/cart/coupon', 'CartController@coupon')->name('cart.coupon');
Route::resource('/cart', 'CartController');

// currency
Route::get('currency/{type}', 'CurrencyController@set');

// shop
Route::resource('/shop', 'ShopController');

// review

Route::resource('/review', 'ReviewController');

// shipping
Route::post('/shipping/get_services', 'ShippingController@getServices')->name('shipping.get_shipping');
Route::resource('/shipping', 'ShippingController');

// checkout
Route::get('/checkout/status', 'CheckoutController@status')->name('checkout.status');
Route::get('/checkout/payment', 'CheckoutController@payment')->name('checkout.payment');
Route::resource('/checkout', 'CheckoutController');

// user memeber
Route::get('/user', function(){
	return redirect()->route('user.dashboard');
});

Route::get('/dashboard', function(){
	return redirect()->route('user.dashboard');
});

Route::prefix('/user')->group(function(){
	Route::get('/dashboard', 'MemberController@dashboard')->name('user.dashboard');
	Route::get('/order', 'MemberController@order')->name('user.order');
	Route::get('/wishlist', 'MemberController@wishlist')->name('user.wishlist');
	Route::get('/reviews', 'MemberController@reviews')->name('user.reviews');
	Route::put('/profile/{id}', 'MemberController@profileUpdate')->name('user.profile_update');
	Route::get('/profile', 'MemberController@profile')->name('user.profile');
	Route::get('/settings', 'MemberController@settings')->name('user.settings');
	Route::post('/settings/change_password', 'MemberController@changePassword')->name('user.change_password');
	Route::get('/settings/otp', 'MemberController@otp')->name('user.otp');
	Route::post('/settings/otp_submit', 'MemberController@otpSubmit')->name('user.otp_submit');
	Route::get('/settings/test', 'MemberController@test')->name('user.test');
});

Route::get('/api_test', 'ApiController@index');
Route::get('/api_test/notification', 'ApiController@notification');
Route::get('/api_test/send_message', 'ApiController@sendMessage');
Route::get('/api_test/helio', 'ApiController@helio');

// product details
Route::get('/{slug}', 'ShopController@details')->name('shop.details');